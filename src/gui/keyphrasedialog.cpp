/**
 * \file keyphrasedialog.cpp Implements the KeyphraseDialog class
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <QPushButton>
#include <QMessageBox>
#include <QChar>
#include <QLayout>

#include "keyphrasedialog.h"
#include "ui_keyphrasedialog.h"
#include "errorhandling.h"

KeyphraseDialog::KeyphraseDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::KeyphraseDialog)
{
    ui->setupUi(this);
}   // end constructor

KeyphraseDialog::~KeyphraseDialog()
{
    // Make sure that we blank out the keyphrase strings with spaces before
    // they are releases into genpop, so that keys are not available for
    // scrutiny elsewhere.
    for (int i = 0; i < ui->keyphrase_edit->text().size(); i++)
        ui->keyphrase_edit->text()[i] = ' ';

    for (int i = 0; i < ui->confirm_keyphrase_edit->text().size(); i++)
        ui->keyphrase_edit->text()[i] = ' ';

    delete ui;
}   // end destructor

int KeyphraseDialog::exec(SCRYFE::ByteArray& keyphrase)
{
    int result = QDialog::exec();
    if (result == QDialog::Accepted)
    {
        // Convert to a string first, then convert to ByteArray.
        std::string keyphrase_s = ui->keyphrase_edit->text().toStdString();
        keyphrase.copy_from(keyphrase_s.c_str(), keyphrase_s.size());
        
        // Make sure we clear the temporary string, so we're not leaving the
        // keyphrase in unallocated memory.
        for (std::size_t i = 0; i < keyphrase_s.size(); i++)
            keyphrase_s[i] = ' ';
            
        // Note that ByteArray clears its buffer securely before deleting.
    }   // end if the user clicked OK

    return result;
}   // end exec

void KeyphraseDialog::showEvent(QShowEvent*)
{
    ERROR_HANDLER_START

    // Layout everything sensibly for the OS, and then lock it in.
    adjustSize();
    window()->layout()->setSizeConstraint(QLayout::SetFixedSize);

    // Check that the keyphrases match
    check_keyphrases();

    // Set the focus to the first control.
    ui->keyphrase_edit->setFocus();

    ERROR_HANDLER_END
}   // end showEvent

void KeyphraseDialog::on_keyphrase_edit_textChanged(const QString&)
{
    ERROR_HANDLER_START

    // Check that the keyphrases match
    check_keyphrases();

    ERROR_HANDLER_END
}   // end on_keyphrase_edit_textChanged

void KeyphraseDialog::on_confirm_keyphrase_edit_textChanged(const QString &arg1)
{
    ERROR_HANDLER_START

    // Check that the keyphrases match
    check_keyphrases();

    ERROR_HANDLER_END
}

void KeyphraseDialog::check_keyphrases(void)
{
    // Check that the keyphrases match
    if (ui->keyphrase_edit->text().isEmpty() &&
            ui->confirm_keyphrase_edit->text().isEmpty())
    {
        ui->keyphrase_status->setText("Keyphrases are empty");
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    }
    else if (ui->keyphrase_edit->text() ==
            ui->confirm_keyphrase_edit->text())
    {
        ui->keyphrase_status->setText("Keyphrases match");
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
    }
    else
    {
        ui->keyphrase_status->setText("Keyphrases do not match");
        ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    }
}   // end check_keyphrases
