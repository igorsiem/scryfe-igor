/**
 * \file maindialog.h Declares the MainDialog class
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINDIALOG_H
#define MAINDIALOG_H

#include <QDialog>

/**
 * \brief Namespace for all Qt user interface declarations
 */
namespace Ui {
class MainDialog;
}

/**
 * \brief The main window for the application, implemented as a dialog
 *
 * Most of the logic for actually encrypting and decrypting files is taken
 * care of in the `on_decrypt_button_clicked` and
 * `file_exists_and_can_be_read` slots. The final calls to the SCryFE core
 * library are implemented through the `OperationDialog` and `Worker`
 * classes.
 */
class MainDialog : public QDialog
{
    Q_OBJECT

    public:

    /**
     * \brief Constructs the main window dialog
     *
     * \param parent The parent of the window
     */
    explicit MainDialog(QWidget *parent = 0);

    /**
     * \brief Destructor - cleans up main window resources
     */
    ~MainDialog();

    // -- Virtual overrides --

    /**
     * \brief Perform initialisation activites for the dialog when it is
     * shown
     */
    virtual void showEvent(QShowEvent*);

    // --- Internal Declarations ---

    private slots:

    /**
     * \brief Executes file encryption
     *
     * This method is called when the 'Encrypt' button is clicked.
     */
    void on_encrypt_button_clicked(void);

    /**
     * \brief Executes file encryption
     *
     * This method is called when the 'Decrypt' button is clicked.
     */
    void on_decrypt_button_clicked();

    // --- Internal Declarations ---

    private:

    // -- Utility methods --

    bool file_exists_and_can_be_read(const std::string& filename);

    // -- Attributes --

    /**
     * \brief The internally-managed UI implementation object
     */
    Ui::MainDialog *ui;
};  // end MainDialog

#endif // MAINDIALOG_H
