/**
 * \file keyphrasedialog.h Declares the KeyphraseDialog class
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef KEYPHRASEDIALOG_H
#define KEYPHRASEDIALOG_H

#include <QDialog>
#include <scryfe/scryfe.h>

namespace Ui {
class KeyphraseDialog;
}

/**
 * \brief A dialog for entering a KeyphraseDialog
 *
 * This class implements special cleanup procedures to ensure that keyphrases
 * are not left in memory any longer than necessary
 */
class KeyphraseDialog : public QDialog
{
    Q_OBJECT

    public:

    /**
     * \brief Constructor, initialising the dialog
     *
     * \param parent The parent of the dialog
     */
    explicit KeyphraseDialog(QWidget *parent = 0);

    /**
     * \brief Destructor - cleans up the dialog
     */
    ~KeyphraseDialog(void);

    /**
     * \brief Execute the dialog, retrieving the keyphrase if accepted
     *
     * \param keyphrase The keyphrase; this is only updated if the 'OK'
     * button is clicked.
     *
     * \return The result (accepted or rejected)
     */
    int exec(SCRYFE::ByteArray& keyphrase);

    // -- virtual overrides --

    /**
     * \brief Perform initialisation activites for the dialog when it is
     * shown
     */
    virtual void showEvent(QShowEvent*);

    private slots:

    /**
     * \brief Check that the keyphrases are present and match
     *
     * This method is called when the text changes in the keyphrase edit box.
     */
    void on_keyphrase_edit_textChanged(const QString&);

    /**
     * \brief Check that the keyphrases are present and match
     *
     * This method is called when the text changes in the confirm keyphrase
     * edit box.
     */
    void on_confirm_keyphrase_edit_textChanged(const QString &arg1);

private:

    /**
     * \brief Check the keyphrase and confirmation edit boxes, to see if
     * they have a keyphrase, and match
     *
     * This method is called when the dialog is first shown, and whenever the
     * text changes in either edit box. If the keyphrase is empty or
     * does not match the confirmaton dialog, the OK button is disabled, and
     * an appropriate status message is displaed. If the keyphrases are
     * present, and matched, the message is made to say so, and the OK button
     * is enabled.
     */
    void check_keyphrases(void);

    /**
     * \brief ui The internal UI object
     */
    Ui::KeyphraseDialog *ui;
};  // end Keyphrase dialog

#endif // KEYPHRASEDIALOG_H
