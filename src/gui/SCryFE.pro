#-------------------------------------------------
#
# Project created by QtCreator 2014-07-08T22:50:08
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SCryFE
TEMPLATE = app


SOURCES += main.cpp\
        maindialog.cpp \
    keyphrasedialog.cpp \
    entropydialog.cpp \
    operationdialog.cpp \
    worker.cpp

HEADERS  += maindialog.h \
    keyphrasedialog.h \
    errorhandling.h \
    entropydialog.h \
    operationdialog.h \
    worker.h

FORMS    += maindialog.ui \
    keyphrasedialog.ui \
    entropydialog.ui \
    operationdialog.ui

RC_ICONS = scryfe.ico
RESOURCES = resources.qrc
