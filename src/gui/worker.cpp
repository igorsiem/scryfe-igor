/**
 * \file worker.cpp Implements the Worker class
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdexcept>
#include <QThread>
#include "worker.h"

Worker::Worker(void) :
    QObject(),
    m_operation(SCRYFE::none),
    m_filename(),
    m_raw_keyphrase(),
    m_raw_entropy_for_iv()
{
}   // end constructor

void Worker::init_encrypt(
        const std::string& filename,
        SCRYFE::ByteArray& raw_keyphrase,
        const SCRYFE::ByteArray& raw_entropy_for_iv)
{
    m_operation = SCRYFE::encrypt;
    m_filename = filename;
    m_raw_keyphrase = raw_keyphrase;
    raw_keyphrase.clear();
    m_raw_entropy_for_iv = raw_entropy_for_iv;
}   // end init_encrypt
    
void Worker::init_decrypt(
        const std::string& filename,
        SCRYFE::ByteArray& raw_keyphrase)
{
    m_operation = SCRYFE::decrypt;
    m_filename = filename;
    m_raw_keyphrase = raw_keyphrase;
    raw_keyphrase.clear();
    
    // The entropy buffer isn't used.
    m_raw_entropy_for_iv.clear();
}   // end init_decrypt

void Worker::process(void)
{
    // Catch all exceptions, and signal them with a message that is passed
    // using a Qt signal.
    try
    {
        // Check our arguments.
        if ((m_operation != SCRYFE::encrypt) &&
                (m_operation != SCRYFE::decrypt))
            throw std::runtime_error("task initialised with unrecognised "
                "operation enumerator");
            
        // Hash the keyphrase and destroy the raw one.
        SCRYFE::SHA256Hash hash256;
        SCRYFE::ByteArray key;
        hash256.process(m_raw_keyphrase, key);
        m_raw_keyphrase.clear();
        
        // Create a progress monitor object that will report back during the
        // operation.
        ProgressMonitor progress_monitor(*this);
        
        // What operation are we doing?
        if (m_operation == SCRYFE::encrypt)
        {
            // We now have been given 128 bits of entropy (scattered over a
            // much wider array of bytes). We use the SHA-256 hash to
            // condition and condense this down to 256 bits (32 bytes)
            //
            // Note: we are actually trying to construct a 128-bit random
            // Initialisation Vector (IV), but we don't have a 128-bit
            // version of SHA (there isn't one). Instead, we hash 128 bits of
            // entropy down to a 256-bit hash, and then XOR one half of this
            // with the other.
            //
            // I don't believe that this will result in any loss of entropy,
            // but this probably needs to be verified by a cryptography
            // expert.
            //
            //        - Igor Siemienowicz, 2 July 2014
            SCRYFE::ByteArray hashed_entropy;
            hash256.process(m_raw_entropy_for_iv, hashed_entropy);
            
            // The IV is then the two halves of the hash, XORd together.
            SCRYFE::ByteArray iv =
                SCRYFE::ByteArray(hashed_entropy.bytes(), 16) ^
                SCRYFE::ByteArray((char*)hashed_entropy.bytes()+16, 16);
                
            // Now we can go ahead and encrypt. If there is no exception, the
            // operation worked.
            SCRYFE::FileEncrypter::encrypt(
                    m_filename,
                    SCRYFE::FileEncrypter::aes256,
                    SCRYFE::FileEncrypter::ofb,
                    iv,
                    key,
                    &progress_monitor);
        }
        else
        {
            // We don't need the IV for decrypting (it comes from the
            // encrypted file). We can go ahead and decrypt. If there is no
            // exception, the operation was successful.
            SCRYFE::FileEncrypter::decrypt(
                    m_filename,
                    SCRYFE::FileEncrypter::aes256,
                    SCRYFE::FileEncrypter::ofb,
                    key,
                    &progress_monitor);
        }

        emit finished_no_error();
    }
    catch(const std::exception& e)
    {
        emit finished_with_error(e.what());
    }
    catch (...)
    {
        emit finished_with_error(
            "An unrecognised error condition was signalled");
    }
}   // end process

void Worker::ProgressMonitor::signal_progress(
        int64 number_of_bytes_processed,
        int64 total_number_of_bytes)
{
    // Convert to a straight percentage to avoid overflowing the progress
    // bar.
    int progress_percent = static_cast<int>(
        100.00 * static_cast<double>(number_of_bytes_processed) /
        static_cast<double>(total_number_of_bytes));

    m_worker.report_progress(progress_percent, 100);
}   // end signal_progress     
