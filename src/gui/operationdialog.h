/**
 * \file operationdialog.h Declares the OperationDialog class
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <QDialog>
#include <QThread>

#include <scryfe/scryfe.h>

#include "worker.h"

#ifndef OPERATIONDIALOG_H
#define OPERATIONDIALOG_H

namespace Ui {
class OperationDialog;
}

/**
 * \brief A dialog for executing and monitoring the encrypt or decrypt
 * operation
 *
 * The operation is executed in a background thread and worker object (see
 * the `Worker` class. The only action a caller needs to take is to
 * instantiate this dialog class, and call the `encrypt` or `decrypt` method.
 */
class OperationDialog : public QDialog
{
    Q_OBJECT

    // --- External interface ---

    public:

    /**
     * \brief Constructor, initialising the UI
     *
     * Note: This method calls the parent constructor with arguments that
     * disable the Close Window button, because we don't want the user to
     * interrupt an encrypt / decrypt operation.
     *
     * \todo Do we really want to stop users from interrupting encrypt /
     * decrypt?
     *
     * \param parent The parent widget of the dialog
     */
    explicit OperationDialog(QWidget *parent = 0);

    /**
     * \brief Cleans up the UI object
     */
    ~OperationDialog();

    /**
     * \brief Execute the dialog in encryption mode
     *
     * This method executes the encryption operation, and displays a message
     * informing the user of success or error.
     *
     * \param filename The name of the file to encrypt
     * 
     * \param raw_keyphrase The **raw** keyphrase; note that this is
     * destroyed in the process (so is non-const)
     * 
     * \param raw_entropy_for_iv The buffer of raw entropy that has been
     * harvested from the keyboard; this will be hashed into the
     * initialisation vector used for encryption; it is *assumed* that
     * sufficient entropy has been harvested
     */
    void encrypt(
        const std::string& filename,
        SCRYFE::ByteArray& raw_keyphrase,
        const SCRYFE::ByteArray& raw_entropy_for_iv);

    /**
     * \brief Execute the dialog in decryption mode
     *
     * This method executes the decryption operation, and displays a message
     * informing the user of success or error.
     *
     * \param filename The name of the file to encrypt
     * 
     * \param raw_keyphrase The **raw** keyphrase to use; note that this is
     * destroyed in the process (so is non-const)
     */
    void decrypt(
        const std::string& filename,
        SCRYFE::ByteArray& raw_keyphrase);

    // -- Virtual overrides --

    /**
     * \brief Perform initialisation of the dialog and start the worker
     * thread
     */
    virtual void showEvent(QShowEvent*);

    /**
     * \brief Disable dialog rejection mechanism
     *
     * We only want to close the dialog after the operaton is complete.
     *
     * \todo Do we REALLY want to stop users from cancelling the operation
     * after he has started?
     */
    virtual void reject() {}

    /**
     * \brief Retrieve the error message
     *
     * This is empty if no error occurred
     *
     * \return The error message string
     */
    const std::string& error_message(void) const { return m_error_message; }

    // -- Signals and slots

    private slots:

    /**
     * \brief Signal progress during the process
     */
    void progress(int p, int total);

    /**
     * \brief The operation completed without any errors - wait for the
     * thread to finish, and shut down the dialog
     */
    void finished_no_error(void) { m_worker_thread.wait(); accept(); }

    /**
     * \brief The operation is finished, but an error occurred
     *
     * Wait for the thread to finish and shut down the dialog.
     *
     * \param error_message An error message
     */
    void finished_with_error(const QString& error_message);

    // --- Internal declarations ---

    private:

    // -- Attributes --

    /**
     * \brief The internal Qt UI object
     */\
    Ui::OperationDialog *ui;

    /**
     * \brief The background thread in which the encryption is done
     */
    QThread m_worker_thread;

    /**
     * \brief The worker object that is executed in the worker thread
     */
    Worker m_worker;

    /**
     * \brief An error message (if an error occurred during the operation)
     *
     * This is empty if there was no error.
     */
    std::string m_error_message;
};  // end OperationDialog

#endif // OPERATIONDIALOG_H
