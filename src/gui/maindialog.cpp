/**
 * \file main.cpp Implements the MainDialog class
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>

#include <QMessageBox>
#include <QFileDialog>

#include "maindialog.h"
#include "ui_maindialog.h"
#include "keyphrasedialog.h"
#include "errorhandling.h"
#include "entropydialog.h"
#include "operationdialog.h"

MainDialog::MainDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MainDialog)
{
    ui->setupUi(this);
}   // end constructor

MainDialog::~MainDialog()
{
    delete ui;
}   // end destructor

void MainDialog::showEvent(QShowEvent*)
{
    ERROR_HANDLER_START

    // Layout everything sensibly for the OS, and then lock it in.
    adjustSize();
    window()->layout()->setSizeConstraint(QLayout::SetFixedSize);

    ERROR_HANDLER_END
}   // end showEvent

void MainDialog::on_encrypt_button_clicked(void)
{
    ERROR_HANDLER_START

    // Ask for a file from the user.
    QFileDialog file_dialog(this, "File to encrypt");
    file_dialog.setAcceptMode(QFileDialog::AcceptOpen);
    if (file_dialog.exec() == QDialog::Accepted)
    {
        if (file_exists_and_can_be_read(
                file_dialog.selectedFiles()[0].toStdString()) == false)
        {
            std::stringstream msg;
            msg << "Could not open file \"" <<
                file_dialog.selectedFiles()[0].toStdString() << "\"";
            throw std::runtime_error(msg.str());
        }

        // Ask for a password from the user.
        SCRYFE::ByteArray keyphrase;
        KeyphraseDialog keyphrase_dialog(this);
        if (keyphrase_dialog.exec(keyphrase) == QDialog::Accepted)
        {
            // We're encrypting, so we need some entropy for the IV.
            SCRYFE::ByteArray raw_entropy;
            EntropyDialog entropy_dialog(this);
            if (entropy_dialog.exec(128, raw_entropy) == QDialog::Accepted)
            {
                // Execute using an operation dialog
                OperationDialog operation_dialog;
                operation_dialog.encrypt(
                    file_dialog.selectedFiles()[0].toStdString(),
                    keyphrase,
                    raw_entropy);

                // Check for errors.
                if (operation_dialog.error_message().empty() == false)
                    throw std::runtime_error(
                        operation_dialog.error_message());
                    
                // Show a message.
                QMessageBox::information(this, "Encrypt", "Operation "
                    "complete");
            }
        }   // end if the user entered a password OK
    }   // end if the user chose a file

    ERROR_HANDLER_END
}   // end on_encrypt_button_clicked

void MainDialog::on_decrypt_button_clicked()
{
    ERROR_HANDLER_START

    // Ask for a file from the user.
    QFileDialog file_dialog(this, "File to decrypt");
    file_dialog.setAcceptMode(QFileDialog::AcceptOpen);
    if (file_dialog.exec() == QDialog::Accepted)
    {
        if (file_exists_and_can_be_read(
                file_dialog.selectedFiles()[0].toStdString()) == false)
        {
            std::stringstream msg;
            msg << "Could not open file \"" <<
                file_dialog.selectedFiles()[0].toStdString() << "\"";
            throw std::runtime_error(msg.str());
        }

        // Ask for a password from the user.
        SCRYFE::ByteArray keyphrase;
        KeyphraseDialog keyphrase_dialog(this);
        if (keyphrase_dialog.exec(keyphrase) == QDialog::Accepted)
        {
            // Execute using an operation dialog
            OperationDialog operation_dialog;
            operation_dialog.decrypt(
                file_dialog.selectedFiles()[0].toStdString(),
                keyphrase);

            // Check for errors.
            if (operation_dialog.error_message().empty() == false)
                throw std::runtime_error(operation_dialog.error_message());
            
            // Show a message.
            QMessageBox::information(this, "Decrypt", "Operation complete");
        }   // end if the user entered a password OK
    }   // end if the user chose a file

    ERROR_HANDLER_END
}   // end on_decrypt_button_clicked

bool MainDialog::file_exists_and_can_be_read(const std::string& filename)
{
    bool file_is_ok = false;

    // Check that the file can be opened for reading and writing, by
    // briefly opening the file for reading. We use fopen_s under Windows to
    // stop it flagging a warning.
#ifdef _WIN32
    FILE* file_ptr = NULL;
    errno_t result = fopen_s(&file_ptr, filename.c_str(), "r");
    if ((file_ptr != NULL) || (result == 0))
#else
    FILE* file_ptr = fopen(filename.c_str(), "r");
    if (file_ptr != NULL)
#endif
    {
        fclose(file_ptr);
        file_is_ok = true;
    }

    return file_is_ok;
}   // end file_exists_and_can_be_read
