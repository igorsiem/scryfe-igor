/**
 * \file entropydialog.h Declares the EntropyDialog class
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ENTROPYDIALOG_H
#define ENTROPYDIALOG_H

#include <QDialog>
#include <QKeyEvent>

#include <scryfe/scryfe.h>

namespace Ui {
class EntropyDialog;
}

/**
 * \brief A dialog for gathering raw keyboard entropy when the user is
 * encrypting
 *
 * Note that the entropy returned by the `exec` method here needs to be
 * 'cooked' using a good hash.
 */
class EntropyDialog : public QDialog
{
    Q_OBJECT

    public:

    /**
     * \brief Initialise the dialog object
     *
     * \param parent The parent widget
     */
    explicit EntropyDialog(QWidget *parent = 0);

    /**
     * \brief Destructor - cleans up the dialog object
     */
    ~EntropyDialog(void);

    /**
     * \brief Executes the dialog to gather raw entropy
     *
     * The number of bits of entropy to gather is requested as an argument.
     * Entropy per keypress is conservatively estimated at 0.5 bits per
     * keypress (ref
     * [eTutorials](http://etutorials.org/Programming/secure+programming/Chapter+11.+Random+Numbers/11.20+Gathering+Entropy+from+the+Keyboard/)).
     *
     * \param entropy_bits The number of bits of entropy to gather
     *
     * \param entropy A byte array for the raw entropy entries; note that
     * this is only filled if the user does not cancel
     *
     * \return Standard QDialog `exec` return value
     */
    int exec(std::size_t entropy_bits, SCRYFE::ByteArray& entropy);

    // -- Virtual overrides --

    /**
     * \brief Perform initialisation activites for the dialog when it is
     * shown
     */
    virtual void showEvent(QShowEvent*);

    protected:

    /**
     * \brief Called when a key is pressed; adds the keypress to the entropy
     * buffer
     *
     * \param e The key event
     */
    virtual void keyPressEvent(QKeyEvent* e);

    // --- Internal Declarations ---

private:

    /**
     * \brief The internal Qt UI object
     */
    Ui::EntropyDialog *ui;

    /**
     * \brief The internal entropy buffer
     */
    SCRYFE::ByteArray m_buffer;

    /**
     * \brief The number of bytes needed (NOT the same as entropy bits)
     */
    std::size_t m_number_of_bytes_needed;

    /**
     * \brief The number of bytes gathered during the operation
     */
    std::size_t m_number_of_bytes_gathered;

    /**
     * \brief The last key pressed
     *
     * This is used to make sure keypresses are not repeats.
     */
    char m_last_key_pressed;
};  // end EntropyDialog

#endif // ENTROPYDIALOG_H
