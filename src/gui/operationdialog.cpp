/**
 * \file main.cpp Implements the MainDialog class
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <QLayout>

#include "operationdialog.h"
#include "ui_operationdialog.h"
#include "errorhandling.h"

OperationDialog::OperationDialog(QWidget *parent) :
    QDialog(parent, Qt::CustomizeWindowHint | Qt::WindowTitleHint),
    ui(new Ui::OperationDialog),
    m_worker_thread(),
    m_worker(),
    m_error_message()
{
    ui->setupUi(this);
}

OperationDialog::~OperationDialog()
{
    delete ui;
}

void OperationDialog::showEvent(QShowEvent*)
{
    ERROR_HANDLER_START

    // Layout everything sensibly for the OS, and then lock it in.
    adjustSize();
    window()->layout()->setSizeConstraint(QLayout::SetFixedSize);

    // Set up the worker and pass it to the worker thread.
    // Worker* worker = new Worker;
    m_worker.moveToThread(&m_worker_thread);
    m_error_message.clear();

    // Connect up the signals and slots between the worker object, the thread
    // and this dialog.
    connect(&m_worker_thread, SIGNAL(started()), &m_worker, SLOT(process()));

    connect(&m_worker, SIGNAL(progress(int, int)), this,
        SLOT(progress(int, int)));

    connect(&m_worker, SIGNAL(finished_no_error()), &m_worker_thread,
        SLOT(quit()));
    connect(&m_worker, SIGNAL(finished_no_error()), this,
        SLOT(finished_no_error()));

    connect(&m_worker, SIGNAL(finished_with_error(const QString&)),
        &m_worker_thread, SLOT(quit()));
    connect(&m_worker, SIGNAL(finished_with_error(const QString&)), this,
        SLOT(finished_with_error(const QString&)));

    // Now we can start the thread itself.
    m_worker_thread.start();

    ERROR_HANDLER_END
}   // end showEvent

void OperationDialog::encrypt(
        const std::string& filename,
        SCRYFE::ByteArray& raw_keyphrase,
        const SCRYFE::ByteArray& raw_entropy_for_iv)
{
    ui->operation_label->setText("Operation: encrypt");
    ui->filename_label->setText(filename.c_str());
    
    m_worker.init_encrypt(filename, raw_keyphrase, raw_entropy_for_iv);
    exec();
}   // end encrypt

void OperationDialog::decrypt(
        const std::string& filename,
        SCRYFE::ByteArray& raw_keyphrase)
{
    ui->operation_label->setText("Operation: decrypt");
    ui->filename_label->setText(filename.c_str());
    
    m_worker.init_decrypt(filename, raw_keyphrase);
    exec();
}   // end encrypt

void OperationDialog::progress(int p, int total)
{
    ui->progress_bar->setMaximum(total);
    ui->progress_bar->setValue(p);
}   // end progress

void OperationDialog::finished_with_error(const QString& error_message)
{
    m_error_message = error_message.toStdString();
    m_worker_thread.wait();
    accept();
}   // end finished_with_error
