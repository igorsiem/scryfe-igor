/**
 * \file worker.h Declares the Worker class
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WORKER_H
#define WORKER_H

#include <string>
#include <QObject>
#include <scryfe/scryfe.h>

/**
 * \brief A task object containing the calls to the SCryFE core library
 * encryption / decryption functionality that is actually executed in the
 * background thread
 */
class Worker : public QObject
{
    Q_OBJECT

    // --- Public interface ---

    public:

    /**
     * Trivial construictor
     * 
     * The object cannot actually be used until either the `init_encrypt` or
     * `init_decrypt` method is called. Note also that the QObject (base
     * class) constructor is called with no argument (because Worker objects
     * don't need to be constructed with a parent).
     */
    Worker(void);

    /**
     * \brief Trivial destructor
     */
    virtual ~Worker(void) {}
    
    /**
     * \brief Initialise the Worker in encryption mode
     * 
     * Note that this just checks the arguments and saves them. The `process`
     * method does the actual work.
     * 
     * \param filename The name of the file to encrypt; this is not checked -
     * it is assumed that it has already been checked for accessibility
     * 
     * \param raw_keyphrase The **raw** keyphrase to use for encryption; note
     * that this is destroyed during the process for security (and so is
     * non-const)
     * 
     * \param raw_entropy_for_iv The buffer of raw entropy that has been
     * harvested, and will be used to produce the IV; it is *assumed* that
     * this includes the required amount of entropy
     */
    void init_encrypt(
        const std::string& filename,
        SCRYFE::ByteArray& raw_keyphrase,
        const SCRYFE::ByteArray& raw_entropy_for_iv);
    
    /**
     * \brief Initialise the Worker in decryption mode
     * 
     * Note that this just checks the arguments and saves them. The `process`
     * method does the actual work.
     * 
     * \param filename The name of the file to encrypt; this is not checked -
     * it is assumed that it has already been checked for accessibility
     * 
     * \param raw_keyphrase The **raw** keyphrase to use for encryption; this
     * is destroyed during the process for security (and is therefore non-
     * const
     */
    void init_decrypt(
        const std::string& filename,
        SCRYFE::ByteArray& raw_keyphrase);

    // -- Signals and slots --
    public slots:

    /**
     * \brief Execute the encryption / decryption operation
     *
     * This method is called from a background worker thread. Either the
     * `init_encrypt` or `init_decrypt` method should have been called
     * first.
     * 
     * Note that all exceptions (including those due to incorrect constructor
     * arguments) that are thrown by the operation are caught here, and
     * signalled using `finished_with_error`.
     */
    void process(void);

    signals:

    /**
     * \brief Signal progress
     */
    void progress(int p, int total);

    /**
     * \brief The operation has finished without errors
     */
    void finished_no_error(void);

    /**
     * \brief The operation has completed, by there was an error
     *
     * \param An error message
     */
    void finished_with_error(const QString& error_message);
    
    // --- Internal declarations ---
    
    private:
        
    // -- Internal types --
        
    /**
     * \brief A simple callback interface so that the encryption operation
     * can report back on its progress
     * 
     * We use it to send signals from the worker thread to the main GUI
     * thread so that a progress bar can be updated.
     */
    class ProgressMonitor : public SCRYFE::FileEncrypter::ProgressMonitor
    {
        public:
            
        /**
         * \brief Constructor - initialises the internal reference to
         * the Worker class that we will be notifying of progress
         * 
         * \param worker The Worker object
         */
        ProgressMonitor(Worker& worker) :
            SCRYFE::FileEncrypter::ProgressMonitor(),
            m_worker(worker) {}
        
        /**
         * \brief Virtual (trivial) destructor for consistency
         */
        virtual ~ProgressMonitor(void) {}
            
        /**
         * \brief Signal that progress has been made in the operation
         * 
         * Derived classes can implement this method to display a percentage,
         * or update a progress bar.
         * 
         * This method is called from the thread in which the `encrypt` or
         * `decrypt` method is running. Callers need to take appropriate
         * measures in multi-threading situations.
         * 
         * \param number_of_bytes_processed The number of bytes that have
         * been processed (encrypted or decrypted) so far
         * 
         * \param total_number_of_bytes The total number of bytes to
         * process
         */
        virtual void signal_progress(
            int64 number_of_bytes_processed,
            int64 total_number_of_bytes);
        
        // --- Internal declarations ---
        
        private:
            
        /**
         * \brief The Worker object that we are notifying of progress
         */
        Worker& m_worker;
    };  // end ProgressMonitor
    
    // -- Internal helper methods --
    
    /**
     * \brief Emits the progress signal
     * 
     * This method is provided so that the progress monitor can tell the
     * Worker object when to emit.
     */
    void report_progress(int p, int total) { emit progress(p, total); }
        
    // -- Internal attributes --
        
    /**
     * \brief The operation being performed
     */
    SCRYFE::cipher_mode m_operation;
    
    /**
     * \brief The name of the file on which the operation works
     */
    std::string m_filename;
    
    /**
     * \brief The **raw** (unhashed) keyphrase being used
     */
    SCRYFE::ByteArray m_raw_keyphrase;
    
    /**
     * \brief The raw entropy that will be used to produce the encryption IV
     * 
     * Note that this is only used if we are encrypting. If we are decrypting,
     * the IV comes from the file itself.
     */
    SCRYFE::ByteArray m_raw_entropy_for_iv;
};  // end Worker

#endif // WORKER_H
