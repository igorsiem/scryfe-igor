/**
 * \file errorhandling.h Declares the error handling macros
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <QMessageBox>

#ifndef ERRORHANDLING_H
#define ERRORHANDLING_H

/**
 * \brief Implements the start of the standard top-level error-handling
 * sequence
 *
 * This macro (and the ERROR_HANDLER_END macro) define a standard top-level
 * try/catch block for catching and displaying exceptions. They should be
 * used at the top level of all slots and event handlers to avoid exceptions
 * being propagated into Qt (which will abort if they are thrown).
 */
#define ERROR_HANDLER_START try {

/**
 * \brief Implements the end of the standard top-level error-handling
 * sequence
 */
#define ERROR_HANDLER_END \
} \
catch (const std::exception& error) \
{ \
    QMessageBox::critical(this, "Error", error.what()); \
} \
catch (...) \
{ \
    QMessageBox::critical(this, "Error", "An unrecognised error condition " \
        "was detected. The operation cannot continue."); \
}

#endif // ERRORHANDLING_H
