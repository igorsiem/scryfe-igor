/**
 * \file entropydialog.h Implements the EntropyDialog class
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef _WIN32
    #include <windows.h>
#else
    #include <sys/time.h>
#endif

#include <QMessageBox>

#include "entropydialog.h"
#include "ui_entropydialog.h"
#include "errorhandling.h"

/**
 * \brief An entry in the entropy table
 *
 * This includes a typed character, and time in milliseconds or microseconds
 * (depending on platform). On most consumer hardware, the clock isn't even
 * accurate to milliseconds, so there's no reduction in generality or
 * randomness for either method. The entire structure is reckoned to
 * encapsulate about half a bit of entropy for keyboard presses.
 *
 * See chapter 11.20 in [eTutorials]([eTutorials.org](http://etutorials.org)
 * for more on this.
 */
struct EntropyEntry
{
    /**
     * \brief The character types
     */
    char m_c;

#ifdef _WIN32

    /**
     * \brief The milliseconds (within the second) of the time at which the
     * key was pressed
     */
    WORD m_milliseconds;

#else

    /**
     * \brief The microseconds as returned by gettimeofday
     */
    unsigned long int m_microseconds;

#endif
};  // end struct EntropyEntry

EntropyDialog::EntropyDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EntropyDialog),
    m_buffer(),
    m_number_of_bytes_needed(0),
    m_number_of_bytes_gathered(0),
    m_last_key_pressed(0)
{
    ui->setupUi(this);
}

EntropyDialog::~EntropyDialog()
{
    delete ui;
}

int EntropyDialog::exec(std::size_t entropy_bits, SCRYFE::ByteArray& entropy)
{
    // How many entries and bytes do we need?
    std::size_t number_of_entries = entropy_bits * 2;
    m_number_of_bytes_needed = number_of_entries * sizeof(EntropyEntry);

    // Allocate the internal buffer with this many bytes.
    m_buffer.set_to_zero(m_number_of_bytes_needed);

    // Set the progress to zero, and initialise the timer.
    ui->progress_bar->setValue(0);
    m_number_of_bytes_gathered = 0;

    // Now we can execute the dialog, checking the result.
    int result = QDialog::exec();
    if (result == QDialog::Accepted)
        entropy = m_buffer;

    return result;
}   // end exec

void EntropyDialog::showEvent(QShowEvent*)
{
    ERROR_HANDLER_START

    // Layout everything sensibly for the OS, and then lock it in.
    adjustSize();
    window()->layout()->setSizeConstraint(QLayout::SetFixedSize);

    // Set the focus to the progress bar, so that the 'Enter' key won't
    // activate the Cancel button.
    ui->progress_bar->setFocus();

    ERROR_HANDLER_END
}   // end showEvent

void EntropyDialog::keyPressEvent(QKeyEvent* e)
{
    ERROR_HANDLER_START

    // Construct a new entropy entry.
    EntropyEntry ent;

    // What was the keypress (as a char)?
    ent.m_c = e->text().toStdString()[0];

    // Make sure it is not the same key as last time.
    if (m_last_key_pressed != ent.m_c)
    {
        m_last_key_pressed = ent.m_c;

        // Get the sytem time.
#ifdef _WIN32
        SYSTEMTIME st;
        GetSystemTime(&st);
        ent.m_milliseconds = st.wMilliseconds;
#else
        struct timeval tv;
        struct timezone tz;
        if (gettimeofday(&tv, &tz) < 0)
            throw std::runtime_error("error retrieving system time");

        ent.m_microseconds = (unsigned long int)tv.tv_usec;
#endif

        memcpy((char*)m_buffer.bytes() + m_number_of_bytes_gathered, &ent,
               sizeof(ent));
        m_number_of_bytes_gathered += sizeof(ent);

        // Put our progress in the progress bar.
        int p = static_cast<int>(100.0 *
            static_cast<double>(m_number_of_bytes_gathered) /
            static_cast<double>(m_number_of_bytes_needed));

        ui->progress_bar->setValue(p);

        // Have we got all the entropy we need?
        if (m_number_of_bytes_gathered >= m_number_of_bytes_needed)
            accept();
    }   // end if this key is not the same as the last one.

    ERROR_HANDLER_END
}   // end keyPressEvent
