# File:         test-integration.rake
# Description:  Rake file for running the automated integration tests
# Author:       Igor Siemienowicz
# Copyright:    The content of this file is copyright (c) 2014 by the named
#               author(s), and licensed under terms of the Gnu Lesser General
#               Public License (LGPL). See the file `lgpl-3.0.txt` in the
#               `doc/licenses` directory for the full text of this license.

Dir.glob("src/test/integration/core/*.rake").each { |r| import r }
