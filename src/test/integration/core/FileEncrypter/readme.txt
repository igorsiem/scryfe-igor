`FileEncrypter` Directory
=========================
This directory contains the source for a test harness executable for the
core `FileEncrypter` class. This executable encrypts or decrypts a named
file, taking the following arguments:

*   `[filename]` - The name of the file to encrypt or decrypt

*   `--cipher [cipher]` - The cipher to use; at the moment, only the `aes256`
    cipher is supported
    
*   `--block-mode [block-mode]` - The block cipher mode to use; at the moment
    only `ofb` (Output FeedBack) is supported

*   `--operation [encrypt|decrypt]` - The operation to perform

*   `--key <hex-digits>` - A hex string for the key; must be correct length
    for the chosen cipher
    
*   `--iv <hex-digits>` - A hex string for the Initialisation Vector; must
    be the correct length for the cipher and block mode

Some things to note:

*   The encryption and decryption is destructive - i.e., it *overwrites* the
    file. This is deliberate, so that the plaintext bytes are not left on
    the disk.
    
*   This executable is intended for verification purposes only. *It is
    not - repeat* **not** *intended for real security.* Having a non-random
    IV and a key on the command-line is laughably insecure.