# File:         TestFileEncrypter.rb
#
# Description:  Integration test script for the SCryFE FileEncrypter class;
#               uses the FileEncrypter-test-harness executable
#
# Author:       Igor Siemienowicz
# Copyright:    The content of this file is copyright (c) 2014 by the named
#               author(s), and licensed under terms of the Gnu Lesser General
#               Public License (LGPL). See the file `lgpl-3.0.txt` in the
#               `doc/licenses` directory for the full text of this license.

require 'fileutils'

# --- Detect windows
require 'rbconfig'
is_windows = (RbConfig::CONFIG['host_os'] =~ /mswin|mingw|cygwin/)

# --- Configuration ---
bin_dir = "build/bin"
bin_dir = "build/bin/Release" if is_windows
test_exe = "#{bin_dir}/FileEncrypter-test-harness"
fixtures_src_dir = "src/test/integration/core/FileEncrypter/fixtures"
fixtures_dest_dir = bin_dir + "/fixtures"

# Files (with separate subscripts)
files = [
    ["test1", "txt"],
    ["test2-31-bytes", "txt"],
    ["test3-32-bytes", "txt"],
    ["test4-33-bytes", "txt"],
	["test5-34-bytes", "txt"],
    ["test6-35-bytes", "txt"],
    ["test7", "jpg"],
    ["test8", "jpg"]
]

# Ciphers
ciphers = [
    "aes256"        # AES-256 (Rijndael)
]

# Block modes
block_modes = [
    "ofb"           # Output FeedBack
]

# Initialisation vectors
ivs = [
    "00000000000000000000000000000000",
    "0123456789abcdef0123456789abcdef"
]

# Encryption keys
keys = [
    "0000000000000000000000000000000000000000000000000000000000000000",
    "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
]

puts "FileEncrypter Integration Test Script"
puts "Copyright (c) 2014 Igor Siemienowicz, licensed under LGPL 3.0"

# Create multiple versions of the file for encryption.
FileUtils.mkpath fixtures_dest_dir
files.each do |file_and_extension|
    filename = file_and_extension[0]
    extension = file_and_extension[1]
    
    ciphers.each do |cipher|
        block_modes.each do |block_mode|
            ivs.each do |iv|
                keys.each do |key|
                    dest_filename = "#{filename}-#{cipher}-#{block_mode}-" +
                        "iv-#{ivs.index(iv)}-key-#{keys.index(key)}"
                    
                    # Copy to encryption file
                    FileUtils.cp("#{fixtures_src_dir}/#{filename}." +
                        "#{extension}",
                        "#{fixtures_dest_dir}/#{dest_filename}-" +
                        "encrypt.#{extension}")
                    
                    # Perform the encryption step
                    system "#{test_exe} #{fixtures_dest_dir}/" +
                        "#{dest_filename}-encrypt.#{extension} " +
                        "--operation encrypt " +
                        "--cipher #{cipher} " +
                        "--block-mode #{block_mode} " +
                        "--iv #{iv} " +
                        "--key #{key} "
                    
                    # Copy to decryption file
                    FileUtils.cp("#{fixtures_dest_dir}/#{dest_filename}-" +
                        "encrypt.#{extension}",
                        "#{fixtures_dest_dir}/#{dest_filename}-decrypt." +
                        "#{extension}")
                    
                    # Perform the decryption step
                    system "#{test_exe} #{fixtures_dest_dir}/" +
                        "#{dest_filename}-decrypt.#{extension} " +
                        "--operation decrypt " +
                        "--cipher #{cipher} " +
                        "--block-mode #{block_mode} " +
                        "--key #{key} "
                    
                    # Do a file compare.
                    if (FileUtils.compare_file(
                            "#{fixtures_src_dir}/#{filename}.#{extension}",
                            "#{fixtures_dest_dir}/#{dest_filename}-" +
                                "decrypt.#{extension}") == false)
                        raise "#{fixtures_dest_dir}/#{dest_filename}-" +
                            "decrypt.#{extension} does not match"
                    end
                end # each key
            end # each iv
        end # each block mode
    end # each cipher
end # each file

puts "\nSUCCESS"
