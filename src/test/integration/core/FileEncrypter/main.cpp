/**
 * \file main Entry point for the FileEncrypter test harness executable
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include <exception>
#include <iostream>
#include <string>
#include <scryfe/scryfe.h>

/**
 * \brief A simple progress reporting monitor for file encryption
 */
class ProgressReporter : public SCRYFE::FileEncrypter::ProgressMonitor
{
    public:
    
    /**
     * \brief Constructor - initialises the update reporting parameters
     *
     * \param update_reporting_frequency Progress will be reported once every
     * 'update_reporting_frequency' updates
     */
    explicit ProgressReporter(int update_reporting_frequency) :
        SCRYFE::FileEncrypter::ProgressMonitor(),
        m_update_counter(0),
        m_update_reporting_frequency(update_reporting_frequency)
    {}
    
    /**
     * \brief Virtual destructor (trivial) for consistency
     */
    virtual ~ProgressReporter(void) {}
        
    /**
     * \brief Report progress on a file encryption / decryption operation
     * 
     * 
     * \param number_of_bytes_processed The number of bytes that have been
     * processed (encrypted or decrypted) so far
     * 
     * \param total_number_of_bytes The total number of bytes to
     * process
     */
    virtual void signal_progress(
        int64 number_of_bytes_processed,
        int64 total_number_of_bytes);
        
    private:
        
    /**
     * \brief A simple counter, so that we can do reporting only once every
     * 'n' updates
     */
    int m_update_counter;
    
    /**
     * \brief The update reporting frequency; the update will be reported
     * once every 'm_update_reporting_frequency' updates.
     */
    int m_update_reporting_frequency;
};  // end class ProgressReporter

void ProgressReporter::signal_progress(
        int64 number_of_bytes_processed,
        int64 total_number_of_bytes)
{
    // Is it time to report?
    if (++m_update_counter >= m_update_reporting_frequency)
    {
        std::cout << "\r" << number_of_bytes_processed << " byte(s) out "
            "of " << total_number_of_bytes << " byte(s) total processed "
            "(" << 100.0 * static_cast<double>(number_of_bytes_processed) /
            static_cast<double>(total_number_of_bytes) << " %)";
            
        m_update_counter = 0;
    }
}   // end signal_progress

/**
 * \brief Entry point for the command-line executable
 * 
 * This method interprets the command-line per the `readme.txt` file, and
 * executes the encryption / decryption operation using the FileEncrypter
 * class.
 * 
 * \param argc The number of command-line arguments
 * 
 * \param argv The vector of command-line arguments
 * 
 * \return Non-zero on error
 */
int main(int argc, char* argv[])
{
    int result = 0;
    try
    {
        std::cout << "FileEncrypter Test Harness" << std::endl <<
            "Copyright (c) 2014 Igor Siemienowicz, licensed under LGPL "
                "3.0" << std::endl;
                
        // Get all our variables sorted out to parse the command-line.
        std::string filename;
        SCRYFE::cipher_mode cm = SCRYFE::none;
        SCRYFE::FileEncrypter::cipher c = SCRYFE::FileEncrypter::aes256;
        SCRYFE::FileEncrypter::block_mode bm = SCRYFE::FileEncrypter::ofb;
        SCRYFE::ByteArray key, iv;
        
        // Loop through the command-line arguments.
        for (int i = 1; i < argc; i++)
        {
            std::string arg = argv[i];
            if (arg == "--cipher")
            {
                // Make sure we have another argument.
                if (i+1 >= argc)
                    throw std::runtime_error("--cipher argument has no "
                        "following argument; need to specify a cipher");
                    
                arg = argv[++i];
                
                if (arg == "aes256") c = SCRYFE::FileEncrypter::aes256;
                else throw std::runtime_error("cipher \"" + arg + "\" not "
                    "recognised");
            }
            else if (arg == "--block-mode")
            {
                if (i+1 >= argc)
                    throw std::runtime_error("--block-mode argument has no "
                        "following argument; need to specify a block mode");
                    
                arg = argv[++i];
                
                if (arg == "ofb") bm = SCRYFE::FileEncrypter::ofb;
                else throw std::runtime_error("block mode \"" + arg +
                    "\" not recognised");
            }
            else if (arg == "--operation")
            {
                if (i+1 >= argc)
                    throw std::runtime_error("--operation argument has no "
                        "following argument; need to specify an operation");
                    
                arg = argv[++i];
                
                if (arg == "encrypt") cm = SCRYFE::encrypt;
                else if (arg == "decrypt") cm = SCRYFE::decrypt;
                else throw std::runtime_error("operation \"" + arg + "\" "
                    "not recognised");
            }
            else if (arg == "--key")
            {
                if (i+1 >= argc)
                    throw std::runtime_error("--key argument has no "
                        "following argument; need to specify a key");
                    
                arg = argv[++i];
                key.copy_from_hex(arg);
            }
            else if (arg == "--iv")
            {
                if (i+1 >= argc)
                    throw std::runtime_error("--iv argument has no "
                        "following argument; need to specify an "
                        "initialisation vector");
                    
                arg = argv[++i];
                iv.copy_from_hex(arg);
            }
            else filename = arg;
        }   // end command-line arguments loop
        
        if (filename.empty())
            throw std::runtime_error("filename not specified");
        
        if (cm == SCRYFE::none)
            throw std::runtime_error("operation not specified");
        
        if (key.size() == 0)
            throw std::runtime_error("key not specified");
        
        if ((iv.size() == 0) && (cm == SCRYFE::encrypt))
            throw std::runtime_error("initialisation vector not specified");
        
        // Print out a summary of what's going on.
        std::cout << std::endl <<
            "operation: ";
            
        if (cm == SCRYFE::encrypt) std::cout << "encrypt";
        else std::cout << "decrypt";
        
        std::cout << std::endl <<
            "file: " << filename << std::endl <<
            "cipher: ";
            
        if (c == SCRYFE::FileEncrypter::aes256) std::cout << "AES-256";
        else std::cout << "*** unknown";
        
        std::cout << std::endl <<
            "block mode: ";
            
        if (bm == SCRYFE::FileEncrypter::ofb) std::cout << "OFB";
        else std::cout << "*** unknown";
        
        std::cout << std::endl <<
            "initialisation vector: " << iv.to_hex_string() << std::endl <<
            "key: " << key.to_hex_string() << std::endl;
            
        // Execute the operation
        ProgressReporter reporter(100);
        if (cm == SCRYFE::encrypt)
            SCRYFE::FileEncrypter::encrypt(filename, c, bm, iv, key,
                &reporter);
        else SCRYFE::FileEncrypter::decrypt(filename, c, bm, key, &reporter);
        
        std::cout << std::endl << std::endl <<
            "operation complete - no errors" << std::endl;
    }
    catch (const SCRYFE::Error& ce)
    {
        std::cerr << "[SCRYFE ERROR] " << ce.what() << std::endl;
        result = -1;
    }
    catch (const std::exception& e)
    {
        std::cerr << "[ERROR] " << e.what() << std::endl;
        result = -2;
    }
    catch (...)
    {
        std::cerr << "[ERROR] *** unrecognised error condition" << std::endl;
        result = -3;
    }
    
    return result;
}   // end main
