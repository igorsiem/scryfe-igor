# File:         test-integration.rake
# Description:  Rake file for running the automated core integration tests
# Author:       Igor Siemienowicz
# Copyright:    The content of this file is copyright (c) 2014 by the named
#               author(s), and licensed under terms of the Gnu Lesser General
#               Public License (LGPL). See the file `lgpl-3.0.txt` in the
#               `doc/licenses` directory for the full text of this license.

namespace :test do
    namespace :integration do
        desc "run automated core integration tests"
        task :core => :binaries do
            sh "ruby src/test/integration/core/FileEncrypter/" +
                "TestFileEncrypter.rb"
        end
        
        task :all => :core
    end # namespace test_integration
    
    desc "Run all automated integration tests"
    task :integration => "integration:all"
    
    task :all => :integration
end # namespace test
