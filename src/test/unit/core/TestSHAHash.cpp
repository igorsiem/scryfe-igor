/**
 * \file TestSHAHash.cpp Tests for the SHA*Hash family of classes
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include <cstdlib>
#include <cppunit/extensions/HelperMacros.h>
#include <scryfe/scryfe.h>
#include <utils/RSPParser.h>

extern std::string fixtures_directory;

/**
 * \brief Tests for the functions of the SHA*Hash classes
 *
 * \todo Test other hash lengths
 */
class TestSHAHash : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestSHAHash);
        CPPUNIT_TEST( test_rsp );
        CPPUNIT_TEST( test_256_short_message );
        CPPUNIT_TEST( test_256_long_message );
        CPPUNIT_TEST( test_256_monte_carlo );      
    CPPUNIT_TEST_SUITE_END();

    /**
     * \brief Test the RSP Parser utility class
     *
     * This isn't strictly part of the core tests, but we do a quick test
     * of the RSP parsing here to make sure everything is OK.
     */
     void test_rsp(void);
     
     /**
      * \brief Test the SHA-256 implementation against the NIST KAT short
      * message vectors
      */
    void test_256_short_message(void);
  
   /**
      * \brief Test the SHA-256 implementation against the NIST KAT long
      * message vectors
      */
    void test_256_long_message(void);

   /**
      * \brief Test the SHA-256 implementation against the NIST KAT Monte
      * Carlo vectors
      */    
    void test_256_monte_carlo(void);
};  // end TestSHAHash

CPPUNIT_TEST_SUITE_REGISTRATION( TestSHAHash );

void TestSHAHash::test_rsp(void)
{
    RSPDocument rsp_doc;
    rsp_doc.parse(fixtures_directory +
        "/shabytetestvectors/SHA256ShortMsg.rsp");
    
    // Test file is valid.
    CPPUNIT_ASSERT(rsp_doc.valid());
    
    // Test file has one section with the name "L = 32"
    CPPUNIT_ASSERT(rsp_doc.get_sections().size() == 1);
    const RSPDocument::Section& section = rsp_doc.get_sections().front();
    CPPUNIT_ASSERT(section.first == "L = 32");
    
    // Section has some pairs in it.
    const RSPDocument::NameValuePairSequence& pairs = section.second;
    CPPUNIT_ASSERT(pairs.size() > 0);
    
    // First pair is "Len = 0"
    const RSPDocument::NameValuePair& first_pair = pairs.front();
    CPPUNIT_ASSERT(first_pair.first == "Len");
    CPPUNIT_ASSERT(first_pair.second == "0");
}   // end test_rsp

void TestSHAHash::test_256_short_message(void)
{
    SCRYFE::ByteArray input, output;
    SCRYFE::SHA256Hash hash;

    RSPDocument rsp_doc;
    rsp_doc.parse(fixtures_directory +
        "/shabytetestvectors/SHA256ShortMsg.rsp");
    
    // Test file is valid.
    CPPUNIT_ASSERT(rsp_doc.valid());
    
    // There is only a single section in the document, with lines grouped in
    // threes, being message length, the message itself, and the message
    // digest.
    //
    // Test these assumptions, just the make sure the file hasn't gotten
    // itself corrupted.
    CPPUNIT_ASSERT(rsp_doc.get_sections().size() == 1);
    const RSPDocument::Section& section = rsp_doc.get_sections().front();
    const RSPDocument::NameValuePairSequence& pairs = section.second;
    CPPUNIT_ASSERT(pairs.size() % 3 == 0);
    
    // Loop through the pairs in threes
    for (RSPDocument::NameValuePairSequence::const_iterator pair_itr =
            pairs.begin(); pair_itr != pairs.end(); pair_itr++)
    {
        // The first of the pairs is the message length.
        CPPUNIT_ASSERT(pair_itr->first == "Len");
        int length = atoi(pair_itr->second.c_str());
        pair_itr++;
        
        // The second is the message itself.
        CPPUNIT_ASSERT(pair_itr->first == "Msg");
        std::string message = pair_itr->second;
        
        // If the length is zero, it's a special case. The RSP file contains
        // the string "00", but it should be a zero-length string.
        if (length == 0) message = "";
        
        pair_itr++;
        
        // The third pair is the expected digest for the message.
        CPPUNIT_ASSERT(pair_itr->first == "MD");
        std::string expected_digest = pair_itr->second;
        
        // Now we can perform our test.
        input.clear();
        output.clear();
        if (length > 0) input.copy_from_hex(message);
        hash.process(input, output);
        CPPUNIT_ASSERT(output.to_hex_string() == expected_digest);
    }
}   // end test_256_short_message
  
void TestSHAHash::test_256_long_message(void)
{
    SCRYFE::ByteArray input, output;
    SCRYFE::SHA256Hash hash;

    RSPDocument rsp_doc;
    rsp_doc.parse(fixtures_directory +
        "/shabytetestvectors/SHA256LongMsg.rsp");
    
    // Test file is valid.
    CPPUNIT_ASSERT(rsp_doc.valid());
    
    // There is only a single section in the document, with lines grouped in
    // threes, being message length, the message itself, and the message
    // digest.
    //
    // Test these assumptions, just the make sure the file hasn't gotten
    // itself corrupted.
    CPPUNIT_ASSERT(rsp_doc.get_sections().size() == 1);
    const RSPDocument::Section& section = rsp_doc.get_sections().front();
    const RSPDocument::NameValuePairSequence& pairs = section.second;
    CPPUNIT_ASSERT(pairs.size() % 3 == 0);
    
    // Loop through the pairs in threes
    for (RSPDocument::NameValuePairSequence::const_iterator pair_itr =
            pairs.begin(); pair_itr != pairs.end(); pair_itr++)
    {
        // The first of the pairs is the message length.
        CPPUNIT_ASSERT(pair_itr->first == "Len");
        int length = atoi(pair_itr->second.c_str());
        pair_itr++;
        
        // The second is the message itself.
        CPPUNIT_ASSERT(pair_itr->first == "Msg");
        std::string message = pair_itr->second;
        
        // If the length is zero, it's a special case. The RSP file contains
        // the string "00", but it should be a zero-length string.
        if (length == 0) message = "";
        
        pair_itr++;
        
        // The third pair is the expected digest for the message.
        CPPUNIT_ASSERT(pair_itr->first == "MD");
        std::string expected_digest = pair_itr->second;
        
        // Now we can perform our test.
        input.clear();
        output.clear();
        if (length > 0) input.copy_from_hex(message);
        hash.process(input, output);
        CPPUNIT_ASSERT(output.to_hex_string() == expected_digest);
    }
}   // end test_256_long_message

void TestSHAHash::test_256_monte_carlo(void)
{
    SCRYFE::ByteArray input, output;
    SCRYFE::SHA256Hash hash;

    RSPDocument rsp_doc;
    rsp_doc.parse(fixtures_directory +
        "/shabytetestvectors/SHA256Monte.rsp");
    
    // Test file is valid.
    CPPUNIT_ASSERT(rsp_doc.valid());
    
    // There is one section in this document. After a line containing the
    // seed, the lines go in pairs, being the count and the digest at that
    // count.
    //
    // Test these assumptions, just the make sure the file hasn't gotten
    // itself corrupted.
    CPPUNIT_ASSERT(rsp_doc.get_sections().size() == 1);
    const RSPDocument::Section& section = rsp_doc.get_sections().front();
    const RSPDocument::NameValuePairSequence& pairs = section.second;
    CPPUNIT_ASSERT((pairs.size()-1) % 2 == 0);

    // Get the first line out - this is the seed input.
    RSPDocument::NameValuePairSequence::const_iterator pair_itr =
        pairs.begin();
    CPPUNIT_ASSERT(pair_itr->first == "Seed");
    std::string seed = pair_itr->second;
    input.copy_from_hex(seed);
    
    pair_itr++;
    
    // Loop through the pairs in twos
    for (; pair_itr != pairs.end(); pair_itr++)
    {
        // The first of the pairs is the count; we'll check it, just to
        // make sure that the file hasn't become corrupted.
        CPPUNIT_ASSERT(pair_itr->first == "COUNT");
        
        // Remove debugging code
        int count = atoi(pair_itr->first.c_str());
        
        pair_itr++;
        
        // The second pair is the expected message digest for this round.
        CPPUNIT_ASSERT(pair_itr->first == "MD");
        std::string expected_output = pair_itr->second;
        
        // The standard NIST Monte-Carlo test round consists of 1000 hash
        // operations, XORing each result with the previous 3.
        SCRYFE::ByteArray md0, md1, md2, md3;
        md0 = input;
        md1 = input;
        md2 = input;
        for (int i = 3; i < 1003; i++)
        {
            hash.process(md0 + md1 + md2, md3);
            
            md0 = md1;
            md1 = md2;
            md2 = md3;
        }
        
        output = md3;
        
        CPPUNIT_ASSERT(output.to_hex_string() == expected_output);
        
        // Recycle the output as input for the next round.
        input = output;
    }
}   // end test_256_monte_carlo
