/**
 * \file TestByteArray.cpp Tests for the ByteArray class
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */
 
#include <cstring>
#include <cppunit/extensions/HelperMacros.h>
#include <scryfe/scryfe.h>

/**
 * \brief Tests for the functions of the ByteArray class
 */
class TestByteArray : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestByteArray);
        CPPUNIT_TEST( test_basic_functions );
        CPPUNIT_TEST( test_xor );
        CPPUNIT_TEST( test_hex_translation );
    CPPUNIT_TEST_SUITE_END();
    
    /**
     * \brief Tests the basic functions of the byte array, creation,
     * retrieval, clearing and destruction
     */
    void test_basic_functions(void);
    
    /**
     * \brief Test bitwise xor
     */
    void test_xor(void);
    
    /**
     * \brief Test translation between byte arrays and hex strings
     */
    void test_hex_translation(void);
};  // end TestByteArray

CPPUNIT_TEST_SUITE_REGISTRATION( TestByteArray );

void TestByteArray::test_basic_functions(void)
{
    // Instantiate an empty byte array.
    SCRYFE::ByteArray a;
    CPPUNIT_ASSERT(a.size() == 0);
    
    // It should throw an exception if we attempt to access an element.
    CPPUNIT_ASSERT_THROW(a.at(1), SCRYFE::Error);
    
    // Put a string into the array, and check the result.
    const char* s = "abc";
    a.copy_from(s, strlen(s));
    
    CPPUNIT_ASSERT(a.size() == 3);
    CPPUNIT_ASSERT(a[0] == 'a');
    CPPUNIT_ASSERT(a[1] == 'b');
    CPPUNIT_ASSERT(a[2] == 'c');
    CPPUNIT_ASSERT_THROW(a.at(3), SCRYFE::Error);
    
    // Check copy and comparison.
    SCRYFE::ByteArray b;
    CPPUNIT_ASSERT(a != b);
    b = a;
    CPPUNIT_ASSERT(a == b);
    
    // Finally, check clearing.
    b.clear();
    CPPUNIT_ASSERT(b.size() == 0);
    CPPUNIT_ASSERT_THROW(b.at(1), SCRYFE::Error);
}   // end test

void TestByteArray::test_xor(void)
{
    // Check a single byte xor
    SCRYFE::ByteArray a("a", 1), b("a", 1);
    SCRYFE::ByteArray c = a ^ b;
    CPPUNIT_ASSERT(c.size() == 1);
    CPPUNIT_ASSERT(c[0] == 0);
    
    // Check with different sized arrays.
    a.copy_from("abcde", 5);
    b.copy_from("fgh", 3);
    c = a ^ b;
    
    CPPUNIT_ASSERT(c.size() == 3);
    CPPUNIT_ASSERT(c[0] == ('a' ^ 'f'));
    CPPUNIT_ASSERT(c[1] == ('b' ^ 'g'));
    CPPUNIT_ASSERT(c[2] == ('c' ^ 'h'));
}   // end test_xor

void TestByteArray::test_hex_translation(void)
{
    // Single byte
    SCRYFE::ByteArray a;
    a.copy_from_hex("ab");
    CPPUNIT_ASSERT(a.size() == 1);
    CPPUNIT_ASSERT(a[0] == 0xab);
    
    // Single char
    a.copy_from_hex("3");
    CPPUNIT_ASSERT(a.size() == 1);
    CPPUNIT_ASSERT(a[0] == 3);
    
    // Multiple bytes
    a.copy_from_hex("1234567890ABCDEF");
    CPPUNIT_ASSERT(a.size() == 8);
    CPPUNIT_ASSERT(a[0] == 0x12);
    CPPUNIT_ASSERT(a[1] == 0x34);
    CPPUNIT_ASSERT(a[7] == 0xef);
    
    // Check translation back to string.
    CPPUNIT_ASSERT(a.to_hex_string() == "1234567890abcdef");
    
    // Test an odd number of multiple chars.
    a.copy_from_hex("de123");
    CPPUNIT_ASSERT(a.size() == 3);
    CPPUNIT_ASSERT(a[0] == 0xde);
    CPPUNIT_ASSERT(a[1] == 0x12);
    CPPUNIT_ASSERT(a[2] == 0x3);
    
    // Check the translation back to a string - note that the trailing byte
    // is output with a leading zero.
    CPPUNIT_ASSERT(a.to_hex_string() == "de1203");
}   // end test_hex_translation
