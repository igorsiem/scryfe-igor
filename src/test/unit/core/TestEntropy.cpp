/**
 * \file TestEntropy.cpp Tests for the Entropy class
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include <sstream>
#include <time.h>
#include <cstring>

#include <cppunit/extensions/HelperMacros.h>
#include <scryfe/scryfe.h>

/**
 * \brief Tests for the functions of the Entropy class
 */
class TestEntropy : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestEntropy);
        CPPUNIT_TEST( test_shannon_function );
        CPPUNIT_TEST( test_pool );
    CPPUNIT_TEST_SUITE_END();
    
    /**
     * \brief Test the shannon entropy calculation
     */
    void test_shannon_function(void);
    
    /**
     * \brief Test the entropy pool
     *
     * This test simply cooks a few bytes, and checks that there is a
     * hashed chunk in the entropy pool.
     */
    void test_pool(void);
};  // end TestEntropy

CPPUNIT_TEST_SUITE_REGISTRATION(TestEntropy);

void TestEntropy::test_shannon_function(void)
{
    SCRYFE::ByteArray a;
    a.copy_from_hex("01020203030304040404");

    double e = SCRYFE::Entropy::shannon_entropy(a);
    
    // Print to a certain number of decimal places, to get around rounding
    // problems.
    std::stringstream entropy;
    entropy << e;
    
    CPPUNIT_ASSERT(entropy.str() == "1.84644");
}   // end test_shannon_function

void TestEntropy::test_pool(void)
{
    const char* chunk = "12345";
    SCRYFE::SHA256Hash hash;
    SCRYFE::Entropy entropy(hash, 10);
    
    // Initialise the entropy object. Its cooked pool is empty.
    CPPUNIT_ASSERT(entropy.cooked_size() == 0);
    CPPUNIT_ASSERT(entropy.cooked_entry_size() == hash.output_size());
    
    // Cook a chunk of data. The cooked pool should have one entry.
    entropy.cook(chunk, strlen(chunk));
    CPPUNIT_ASSERT(entropy.cooked_size() == 1);
    
    // Get the cooked chunk out. It's size should be the same as the hash
    // output size.
    SCRYFE::ByteArray cooked;
    entropy.pop_cooked(cooked);
    
    CPPUNIT_ASSERT(cooked.size() == hash.output_size());
    CPPUNIT_ASSERT(entropy.cooked_size() == 0);
}   // end test_pool
