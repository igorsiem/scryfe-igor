/**
 * \file TestOFBBlockMode.cpp Tests for the OFBBlockMode class
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */
 
#include <cstdlib>
#include <list>

#include <cppunit/extensions/HelperMacros.h>

#include <scryfe/scryfe.h>
#include <utils/RSPParser.h>

extern std::string fixtures_directory;

/**
 * \brief Tests for the functions of the OFBBlockMode class
 */
class TestOFBBlockMode : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestOFBBlockMode);
        CPPUNIT_TEST( test_aes256 );
    CPPUNIT_TEST_SUITE_END();

    /**
     * \brief Test the OFB block mode with the AES-256 cipher, across the
     * entire Known Answer Test (KAT) suite of response (RSP) files
     * (published by NIST).
     */
    void test_aes256(void);
};  // end TestAES256

CPPUNIT_TEST_SUITE_REGISTRATION( TestOFBBlockMode );

void TestOFBBlockMode::test_aes256(void)
{
    // Set up the cipher and block mode objects.
    SCRYFE::AES256Cipher cipher;
    SCRYFE::OFBBlockMode block_mode(cipher);
    
    // Loop through each of the RSP files
    std::list<std::string> rsp_filenames;
    rsp_filenames.push_back("OFBGFSbox256.rsp");
    rsp_filenames.push_back("OFBKeySbox256.rsp");
    rsp_filenames.push_back("OFBVarKey256.rsp");
    rsp_filenames.push_back("OFBVarTxt256.rsp");
    
    for (std::list<std::string>::const_iterator filename_itr =
            rsp_filenames.begin(); filename_itr != rsp_filenames.end();
            filename_itr++)
    {
        // Load the RSP file
        RSPDocument rsp_doc;
        rsp_doc.parse(fixtures_directory + "/KAT_AES/" + (*filename_itr));
        
        // Check that it has two sections, called "ENCRYPT" and "DECRYPT"
        CPPUNIT_ASSERT(rsp_doc.get_sections().size() == 2);
        const RSPDocument::Section& encrypt_section =
            rsp_doc.get_sections().front();
        const RSPDocument::Section& decrypt_section =
            rsp_doc.get_sections().back();
        CPPUNIT_ASSERT(encrypt_section.first == "ENCRYPT");
        CPPUNIT_ASSERT(decrypt_section.first == "DECRYPT");
        
        // Both the encrypt and decrypt sections consist of blocks of
        // five pairs, being a count (which is ignored), an encryption or
        // decryption key, an initialisation vector, the source (plaintext
        // or ciphertext), and the expected output (plaintext or ciphertext).
        CPPUNIT_ASSERT(encrypt_section.second.size() % 5 == 0);
        CPPUNIT_ASSERT(decrypt_section.second.size() % 5 == 0);
        
        // Loop through the encrypt pairs.
        for (RSPDocument::NameValuePairSequence::const_iterator pair_itr =
                encrypt_section.second.begin(); pair_itr !=
                encrypt_section.second.end(); pair_itr++)
        {
            // The first in the block is the count. It is ignored, but
            // checked for consistency.
            CPPUNIT_ASSERT(pair_itr->first == "COUNT");
            int count = atoi(pair_itr->second.c_str());
            
            // The second pair in the block is the encryption key.
            pair_itr++;
            CPPUNIT_ASSERT(pair_itr->first == "KEY");
            SCRYFE::ByteArray key;
            key.copy_from_hex(pair_itr->second);
         
            // The third pair is the initialisation vector.
            pair_itr++;
            SCRYFE::ByteArray iv;
            CPPUNIT_ASSERT(pair_itr->first == "IV");
            iv.copy_from_hex(pair_itr->second);
            
            // The fourth pair in the block is the plaintext.
            pair_itr++;
            CPPUNIT_ASSERT(pair_itr->first == "PLAINTEXT");
            SCRYFE::ByteArray plaintext;
            plaintext.copy_from_hex(pair_itr->second);
            
            // The last pair in the block is the expected ciphertext.
            pair_itr++;
            CPPUNIT_ASSERT(pair_itr->first == "CIPHERTEXT");
            SCRYFE::ByteArray expected_ciphertext;
            expected_ciphertext.copy_from_hex(pair_itr->second);

            // Initialise the block mode (which also initialises the cipher),
            // encrypt and compare the result.
            block_mode.initialise(iv, key, SCRYFE::encrypt);
            SCRYFE::ByteArray ciphertext;
            block_mode.process(plaintext, ciphertext);
            CPPUNIT_ASSERT(ciphertext == expected_ciphertext);
        }   // encrypt section loop
        
        // Now loop through the decrypt pairs.
        for (RSPDocument::NameValuePairSequence::const_iterator pair_itr =
                decrypt_section.second.begin(); pair_itr !=
                decrypt_section.second.end(); pair_itr++)
        {
            // The first in the block is the count. It is ignored, but
            // checked for consistency.
            CPPUNIT_ASSERT(pair_itr->first == "COUNT");
            int count = atoi(pair_itr->second.c_str());
            
            // The second pair in the block is the encryption key.
            pair_itr++;
            CPPUNIT_ASSERT(pair_itr->first == "KEY");
            SCRYFE::ByteArray key;
            key.copy_from_hex(pair_itr->second);
         
            // The third pair is the initialisation vector.
            pair_itr++;
            SCRYFE::ByteArray iv;
            CPPUNIT_ASSERT(pair_itr->first == "IV");
            iv.copy_from_hex(pair_itr->second);
            
            // The fourth pair in the block is the ciphertext.
            pair_itr++;
            CPPUNIT_ASSERT(pair_itr->first == "CIPHERTEXT");
            SCRYFE::ByteArray ciphertext;
            ciphertext.copy_from_hex(pair_itr->second);
            
            // The last pair in the block is the expected plaintext.
            pair_itr++;
            CPPUNIT_ASSERT(pair_itr->first == "PLAINTEXT");
            SCRYFE::ByteArray expected_plaintext;
            expected_plaintext.copy_from_hex(pair_itr->second);

            // Initialise the block mode (which also initialises the cipher),
            // decrypt and compare the result.
            block_mode.initialise(iv, key, SCRYFE::decrypt);
            SCRYFE::ByteArray plaintext;
            block_mode.process(ciphertext, plaintext);
            CPPUNIT_ASSERT(plaintext == expected_plaintext);
        }   // encrypt section loop
    }   // end file loop
}   // end test_aes256
