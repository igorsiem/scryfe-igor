/**
 * \file TestSHACipher.cpp Tests for the AES256Cipher class
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */
 
#include <cstdlib>
#include <list>

#include <cppunit/extensions/HelperMacros.h>

#include <scryfe/scryfe.h>
#include <utils/RSPParser.h>

extern std::string fixtures_directory;

/**
 * \brief Tests for the functions of the AES256Key class
 */
class TestAES256Cipher : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestAES256Cipher);
        CPPUNIT_TEST( test_block_size );
        CPPUNIT_TEST( test_ecb );
    CPPUNIT_TEST_SUITE_END();

    /**
     * \brief Simple test of block size reporting
     */
    void test_block_size(void);
    
    /**
     * \brief Test the AES-256 cipher against the entire Known Answer Test
     * KAT suite of response (RSP) files (published by NIST) in Electronc
     * Code Book (ECB) mode
     *
     * The RSP files are found in the `fixtures/KAT_AES` directory. Other
     * block cipher modes are tested in the block mode unit tests (e.g.,
     * `TestOFBBlockMode`).
     */
    void test_ecb(void);
};  // end TestAES256

CPPUNIT_TEST_SUITE_REGISTRATION( TestAES256Cipher );


void TestAES256Cipher::test_block_size(void)
{
    SCRYFE::AES256Cipher cipher;
    CPPUNIT_ASSERT(cipher.block_size() == 16);
}   // end test_block_size

void TestAES256Cipher::test_ecb(void)
{
    // Set up our cipher.
    SCRYFE::AES256Cipher cipher;

    // Loop through each of the RSP files
    std::list<std::string> rsp_filenames;
    rsp_filenames.push_back("ECBGFSbox256.rsp");
    rsp_filenames.push_back("ECBKeySbox256.rsp");
    rsp_filenames.push_back("ECBVarKey256.rsp");
    rsp_filenames.push_back("ECBVarTxt256.rsp");
    
    for (std::list<std::string>::const_iterator filename_itr =
            rsp_filenames.begin(); filename_itr != rsp_filenames.end();
            filename_itr++)
    {
        // Load the RSP file
        RSPDocument rsp_doc;
        rsp_doc.parse(fixtures_directory + "/KAT_AES/" + (*filename_itr));
        
        // Check that it has two sections, called "ENCRYPT" and "DECRYPT"
        CPPUNIT_ASSERT(rsp_doc.get_sections().size() == 2);
        const RSPDocument::Section& encrypt_section =
            rsp_doc.get_sections().front();
        const RSPDocument::Section& decrypt_section =
            rsp_doc.get_sections().back();
        CPPUNIT_ASSERT(encrypt_section.first == "ENCRYPT");
        CPPUNIT_ASSERT(decrypt_section.first == "DECRYPT");
        
        // The encryption section is comprised of blocks of four name-value
        // pairs, being the count, key, plaintext, and expected ciphertext.
        CPPUNIT_ASSERT(encrypt_section.second.size() % 4 == 0);
        
        // Loop through the pairs.
        for (RSPDocument::NameValuePairSequence::const_iterator pair_itr =
                encrypt_section.second.begin(); pair_itr !=
                encrypt_section.second.end(); pair_itr++)
        {
            // The first in the block is the count. It is ignored, but
            // checked for consistency.
            CPPUNIT_ASSERT(pair_itr->first == "COUNT");
            int count = atoi(pair_itr->second.c_str());
            
            // The second pair in the block is the encryption key.
            pair_itr++;
            CPPUNIT_ASSERT(pair_itr->first == "KEY");
            SCRYFE::ByteArray key;
            key.copy_from_hex(pair_itr->second);
            
            // The third pair in the block is the plaintext.
            pair_itr++;
            CPPUNIT_ASSERT(pair_itr->first == "PLAINTEXT");
            SCRYFE::ByteArray plaintext;
            plaintext.copy_from_hex(pair_itr->second);
            
            // The last pair in the block is the expected ciphertext.
            pair_itr++;
            CPPUNIT_ASSERT(pair_itr->first == "CIPHERTEXT");
            SCRYFE::ByteArray expected_ciphertext;
            expected_ciphertext.copy_from_hex(pair_itr->second);
            
            // Initialise the cipher, do our own encryption, and compare.
            cipher.initialise(key, SCRYFE::encrypt);
            SCRYFE::ByteArray ciphertext;
            cipher.process(plaintext, ciphertext);
            
            CPPUNIT_ASSERT(ciphertext == expected_ciphertext);
        }   // end encryption name/value pair loop
        
        // The decryption section has the same composition, except that
        // there is ciphertext followed by expected plaintext.
        CPPUNIT_ASSERT(decrypt_section.second.size() % 4 == 0);
        
        // Loop through the pairs.
        for (RSPDocument::NameValuePairSequence::const_iterator pair_itr =
                decrypt_section.second.begin(); pair_itr !=
                decrypt_section.second.end(); pair_itr++)
        {
            // The first in the block is the count. It is ignored, but
            // checked for consistency.
            CPPUNIT_ASSERT(pair_itr->first == "COUNT");
            int count = atoi(pair_itr->second.c_str());
            
            // The second pair in the block is the decryption key.
            pair_itr++;
            CPPUNIT_ASSERT(pair_itr->first == "KEY");
            SCRYFE::ByteArray key;
            key.copy_from_hex(pair_itr->second);
            
            // The third pair in the block is the ciphertext.
            pair_itr++;
            CPPUNIT_ASSERT(pair_itr->first == "CIPHERTEXT");
            SCRYFE::ByteArray ciphertext;
            ciphertext.copy_from_hex(pair_itr->second);
            
            // The last pair in the block is the expected plaintext.
            pair_itr++;
            CPPUNIT_ASSERT(pair_itr->first == "PLAINTEXT");
            SCRYFE::ByteArray expected_plaintext;
            expected_plaintext.copy_from_hex(pair_itr->second);
            
            // Now do our own decryption, and compare.
            cipher.initialise(key, SCRYFE::decrypt);
            SCRYFE::ByteArray plaintext;
            cipher.process(ciphertext, plaintext);
            
            CPPUNIT_ASSERT(plaintext == expected_plaintext);
        }   // end decryption name/value pair loop
    }   // end filename loop
}   // end test_ecb
