/**
 * \file TestCTRBlockMode.cpp Tests for the CTRBlockMode class
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include <list>
#include <string>

#include <cppunit/extensions/HelperMacros.h>
#include <scryfe/scryfe.h>

/**
 * \brief Tests for the functons of the CTRBlockMode class
 */
class TestCTRBlockMode : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestCTRBlockMode);
        CPPUNIT_TEST( test_aes256 );
    CPPUNIT_TEST_SUITE_END();
    
    /**
     * \brief Test CTR mode using the AES-256 KAT vectors published by NIST
     *
     * Note that we've only been able to source a NIST PDF document with
     * four KAT vectors for AES-256 and CTR. If other 'standard' KATs are
     * located, they should be added to this test.
     *
     * See http://csrc.nist.gov/publications/nistpubs/800-38a/sp800-38a.pdf
     */
    void test_aes256(void);
};  // end class TestCTRBlockMode

CPPUNIT_TEST_SUITE_REGISTRATION( TestCTRBlockMode );

void TestCTRBlockMode::test_aes256(void)
{
    // Set up the KAT vectors from the NIST publication. This pairs in this
    // list are in the order <plaintext, ciphertext>.
    std::list<std::pair<std::string, std::string> > kat_vectors;
    kat_vectors.push_back(std::make_pair(
        std::string("6bc1bee22e409f96e93d7e117393172a"),
        std::string("601ec313775789a5b7a7f504bbf3d228")));
    kat_vectors.push_back(std::make_pair(
        std::string("ae2d8a571e03ac9c9eb76fac45af8e51"),
        std::string("f443e3ca4d62b59aca84e990cacaf5c5")));
    kat_vectors.push_back(std::make_pair(
        std::string("30c81c46a35ce411e5fbc1191a0a52ef"),
        std::string("2b0930daa23de94ce87017ba2d84988d")));
    kat_vectors.push_back(std::make_pair(
        std::string("f69f2445df4f9b17ad2b417be66c3710"),
        std::string("dfc9c58db67aada613c2dd08457941a6")));

    // Set up the cipher and block mode, IV, nonce and key objects.
    SCRYFE::AES256Cipher cipher;
    SCRYFE::CTRBlockMode block_mode(cipher);
    SCRYFE::ByteArray key, iv;
    uint64_t nonce = 0xf8f9fafbfcfdfeff;
    
    key.copy_from_hex("603deb1015ca71be2b73aef0857d7781"
        "1f352c073b6108d72d9810a30914dff4");
    iv.copy_from_hex("f0f1f2f3f4f5f6f7");
    
    block_mode.initialise(iv, key, SCRYFE::encrypt, nonce);
        
    // Loop through the vectors, doing the encryption.
    for (std::list<std::pair<std::string, std::string> >::const_iterator
            itr = kat_vectors.begin(); itr != kat_vectors.end(); itr++)
    {
        SCRYFE::ByteArray plaintext, ciphertext;
        plaintext.copy_from_hex(itr->first);
        block_mode.process(plaintext, ciphertext);
        CPPUNIT_ASSERT(ciphertext.to_hex_string() == itr->second);
    }   // end encrypt loop
    
    // Reinitialise for decryption.
    block_mode.initialise(iv, key, SCRYFE::decrypt, nonce);
    
    for (std::list<std::pair<std::string, std::string> >::const_iterator
            itr = kat_vectors.begin(); itr != kat_vectors.end(); itr++)
    {
        SCRYFE::ByteArray plaintext, ciphertext;
        ciphertext.copy_from_hex(itr->second);
        block_mode.process(ciphertext, plaintext);
        CPPUNIT_ASSERT(plaintext.to_hex_string() == itr->first);
    }   // end encrypt loop
}   // end basic_test
