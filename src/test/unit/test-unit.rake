# File:         test-unit.rake
# Description:  Rake file for running the automated unit tests
# Author:       Igor Siemienowicz
# Copyright:    The content of this file is copyright (c) 2014 by the named
#               author(s), and licensed under terms of the Gnu Lesser General
#               Public License (LGPL). See the file `lgpl-3.0.txt` in the
#               `doc/licenses` directory for the full text of this license.

# Directory with the test executables (windows puts a Release subdirectory)
test_dir = "build/bin"
if (Rake::Win32::windows?)
    test_dir = "build/bin/Release"
end

namespace :test do
    desc "run all automated unit tests"
    task :unit => :binaries do
        sh "#{test_dir}/test-core src/test/unit/core/fixtures"
    end
    
    task :all => :unit
end # namespace test
