`unit` Test Directory
=====================
This directory is for automated unit tests of *SCryFE* code. Its
subdirectories are arranged to reflect the main `src` area so tests are
easy to find.

Index
-----
`core` - Tests of *SCryFE* core functionality
