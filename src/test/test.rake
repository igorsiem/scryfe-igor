# File:         test.rake
# Description:  Rake file for running the automated tests
# Author:       Igor Siemienowicz
# Copyright:    The content of this file is copyright (c) 2014 by the named
#               author(s), and licensed under terms of the Gnu Lesser General
#               Public License (LGPL). See the file `lgpl-3.0.txt` in the
#               `doc/licenses` directory for the full text of this license.

require 'pathname'

# Directory with the test executables (windows puts a Release subdirectory)
test_dir = "build/bin"
test_dir = "build/bin/Release" if $PLATFORM == :windows

Dir.glob("src/test/third-party/*.rake").each { |r| import r }
Dir.glob("src/test/unit/*.rake").each { |r| import r }
Dir.glob("src/test/integration/*.rake").each { |r| import r }

# Include testing in the 'all' target.
desc "run all tests"
task :test => "test:all"

task :all => :test
