`test` Directory
================
All test material, both code and artefacts for both automated and manual
tests are placed here.

Index
-----
*   `integration` - Automated integration tests

*   `third-party` - Tests of third-party components; these are generally to
    ensure that they have been integrated correctly
    
*   `unit` - Automated unit tests, arranged by code area

*   `utils` - Test utility code
