/**
 * \file TestRijndael.cpp Tests for the Rijndael reference implementation
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */
 
#include <cppunit/extensions/HelperMacros.h>
#include <scryfe/scryfe.h>
#include <rijndael/Rijndael.h>

/**
 * \brief Tests for the functions of the Rijndael block encryption algorithm
 */
class TestRijndael : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestRijndael);
        CPPUNIT_TEST( test_ecb );
    CPPUNIT_TEST_SUITE_END();
    
    /**
     * \brief Test Rijndael implementation with Electronic Code Book (ECB)
     * mode
     *
     * This method uses a sampling of 256-bit Known Answer Test (KAT)
     * vectors, published by NIST (http://csrc.nist.gov/groups/STM/cavp/)
     * 
     * Note: ECB is useless in real life, because it is not secure, but it
     * is good for testing.
     * 
     * \todo Update test to use the entire set of KAT vectors
     */
    void test_ecb(void);
};  // end TestRijndael

CPPUNIT_TEST_SUITE_REGISTRATION( TestRijndael );

void TestRijndael::test_ecb(void)
{
    // Use byte arrays for the key, plain and cipher texts
    SCRYFE::ByteArray key, plaintext, ciphertext;
    
    // This is the encryption working buffer.
    unsigned long rk[RKLENGTH(256)];
    
    // Read in the key and plaintext
    key.copy_from_hex("00000000000000000000000000000000000000000000000000000"
        "00000000000");
    plaintext.copy_from_hex("014730f80ac625fe84f026c60bfd547d");
    
    // Initialise the ciphertext to empty bytes.
    ciphertext.set_to_zero(16);
    
    // Initialise the working buffer, getting the number of encryption
    // 'rounds' at the same time.
    int rounds = rijndaelSetupEncrypt(rk, (const unsigned char*)key.bytes(),
        256);
    
    // Now we can encrypt!
    rijndaelEncrypt(rk, rounds, (const unsigned char*)plaintext.bytes(),
        (unsigned char*)ciphertext.bytes());
    
    // Check the ciphertext against the known answer.
    CPPUNIT_ASSERT(ciphertext.to_hex_string() ==
        "5c9d844ed46f9885085e5d6a4f94c7d7");
    
    // Now do the decryption round - same process.
    rounds = rijndaelSetupDecrypt(rk, (const unsigned char*)key.bytes(),
        256);
    
    plaintext.set_to_zero();
    
    rijndaelDecrypt(rk, rounds, (const unsigned char*)ciphertext.bytes(),
        (unsigned char*)plaintext.bytes());
    
    // Did we get our plaintext back again?
    CPPUNIT_ASSERT(plaintext.to_hex_string() ==
        "014730f80ac625fe84f026c60bfd547d");
    
    // ... rinse and repeat (with other KAT vectors).
    key.copy_from_hex("c47b0294dbbbee0fec4757f22ffeee3587ca4730c3d33b691df38"
        "bab076bc558");
    plaintext.copy_from_hex("00000000000000000000000000000000");
    ciphertext.set_to_zero();
    
    rounds = rijndaelSetupEncrypt(rk, (const unsigned char*)key.bytes(),
        256);
    rijndaelEncrypt(rk, rounds, (const unsigned char*)plaintext.bytes(),
        (unsigned char*)ciphertext.bytes());
    
    CPPUNIT_ASSERT(ciphertext.to_hex_string() ==
        "46f2fb342d6f0ab477476fc501242c5f");
    
    rounds = rijndaelSetupDecrypt(rk, (const unsigned char*)key.bytes(),
        256);
    plaintext.set_to_zero();
    
    rijndaelDecrypt(rk, rounds, (const unsigned char*)ciphertext.bytes(),
        (unsigned char*)plaintext.bytes());

    CPPUNIT_ASSERT(plaintext.to_hex_string() ==
        "00000000000000000000000000000000");
    
    // ... another sample...
    
    key.copy_from_hex("80000000000000000000000000000000000000000000000000000"
        "00000000000");
    plaintext.copy_from_hex("00000000000000000000000000000000");
    ciphertext.set_to_zero();
    
    rounds = rijndaelSetupEncrypt(rk, (const unsigned char*)key.bytes(),
        256);
    rijndaelEncrypt(rk, rounds, (const unsigned char*)plaintext.bytes(),
        (unsigned char*)ciphertext.bytes());
    
    CPPUNIT_ASSERT(ciphertext.to_hex_string() ==
        "e35a6dcb19b201a01ebcfa8aa22b5759");
    
    rounds = rijndaelSetupDecrypt(rk, (const unsigned char*)key.bytes(),
        256);
    plaintext.set_to_zero();
    
    rijndaelDecrypt(rk, rounds, (const unsigned char*)ciphertext.bytes(),
        (unsigned char*)plaintext.bytes());

    CPPUNIT_ASSERT(plaintext.to_hex_string() ==
        "00000000000000000000000000000000");

    // ... one more sample...
    
    key.copy_from_hex("00000000000000000000000000000000000000000000000000000"
        "00000000000");
    plaintext.copy_from_hex("80000000000000000000000000000000");
    ciphertext.set_to_zero();
    
    rounds = rijndaelSetupEncrypt(rk, (const unsigned char*)key.bytes(),
        256);
    rijndaelEncrypt(rk, rounds, (const unsigned char*)plaintext.bytes(),
        (unsigned char*)ciphertext.bytes());
    
    CPPUNIT_ASSERT(ciphertext.to_hex_string() ==
        "ddc6bf790c15760d8d9aeb6f9a75fd4e");
    
    rounds = rijndaelSetupDecrypt(rk, (const unsigned char*)key.bytes(),
        256);
    plaintext.set_to_zero();
    
    rijndaelDecrypt(rk, rounds, (const unsigned char*)ciphertext.bytes(),
        (unsigned char*)plaintext.bytes());

    CPPUNIT_ASSERT(plaintext.to_hex_string() ==
        "80000000000000000000000000000000");
}   // end test
