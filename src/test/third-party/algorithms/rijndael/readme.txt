`rijndael` Test
===============
This is the source code for a simple CppUnit test of the reference code for
the Rijndael reference implementation. The intent is to ensure that the
source code has been integrated correctly into *SCryFE* by running it with
a sample of the Known Answer Test (KAT) vectors published by NIST, and
comparing them with the known results.

The full set of relevant KAT vectors is used with the unit tests of the C++
wrapper classes for this algorithm.