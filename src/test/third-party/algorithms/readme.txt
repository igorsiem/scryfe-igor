`algorithms` Test Directory
===========================
This area contains executable tests for third-party encryption algorithms
that are used in *SCryFE*. The tests are written by the *SCryFE* team to
ensure that the third-party code has been integrated correctly.

Index
-----
*   `rijndael` - Test for the reference implementation of the Rijndael
    block cipher (also known as the AES encryption algorithm)

*   `sha` - Test for the IETF reference implementation of the SHA series of
    hashing algorithms
