`sha` Test
==========
This is a CppUnit test for the IETF reference implementation of the SHA
hashing algorithm. It tests against a sample of the published test vectors
from NIST to verify that the SHA code has been integrated into *SCryFE*
correctly.

The C++ wrapper class for this algorithm is tested against the full suite
of KAT vectors in its unit test.