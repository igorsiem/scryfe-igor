/**
 * \file main.cpp Entry point for the test executable
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */
 
#include <stdexcept>
#include <iostream>

#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>
 
/**
 * \brief Entry point for the test executable
 *
 * This function sets up the test framework, and runs the tests.
 * 
 * \param argc The number of command-line arguments (including the name
 * of the executable itself
 * 
 * \param argv The array of command-line argument strings
 *
 * \return 0 if all tests pass, -1 if there was a test failure, -2 if there
 * was a test error with a known exception, and -3 if an unrecognised
 * exception is thrown
 */
int main(int argc, char* argv[])
{
    int retcode = 0;
    try
    {
        std::cout << "SCryFE SHA Test" << std::endl <<
            "Copyright (c) 2014 Igor Siemienowicz, licensed under "
                "LGPL 3.0" << std::endl << std::endl;
                
        // Simple test run setup
		CppUnit::TextUi::TestRunner test_runner;
        CppUnit::TestFactoryRegistry& test_factory_registry =
			CppUnit::TestFactoryRegistry::getRegistry();
		test_runner.addTest(test_factory_registry.makeTest());
        
        // Run the test and check the result.
        int result = test_runner.run();
        if (result)
        {
            std::cout << "result: SUCCESS" << std::endl;
        }
        else
        {
            std::cout << "result: FAIL" << std::endl;
            return -1;
        }
    }
    catch (const std::exception& error)
    {
        std::cerr << "[ERROR] " << error.what() << std::endl;
        retcode = -2;
    }
    catch (...)
    {
        std::cerr << "[ERROR] *** unrecognised exception" << std::endl;
        retcode = -3;
    }
    
    return retcode;
}   // end main
