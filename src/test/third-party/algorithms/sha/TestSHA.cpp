/**
 * \file TestSHA.cpp Tests for the IETF SHA reference implementation
 *
 * Note that there is not corresponding header for this class, since it is
 * not referenced anywhere else. Class declaration and definition are both in
 * this file.
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */
 
#include <cppunit/extensions/HelperMacros.h>
#include <scryfe/scryfe.h>
#include <sha/sha.h>

/**
 * \brief Tests for the SHA sample implementation provided by the IETF
 * 
 * \todo Test against other lengths of SHA hash.
 */
class TestSHA : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TestSHA);
        CPPUNIT_TEST( test_sha256 );
        CPPUNIT_TEST( test_sha512 );
    CPPUNIT_TEST_SUITE_END();
    
    /**
     * Test the SHA-256 algorithm
     *
     * This method tests against a sample of the NIST SHA-256 test vectors
     * published at http://csrc.nist.gov/groups/STM/cavp/. 
     * 
     * \todo Update test to use the entire set of SHA-256 vectors
     */
    void test_sha256(void);
    
    /**
     * \brief Test the SHA-512 algorithm
     * 
     * This method tests against a sample of the NIST SHA-512 test vectors
     * published at http://csrc.nist.gov/groups/STM/cavp/. 
     * 
     * \todo Update test to use the entire set of SHA-512 vectors
     */
    void test_sha512(void);
};  // end TestRijndael

CPPUNIT_TEST_SUITE_REGISTRATION( TestSHA );

void TestSHA::test_sha256(void)
{
    SCRYFE::ByteArray input, output;
    SHA256Context context;
    input.copy_from_hex("d3");
    output.set_to_zero(SHA256HashSize);

    // Initialise the SHA hashing context, and provide the input.
    SHA256Reset(&context);
    int result = SHA256Input(&context, (const uint8_t*)input.bytes(),
        input.size());
    CPPUNIT_ASSERT(result == 0);
    
    // Retrieve the output, checking the result.
    result = SHA256Result(&context, (uint8_t*)output.bytes());
    CPPUNIT_ASSERT(result == 0);
    
    CPPUNIT_ASSERT(output.to_hex_string() == "28969cdfa74a12c82f3bad960b0b00"
        "0aca2ac329deea5c2328ebc6f2ba9802c1");
    
    // Try another, longer input
    SHA256Reset(&context);
    input.copy_from_hex("58e5a3259cb0b6d12c83f723379e35fd298b60");
    result = SHA256Input(&context, (const uint8_t*)input.bytes(),
        input.size());
    CPPUNIT_ASSERT(result == 0);
    
    result = SHA256Result(&context, (uint8_t*)output.bytes());
    CPPUNIT_ASSERT(result == 0);
    CPPUNIT_ASSERT(output.to_hex_string() == "9b5b37816de8fcdf3ec10b74542870"
        "8df8f391c550ea6746b2cafe019c2b6ace");
}   // end test_sha256

void TestSHA::test_sha512(void)
{
    SCRYFE::ByteArray input, output;
    SHA512Context context;
    input.copy_from_hex("21");
    output.set_to_zero(SHA512HashSize);
    
    // Initialise the SHA hashing context, and provide the input.
    SHA512Reset(&context);
    int result = SHA512Input(&context, (const uint8_t*)input.bytes(),
        input.size());
    CPPUNIT_ASSERT(result == 0);
    
    // Retrieve the output, checking the result.
    result = SHA512Result(&context, (uint8_t*)output.bytes());
    CPPUNIT_ASSERT(result == 0);
    
    CPPUNIT_ASSERT(output.to_hex_string() == "3831a6a6155e509dee59a7f451eb35"
        "324d8f8f2df6e3708894740f98fdee23889f4de5adb0c5010dfb555cda77c8ab5dc"
        "902094c52de3278f35a75ebc25f093a");
        
    // Try another, longer input
    SHA512Reset(&context);
    input.copy_from_hex("ca14e2ea2f37c78f78ef280f58707ec549a31a94361073e37701"
        "bfe503e4c01ee1f2e123e00e81a188f08fa050825709128a9b66bb8ae6ea47e4"
        "1d");
    result = SHA512Input(&context, (const uint8_t*)input.bytes(),
        input.size());
    CPPUNIT_ASSERT(result == 0);
    
    result = SHA512Result(&context, (uint8_t*)output.bytes());
    CPPUNIT_ASSERT(result == 0);
    CPPUNIT_ASSERT(output.to_hex_string() == "4600e022a02258739f67fdd367cc1e"
        "662631fb087918768352062b9b3c8de8dbca0e9ec751b91f284694fbddb8d325c06"
        "37bccb21dd2efa92e48dbab2e5e9c26");
}   // end test_sha512
