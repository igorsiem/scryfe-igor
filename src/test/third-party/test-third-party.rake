# File:         test-third-party.rake
# Description:  Rake file for running the automated third-party tests
# Author:       Igor Siemienowicz
# Copyright:    The content of this file is copyright (c) 2014 by the named
#               author(s), and licensed under terms of the Gnu Lesser General
#               Public License (LGPL). See the file `lgpl-3.0.txt` in the
#               `doc/licenses` directory for the full text of this license.

# Directory with the test executables (windows puts a Release subdirectory)
test_dir = "build/bin"
if (Rake::Win32::windows?)
    test_dir = "build/bin/Release"
end

namespace :test do
    desc "run all automated tests of third-party components"
    task :third_party => :binaries do
        sh "#{test_dir}/test-rijndael"
        sh "#{test_dir}/test-sha"
    end
    
    task :all => :third_party
end # namespace test
