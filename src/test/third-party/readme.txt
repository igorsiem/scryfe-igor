`third-party` Test Directory
============================
This area contains tests for various third-party componets. These are
generally to demonstrate that these have been integrated correctly into
*SCryFE*.

Index
-----
*   `algorithms` - Tests for third-party encryption algorithm code