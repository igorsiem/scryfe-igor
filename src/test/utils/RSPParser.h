/**
 * \file RSPParser.h Declares the RSPParser class
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include <string>
#include <list>
 
#ifndef _RSPParser_h_included
#define _RSPParser_h_included

/**
 * \brief Parsing and encoding of NIST Known Answer Test (KAT) Respose (RSP)
 * files.
 *
 * RSP files are simple, text-based name/value pairs with comment lines that
 * start with a hash character, and the majority of the values being strings.
 *
 * One important thing to note is that ORDER of lines is important - there
 * are groups of pairs that need to remain together, and these groups all
 * have similar sets of names in their name/value pairs. Name/value pairs are
 * therefore stored in lists.
 *
 * This class encapsulates the RSP document, which can be parsed from the
 * disk, and its elements read from internal structures.
 */
class RSPDocument
{
    // --- External interface ---
    
    public:
    
    // -- Sub-types --
    
    /**
     * \brief A pair of strings, used for name/value pairs
     */
    typedef std::pair<std::string, std::string> NameValuePair;
    
    /**
     * \brief A sequence of name/value pairs (remembering that these cannot
     * be (easily) indexed
     */
    typedef std::list<NameValuePair> NameValuePairSequence;
    
    /**
     * \brief An RSP file 'section', with a name and sequence of name/value
     * pairs
     */
    typedef std::pair<std::string, NameValuePairSequence> Section;
    
    /**
     * \brief A document is essentially a sequence of sections
     */
    typedef std::list<Section> SectionSequence;
    
    // -- Methods --
    
    /**
     * \brief Constructor, optionally parsing a file
     *
     * If the file is parsed successfully, the 'valid' flag is set to true.
     * If there was a problem, or no filename is supplied, 'valid' is false.
     *
     * This is in contrast to the 'parse' method, which will throw an
     * exception if there is a problem parsing a file. We don't throw an
     * exception here, because this causes problems for some compilers.
     *
     * \param filename The (optional) name of the file to parse as an RSP
     * file
     */
    explicit RSPDocument(const std::string& filename = "");
    
    // Default destructor is OK - everything will clean itself up
    
    /**
     * \brief Whether or not a valid RSP document has been parsed
     *
     * \return True if an RSP document has been parsed successfully
     */
    bool valid(void) const { return m_valid; }
    
    /**
     * \brief Retrieve the parsed filename
     *
     * This is empty if an RSP file has not be parsed
     *
     * \return The RSP filename
     */
    const std::string& get_filename(void) const { return m_filename; }
    
    /**
     * \brief Const access to the sequence of sections that were parsed from
     * the document
     *
     * \return A reference to the section sequence
     */
    const SectionSequence& get_sections(void) const { return m_sections; }
    
    /**
     * \brief Parse the named RSP document (clearing the sections sequence
     * beforehand)
     *
     * The `valid` flag is set to true if the operation completed
     * successfully, or false if there was a problem.
     *
     * \param filename The name of the RSP document to parse
     *
     * \throws std::runtime_error Signals that a problem occurred when
     * attempting to parse the RSP file
     */
    void parse(const std::string filename);
    
    // --- Internal declarations ---
    
    private:
    
    // -- Attributes --
    
    /**
     * \brief Whether or not an RSP document has been parsed successfully
     */
    bool m_valid;
    
    /**
     * \brief The name of the RSP document file
     *
     * This may be blank if a file has not been parsed.
     */
    std::string m_filename;
    
    /**
     * \brief The section sequence of the document (often just a single
     * section)
     */
    SectionSequence m_sections;
};  // end class RSPDocument

#endif
