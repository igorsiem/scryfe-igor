test utils readme
=================
This directory contains utility code that is used across the test suite, and
is compiled into a `test-utils` library. The functionality of this suite is
only tested informally, as part of other automated tests. For example, the
RSP document parser is tested as part of the core unit tests.

*   `RSPParser.*` - RSPDocument class - a parser for NIST Known Answer Test
    (KAT) Response (RSP) files