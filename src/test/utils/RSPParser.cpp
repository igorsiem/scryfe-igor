/**
 * \file RSPParser.h Implements the RSPParser class
 *
 * \author Igor Siemienowicz
 *
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include <stdexcept>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <utility>
#include <fstream>
#include <vector>
#include <sstream>

#include "../../utils/string_utils.h"
#include "RSPParser.h"

RSPDocument::RSPDocument(const std::string& filename) :
    m_valid(false),
    m_filename(),
    m_sections()
{
    if (filename.empty() == false)
    {
        // We want to parse a file. Make sure all exceptions are caught.
        try
        {
            parse(filename);
        }
        catch (...)
        {
            m_valid = false;
            
            // We don't rethrow, because we are in a constructor, and some
            // compilers don't like that.
        }
    }   // end if we have a filename
}   // end constructor

void RSPDocument::parse(const std::string filename)
{
    m_filename = filename;
    m_valid = false;
            
    // Attempt to open the file in text mode.
    std::ifstream file(filename.c_str());
    if (!file)
        throw std::runtime_error("could not open file \"" + filename +
            "\" for reading");
            
    // Clear our existing sequence of sections, and build up a new sequence,
    // starting with one that has a blank name.
    m_sections.clear();
    Section section;
    
    // Read the file line-by-line, creating sections as we go.
    std::string line;
    std::size_t line_number = 0;
    
    while (std::getline(file, line))
    {
        line_number++;

        // First, trim leading and trailing whitespace.
        SCRYFE::trim_ws(line);
        
        // Is it a comment or empty?
        if ((line.empty() == false) && (line[0] != '#'))
        {
            // Is it a section heading?
            if (line[0] == '[')
            {
                // It is a section heading - separate it out - need to find
                // the end position.
                std::size_t last_pos = line.find_first_of(']', 1);
                if (last_pos == std::string::npos)
                {
                    std::stringstream message;
                    message << "error in line " << line_number << " of "
                        "file \"" << filename << "\": section heading "
                        "has not closing square bracket (]) character";
                    throw std::runtime_error(message.str());
                }
                
                std::string section_name = line.substr(1, last_pos-1);
                
                // Have we been building a section already? If so, add it to
                // our sequence, and start with a fresh, empty section.
                if (section.second.size() > 0)
                {
                    m_sections.push_back(section);
                    section.first.clear();
                    section.second.clear();
                }
                
                section.first = section_name;
            }   // end if it is a section heading line
            else
            {
                // It's not an empty, comment or section-heading line. Assume
                // it is a <name>=<value> line.
                std::vector<std::string> elements;
                SCRYFE::split(line, '=', elements);
                
                if (elements.size() != 2)
                {
                    std::stringstream message;
                    message << "error in line " << line_number << " of "
                        "file \"" << filename << "\": could not parse \"" <<
                        line << "\" into <name> and <value> components";
                    throw std::runtime_error(message.str());
                }
                
                std::string name = elements[0];
                std::string value = elements[1];
                
                // Strip away any whitespace and trailing comments.
                SCRYFE::trim_ws(name);
                
                elements.clear();
                value = SCRYFE::split(value, '#', elements)[0];
                SCRYFE::trim_ws(value);
                
                // Add the line to the current section
                section.second.push_back(std::make_pair(name, value));
            }   // end if it is a normal <name>=<value> line
        }   // end if it is NOT a comment
    }   // end loop through the lines in the file
    
    // We've been through the entire file - do we have a section that needs
    // to be added?
    if (section.second.size() > 0)
        m_sections.push_back(section);
        
    // If we get to here, everything was successful.
    m_valid = true;
}   // end parse
