`src` Directory
===============
This is the top-level directory for all *SCryFE* source code.

Index
-----
*   `core` - Core functionality for SCryFE

*   `include` - Single-point include structure - see `readme.txt` in that
    directory
    
*   `test` - Code and artefacts for all tests, both automated and manual

*   `third-party` - Third-party components, not created by the *SCryFE* team

*   `utils` - Various pieces of utility code that are used across
    everything else; this is compiled in the the main *SCryFE* library