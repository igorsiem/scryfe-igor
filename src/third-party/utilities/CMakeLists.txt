# File:         CMakeLists.txt
# Description:  CMake file for the third-party utilities area; most of these
#               components are built as individual libraries at this level,
#               rather than included in the top-level `scryfe` library
# Author:       Igor Siemienowicz
# Copyright:    The content of this file is copyright (c) 2014 by the named
#               author(s), and licensed under terms of the Gnu Lesser General
#               Public License (LGPL). See the file `lgpl-3.0.txt` in the
#               `doc/licenses` directory for the full text of this license.

add_subdirectory(cppunit)
