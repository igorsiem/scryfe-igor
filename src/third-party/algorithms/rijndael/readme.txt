Rijndael Source Code
====================
The code in this directory is the reference implementation of the Rijndael
encryption algorithm, now known as the Advanced Encryption Standard (AES). It
is public domain software. See [the author's home
page](http://www.efgh.com/software/rijndael.htm) for more information.
