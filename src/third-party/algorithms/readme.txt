`algorithms` Directory
======================
This directory contains code and components that implement various
cryptographic algorithms.

Note that this components in this area are all included in the top-level
`scryfe` library, so this area does not have its own CMake file.

Index
-----
*   `rijndael` - Reference implementation for the Rijndael cryptographic
    algorithm, now known as the Advanced Encryption Standard (AES).
    
*   `sha` - IETF reference implementation of the SHA series of
    cryptographically secure hashing algorithms