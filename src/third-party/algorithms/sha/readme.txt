SHA Source Code
===============
This is the part of the IETF reference implementation of the SHA set of
cryptographically secure hashing algorithms provided with
[RFC-6234](http://tools.ietf.org/html/rfc6234). It is free to use and
distribute, provided the citation at the top of the header file is retained,
and distributed with all binary distributions. 