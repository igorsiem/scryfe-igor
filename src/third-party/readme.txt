`third-party` Directory
=======================
This directory compnents components of *SCryFE* not created by the *SCryFE*
project team.

*Please note:* Licensing is important in this project. When adding a
component in this area, *please* make sure that it has a license that is
compatible with LGPL 3.0 (usually easy to check by looking up the relevant
license in Wikipedia) and update the `doc/licenses/license.md` document
accordingly.

Index
-----
*   `algorithms` - Code and components for various cryptographic algorithms.

*   `utilities` - Utility components, such as the CppUnit unit testing
    library