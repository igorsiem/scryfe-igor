/**
 * \file string_utils.h Implements a number of string utilities that are
 * used throughout the *SCryFE* library
 */

#include "string_utils.h"
#include <algorithm> 
#include <cctype>
#include <sstream>

namespace SCRYFE {

std::string& ltrim_ws(std::string& s)
{
    s.erase(
        s.begin(),
        std::find_if(
            s.begin(),
            s.end(),
            std::not1(
                std::ptr_fun<int, int>(std::isspace))));
                
    return s;
}   // end ltrim_ws
    
std::string& rtrim_ws(std::string& s)
{
    s.erase(
        std::find_if(
            s.rbegin(),
            s.rend(),
            std::not1(
                std::ptr_fun<int, int>(std::isspace))).base(),
        s.end());
        
    return s;
}   // end rtrim_ws
    
std::string& trim_ws(std::string& s)
{
    return ltrim_ws(rtrim_ws(s));
}   // end trim_ws

std::vector<std::string>& split(
            const std::string &s,
            char delim,
            std::vector<std::string> &elems)
{
    // Use a stringstream to tokenize the string
    std::stringstream ss(s);
    std::string item;
    
    while (std::getline(ss, item, delim))
    {
        elems.push_back(item);
    }
    
    return elems;
}   // end split

std::vector<std::string> split(const std::string &s, char delim)
{
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}   // end split

}   // end namespace SCRYFE
