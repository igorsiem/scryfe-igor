/**
 * \file string_utils.h Declares a number of string utilities that are
 * used throughout the *SCryFE* library
 */
 
#include <string>
#include <vector>

#ifndef _scryfe_string_utils_h_included
#define _scryfe_string_utils_h_included

namespace SCRYFE {

/**
 * \brief Left-trim of whitespace from a string
 *
 * Note that whitespace after the first non-whitespace character in the
 * string is ignored. Whitespace is defined by the `std::isspace` function.
 *
 * \param s The string to trim
 *
 * \return Reference to same trimmed string object
 */
extern std::string& ltrim_ws(std::string& s);
    
/**
 * \brief Right-trim of whitespace from a string
 *
 * Note that whitespace before the last non-whitespace character in the
 * string is ignored. Whitespace is defined by the `std::isspace` function.
 *
 * \param s The string to trim
 *
 * \return Reference to same trimmed string object 
 */
extern std::string& rtrim_ws(std::string& s);
    
/**
 * \brief Trim whitespace from both ends of string
 *
 * Note that whitespace after the first non-whitespace character and before
 * the last non-whitespace character is ignored.
 *
 * \param s The string to trim
 *
 * \return Reference to the same trimmed string object
 */
extern std::string& trim_ws(std::string& s);
    
/**
 * \brief Split a string into parts, with a given token as a delimiter
 * 
 * This version of the method uses preconstructed container that is passed
 * in as an argument, and returned back (same reference). Note that
 * consecutive instances of the delimiter will result in empty elements
 * (which are not skipped).
 * 
 * \param s The string to split
 * 
 * \param delim The character on which to split
 * 
 * \param elems The result vector
 * 
 * \return A reference to the elems vector, all filled
 */
extern std::vector<std::string> &split(
        const std::string &s,
        char delim,
        std::vector<std::string> &elems);

/**
 * \brief Split a string into parts with a given token as a delimiter
 * 
 * This version of the method constructs a container on the fly and returns
 * a copy.  Note that consecutive instances of the delimiter will result in
 * empty elements (which are not skipped).
 * 
 * \param s The string to split
 * 
 * \param delim The delimiter to use to split the string
 * 
 * \return The split elements
 */
extern std::vector<std::string> split(const std::string &s, char delim);

}   // end namespace SCRYFE

#endif
