# File:         build.rake
# Description:  Rake file for building binaries from source, using CMake and
#               platform-specific compile / build systems
# Author:       Igor Siemienowicz
# Copyright:    The content of this file is copyright (c) 2014 by the named
#               author(s), and licensed under terms of the Gnu Lesser General
#               Public License (LGPL). See the file `lgpl-3.0.txt` in the
#               `doc/licenses` directory for the full text of this license.

require 'zip'

# --- TASKS ---

# Make sure the build directory exists
directory BUILD_DIR

# QT path
if ENV['QT_DIR'] == nil
    ENV['QT_DIR'] = "/opt/Qt"
    ENV['QT_DIR'] = "C:/Qt/" if $PLATFORM == :windows
end

if File.directory?(ENV['QT_DIR']) == false
    raise "could not find Qt installation at the standard location " +
        "(\"#{ENV['QT_DIR']}\"); please set the 'QT_DIR' environment " +
        "variable to you QT install location"
end

# Cmake stuff
task :run_cmake => BUILD_DIR do
    # We need to run cmake from the build directory, so change to that
    # location now.
    old_pwd = Dir.pwd
    Dir.chdir BUILD_DIR
    
    # Platform specific stuff.
    cmake_command = "cmake ../src"
    if $PLATFORM == :windows
        # Check that Visual Studio has been installed, and that the vcvars
        # batch file has been called. There is no way to call it from the
        # Ruby script, and have the environment variables carry over. There
        # might be a clean or not-so-clean way around this, but I'm not sure
        # what it is at this point. See issues #1 and #2.
        if ENV['VS100COMNTOOLS'] == nil
            raise "could not find environment variable 'VS100COMNTOOLS' - " +
                "is VS2010 installed?"
        end

        if ENV['VCINSTALLDIR'] == nil
            raise "could not find environment variable 'VCINSTALLDIR' - " +
                "have you run 'vcvars.bat' or 'vcvars32.bat' (see issue #2)?"
        end
        
        cmake_command = "cmake -G \"Visual Studio 10\" ..\\src"
        
    end
    
    sh cmake_command
    
    Dir.chdir old_pwd
end

task :run_make => :run_cmake do
    old_pwd = Dir.pwd
    Dir.chdir BUILD_DIR

    # The make command is platform-specific
    make_command = "make"
    
    if $PLATFORM == :windows
        make_command = "msbuild SCryFE.sln /p:Configuration=Release"
    end
        
    sh make_command

    Dir.chdir old_pwd
end

# If we're in windows, we need a number of Qt DLLs. We copy them to the
# release area so that we can run from there, and they also need to be
# deployed in the installation package. The following creates rules for
# these DLLs.
dll_dependencies = []
if $PLATFORM == :windows
    dlls = [
        "icudt52.dll",
        "icuin52.dll",
        "icuuc52.dll",
        "Qt5Gui.dll",
        "Qt5Core.dll",
        "Qt5Widgets.dll"]
        
    directory "#{BUILD_DIR}/bin/Release"
                
    dlls.each do |dll_name|
        file "#{BUILD_DIR}/bin/Release/#{dll_name}" =>
                ["#{ENV['QT_DIR']}/5.3/msvc2010_opengl/bin/#{dll_name}",
                "#{BUILD_DIR}/bin/Release"] do
            puts "Copying #{dll_name}..."
                
            FileUtils.cp(
                "#{ENV['QT_DIR']}/5.3/msvc2010_opengl/bin/#{dll_name}",
                "#{BUILD_DIR}/bin/Release")
                
            puts "    ... done"
        end
        
        dll_dependencies << "#{BUILD_DIR}/bin/Release/#{dll_name}"
    end # each dll
end # if windows

namespace :binaries do
    desc "set up the build environment and project files"
    task :environment => :run_cmake
    
    desc "build all binary targets"
    task :make => :run_make
    
    task :all => [:environment, :make]
    task :all => dll_dependencies
end

desc "build all binary targets"
task :binaries => "binaries:all"

# --- Executable Installer ---

# This is the installable executable name.
executable_installer_name = \
    "SCryFE-#{$SCRYFE_VERSION}-debian-x#{$WORDSIZE}.deb"

if $PLATFORM == :windows
    # If it is windows, note that at the moment, we are only supporting
    # 32-bit windows.
    #
    # TODO 64-bit windows support
    executable_installer_name = "SCryFE-#{$SCRYFE_VERSION}-windows-x32.exe"
end

# TODO OSX

file "#{DELIVERABLES_DIR}/#{executable_installer_name}" =>
        [:binaries, "docs:release_notes", "docs:user"] do
    # Run a make for the packaging
    old_pwd = Dir.pwd
    Dir.chdir BUILD_DIR

    make_command = "make package"
    
    if $PLATFORM == :windows
        make_command = "msbuild PACKAGE.vcxproj " +
            "/p:Configuration=Release"
    end
    
    # TODO OSX version of packaging
    
    sh make_command

    Dir.chdir old_pwd
    
    # TODO repackage deb file to get around permissions bug
    
    # Move the package to the deliverables area
    FileUtils.mv("#{BUILD_DIR}/#{executable_installer_name}",
        "#{DELIVERABLES_DIR}")
end

# --- Zip user distro ---

zip_filename = "SCryFE-#{$SCRYFE_VERSION}-linux-x#{$WORDSIZE}.zip"
if $PLATFORM == :windows
    # If it is windows, note that at the moment, we are only supporting
    # 32-bit windows.
    #
    # TODO 64-bit windows support
    zip_filename = "SCryFE-#{$SCRYFE_VERSION}-windows-x32.zip"
end

# TODO OSX support

file "#{DELIVERABLES_DIR}/#{zip_filename}" =>
        [:binaries, "docs:user", "docs:release_notes"] do
    # Delete the old one if it exists.
    if File.exists?("#{DELIVERABLES_DIR}/#{zip_filename}")
        FileUtils.rm("#{DELIVERABLES_DIR}/#{zip_filename}")
    end
    
    Zip::File.open("#{DELIVERABLES_DIR}/#{zip_filename}",
            Zip::File::CREATE) do |zipfile|
        # When there's lots more files to go in, we might abstract everything
        # out into a package manifest structure, but we only have a few files
        # here, so we'll just list them explicitly.
        #
        # First, the executables
        if $PLATFORM == :windows
            zipfile.add("scryfe-cli.exe",
                        "#{BUILD_DIR}/bin/Release/scryfe-cli.exe");
            zipfile.add("scryfe-gui.exe",
                        "#{BUILD_DIR}/bin/Release/scryfe-gui.exe");
            
            # For windows, we also have the Qt DLLs, which have been copied
            # to the binaries area as well.
            zipfile.add("icudt52.dll",
                "#{BUILD_DIR}/bin/Release/icudt52.dll")
            zipfile.add("icuin52.dll",
                "#{BUILD_DIR}/bin/Release/icuin52.dll")
            zipfile.add("icuuc52.dll",
                "#{BUILD_DIR}/bin/Release/icuuc52.dll")
            zipfile.add("Qt5Gui.dll", "#{BUILD_DIR}/bin/Release/Qt5Gui.dll")
            zipfile.add("Qt5Core.dll",
                "#{BUILD_DIR}/bin/Release/Qt5Core.dll")
            zipfile.add("Qt5Widgets.dll",
                "#{BUILD_DIR}/bin/Release/Qt5Widgets.dll")
        else
            zipfile.add("scryfe-cli",
                "#{BUILD_DIR}/bin/scryfe-cli")
            zipfile.add("scryfe-gui",
                "#{BUILD_DIR}/bin/scryfe-gui")
        end
        
        # Now the documentation
        zipfile.add("scryfe-cli-Users-Manual.pdf",
            "#{BUILD_DIR}/doc/user/scryfe-cli-Users-Manual.pdf")
        zipfile.add("SCryFE-User-Manual.pdf",
            "#{BUILD_DIR}/doc/user/SCryFE-User-Manual.pdf")
        zipfile.add("release-notes.txt",
            "#{BUILD_DIR}/doc/release-notes.txt")
        
        # For posix platforms, there's the man file
        if $PLATFORM != :windows
            zipfile.add("scryfe-cli.1", "#{BUILD_DIR}/doc/user/scryfe-cli.1")
        end
    end # zip file creation
end # file task

# --- Zip SDK ---

# The SDK is 'platform independent', in that it contains no compiled
# binaries - just source code.
sdk_zip_filename = "SCryFE-SDK-#{$SCRYFE_VERSION}.zip"
file "#{DELIVERABLES_DIR}/#{sdk_zip_filename}" => [
        DELIVERABLES_DIR, "docs:developer", "docs:release_notes"] do
    puts "zipping SDK..."
    
    # Delete the old one if it exists.
    if File.exists?("#{DELIVERABLES_DIR}/#{sdk_zip_filename}")
        FileUtils.rm("#{DELIVERABLES_DIR}/#{sdk_zip_filename}")
    end

    
    Zip::File.open("#{DELIVERABLES_DIR}/#{sdk_zip_filename}",
            Zip::File::CREATE) do |zipfile|
        # Do the source directories
        sdk_src_dirs = [
            "src/core",
            "src/third-party/algorithms",
            "src/include"]
        sdk_src_dirs.each do |dirname|
            Dir[File.join(dirname, '**', '**')].each do |file|
                zipfile.add(file.sub("src/", ""), file)
                puts "    zipped \"#{file}\""
            end
        end # each SDK src dir
        
        # Do the licenses
        Dir[File.join("doc/licenses", "**", "*.txt")].each do |file|
            zipfile.add(file.sub("doc/", ""), file)
            puts "    zipped \"#{file}\""
        end # each license file
        
        # Now the documentation
        Dir[File.join("#{BUILD_DIR}/doc/developer", "**", "**")].each do
                |file|
            zipfile.add(
                File.join(
                    "doc",
                    file.sub("#{BUILD_DIR}/doc/developer", "")),
                file)
            puts "    zipped \"#{file}\""
        end
        
        # Readme and release notes
        zipfile.add("release-notes.txt",
            "#{BUILD_DIR}/doc/release-notes.txt")
        puts "    zipped \"doc/release-notes.txt\""
        
        zipfile.add("readme.txt", "doc/developer/sdk-readme.txt")
        puts "    zipped \"doc/developer/sdk-readme.txt\""
    end # zipfile
    
    puts "... SDK complete"
end

# --- Group up the tasks into namespaces ---

namespace :package do
    desc "create the executable installer package"
    task :installer => "#{DELIVERABLES_DIR}/#{executable_installer_name}"
    
    desc "create the zipped binaries file"
    task :zip => "#{DELIVERABLES_DIR}/#{zip_filename}"
    
    desc "create the SDK"
    task :sdk => "#{DELIVERABLES_DIR}/#{sdk_zip_filename}"
    
    task :all => [:installer, :zip, :sdk]
end

task :package => "package:all"

# Add the packages to the deliverables task
task :deliverables => :package
