/**
 * \file Keyphrase.h Code for getting an encryption key phrase securely from
 * the console
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cstring>

#ifdef _WIN32
    #include <conio.h>
#else
    #include <unistd.h>
    #include <termios.h>
#endif
 
#include "Keyphrase.h"

void get_keyphrase(
        SCRYFE::ByteArray& keyphrase,
        const std::string& prompt,
        char replacement)
{
    std::cout << prompt;

    // Allocate a temporary buffer.
    SCRYFE::ByteArray temp_buffer;
    temp_buffer.set_to_zero(20);
    std::size_t actual_length = 0;  // Actual length of keyphrase
    char c = 0;
    
// Do this differently in windows and posix
#ifdef _WIN32

    // Loop until we get a carriage return or EOF.
    while (((c = _getch()) != '\r') && (c != '\n') && (c != EOF))
    {
        // Do we need more room in the temp buffer?
            // If we need more room, allocate it here.
            if (actual_length >= temp_buffer.size())
            {
                SCRYFE::ByteArray new_temp_buffer;
                new_temp_buffer.set_to_zero(temp_buffer.size() + 20);
                memcpy(
                    new_temp_buffer.bytes(),
                    temp_buffer.bytes(),
                    temp_buffer.size());
                    
                temp_buffer = new_temp_buffer;
            } 

            // Store the character, and print a replacement.
            temp_buffer[actual_length++] = c; // note postfix increment
            if (replacement) std::cout << replacement;            
    }   // end while we haven't finished.

// --- END WINDOWS
#else
// --- BEGIN POSIX

	struct termios tty_attr;
	
	if (tcgetattr(STDIN_FILENO, &tty_attr) < 0)
		throw std::runtime_error("failed to retrieve attributes for the "
            "standard input console");
            
    // Store attributes so we can restore them later.
	const tcflag_t c_lflag = tty_attr.c_lflag;
	tty_attr.c_lflag &= ~ICANON;
	tty_attr.c_lflag &= ~ECHO;

	if (tcsetattr(STDIN_FILENO, 0, &tty_attr) < 0)
		throw std::runtime_error("failed to change attributes for the "
            "standard input console");

    // Make sure everything gets restored back, even if an exception is
    // thrown.
    try
    {
        while ((c = getchar()) != '\n' && c != EOF && c != '\r')
        {
            // If we need more room, allocate it here.
            if (actual_length >= temp_buffer.size())
            {
                SCRYFE::ByteArray new_temp_buffer;
                new_temp_buffer.set_to_zero(temp_buffer.size() + 20);
                memcpy(
                    new_temp_buffer.bytes(),
                    temp_buffer.bytes(),
                    temp_buffer.size());
                    
                temp_buffer = new_temp_buffer;
            }
            
            // Store the character, and print the replacement char.
            temp_buffer[actual_length++] = c; // note postfix increment
            if (replacement) putchar(replacement);
        }

        // Restore the console back the way it was.
        tty_attr.c_lflag = c_lflag;

        if (tcsetattr(STDIN_FILENO, 0, &tty_attr) < 0)
            throw std::runtime_error("could not restore standard input "
                "console mode");
    }
    catch (...)
    {
        // Put everything back the way it was
        tty_attr.c_lflag = c_lflag;
        tcsetattr(STDIN_FILENO, 0, &tty_attr);

        throw;
    }
    
// --- END POSIX
#endif

    // Output the temp buffer as the keyphrase
    keyphrase.set_to_zero(actual_length);
    memcpy(keyphrase.bytes(), temp_buffer.bytes(), actual_length);

    // Move to a new line on the console.
    std::cout << std::endl;
}   // end get_password
