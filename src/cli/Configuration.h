/**
 * \file Configuration.h Declares the Configuration class
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <scryfe/scryfe.h>

#ifndef _scryfe_cli_configuration_h_included
#define _scryfe_cli_configuration_h_included

/**
 * \brief Configuration items for the *SCryFE* CLI tool
 * 
 * All configuration items for the CLI are given as arguments on the
 * command-line (future iterations may allow file-based config as well).
 * 
 * This class contains all the configuration items and defaults, as well as
 * code for parsing command-line arguments. It is implemented as a singleton.
 * See the CLI `readme.txt` file for the set of command-line arguments that
 * is supported in the current implementation.
 */
class Configuration
{
    // --- Public interface ---
    
    public:
        
    /**
     * \brief Retrieve the single instance of this class
     * 
     * \return A reference to the single instance of this class
     */
    static Configuration& get_configuration(void);
    
    // -- Parsing --
    
    /**
     * \brief Parse the given command-line arguments
     * 
     * \param argc The number of command-line arguments, including the path
     * to the executable (as passed to main)
     * 
     * \param argv The array of command-line arguments, includingg the path
     * to the executable (as passed to main)
     * 
     * \throws std::runtime_error If there is a problem with the command-line
     * arguments
     */
    void parse(int argc, char* argv[]);
    
    /**
     * \brief Get the help string
     *
     * \param exename The name of the executable
     *
     * \return A multi-line string describing the command-line arguments
     */
    static std::string help_message(
        const std::string& exename = "scryfe-cli");
    
    // -- Accessors --
    
    /**
     * \brief Retrieve the selected cipher
     * 
     * \return The selected cipher enumerator
     */
    SCRYFE::FileEncrypter::cipher cipher(void) const { return m_cipher; }
    
    /**
     * \brief Retrieve the selected block mode
     * 
     * \return The selected block mode enumerator
     */
    SCRYFE::FileEncrypter::block_mode block_mode(void) const
        { return m_block_mode; }
        
    /**
     * \brief Retrieve the selected operation
     * 
     * \return The selected operation
     */
    SCRYFE::cipher_mode operation(void) const { return m_operation; }
    
    /**
     * \brief Retrieve the name of the file to encrypt or decrypt
     * 
     * \return The name of the file
     */
    const std::string& filename(void) const { return m_filename; }
    
    /**
     * \brief Whether or not the help option was selected
     *
     * This option overrides all other options
     *
     * \return True if help was selected
     */
    bool help(void) const { return m_help; }
    
    /**
     * \brief Whether or not verbose mode was selected
     *
     * \return True if verbose mode was selected
     */
    bool verbose(void) const { return m_verbose; }
        
    // --- Internal declarations ---
        
    private:
        
    // -- Internal methods --
        
    /**
     * \brief Constructor - initialises the configuration to defaults
     */
    Configuration(void);
    
    // Default destructor is fine
        
    // -- Internal attributes --
        
    /**
     * \brief The cipher to use for encryption and decryption
     * 
     * In this iteration, only one cipher is supported - AES-256, and there
     * is no choice of cipher from the command-line.
     */
    SCRYFE::FileEncrypter::cipher m_cipher;

    /**
     * \brief The block mode to use for encryption and decryption
     * 
     * In this iteration, only one block mode is supported - OFB, and there
     * is no choice of block mode from the command-line
     */
    SCRYFE::FileEncrypter::block_mode m_block_mode;
    
    /**
     * \brief The operation to perform (encryption or decryption)
     * 
     * This *must* be set from the command-line.
     */
    SCRYFE::cipher_mode m_operation;
    
    /**
     * \brief The name of the file to encrypt or decrypt
     */
    std::string m_filename;
    
    /**
     * \brief Whether or not the help option was selected
     *
     * This option overrides all other options
     */
    bool m_help;
    
    /**
     * \brief Whether or not verbose mode was selected
     */
    bool m_verbose;
};  // end class Configuration

#endif
