/**
 * \file GatherEntropyFromCLI.h Functions for gathering entropy from keyboard
 * presses
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <scryfe/scryfe.h>

#ifndef _scryfe_cli_gatherentropyfromcli_h_included
#define _scryfe_cli_gatherentropyfromcli_h_included

/**
 * \brief Gather entropy from keypresses, displaying the amount of entropy
 * gathered on the command-line
 * 
 * This method gathers entropy from keypresses and timings, placing it in
 * an array. A requested number of BITS (not bytes) of entropy is gathered,
 * with the amount gathered so far displayed as a percentage on the command-
 * line.
 * 
 * Note that it is assumed that each keypress generates approximately 0.5
 * bits of entropy.
 * 
 * \param entropy_bits The number of bits of entropy (not bytes) to gather
 * 
 * \param entropy The buffer into which the random bits are placed
 *
 * \param out The stream on which to report progress; cursor should be at the
 * beginning of the line
 * 
 * \throws SCRYFE::Error if there is a *SCryFE*-related error
 * 
 * \throws std::runtime_error if there is any other error
 */
extern void gather_entropy(
    std::size_t entropy_bits,
    SCRYFE::ByteArray& entropy,
    std::ostream& out);

#endif
