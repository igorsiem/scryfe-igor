/**
 * \file GatherEntropyFromCLI.cpp Functions for gathering entropy from
 * keyboard presses
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdexcept>
#include <iomanip>

#include <scryfe/scryfe.h>

#ifdef _WIN32
    #include <windows.h>
    #include <conio.h>
#else
    #include <sys/time.h>
    #include <termios.h>    
    #include <fcntl.h>
    #include <unistd.h>
    #include <errno.h>
    #include <cstring>
#endif

/**
 * \brief An entry in the entropy table
 * 
 * This includes a typed character, and time in milliseconds or microseconds
 * (depending on platform). On most consumer hardware, the clock isn't even
 * accurate to milliseconds, so there's no reduction in generality or
 * randomness for either method. The entire structure is reckoned to
 * encapsulate about half a bit of entropy for keyboard presses.
 * 
 * See chapter 11.20 in [eTutorials]([eTutorials.org](http://etutorials.org)
 * for more on this.
 */
struct EntropyEntry
{
    /**
     * \brief The character types
     */
    char m_c;
    
#ifdef _WIN32
    /**
     * \brief The milliseconds (within the second) of the time at which the
     * key was pressed
     */
    WORD m_milliseconds;
    
#else
    
    /**
     * \brief The microseconds as returned by gettimeofday
     */
    unsigned long int m_microseconds;
    
#endif
};  // end struct EntropyEntry

/**
 * \brief Print the current entropy-gathering progress to the given output
 * stream
 * 
 * It is assumed that this is a console stream, and that the cursor is at the
 * beginning of a clear line.
 * 
 * \param number_of_bytes_collected The number of bytes collected
 * 
 * \param total_bytes_to_collect The total number of bytes to collect
 * 
 * \param out The output stream
 */
void print_progress(
        std::size_t number_of_bytes_collected,
        std::size_t total_bytes_to_collect,
        std::ostream& out)
{
    double progress = 100 * (double)number_of_bytes_collected /
        (double)total_bytes_to_collect;
    out << "\r[ " << std::setw(5) << std::setprecision(1) << std::fixed <<
        progress << " % ]   ";
        
    out.flush();
}   // end print_progress

#ifndef _WIN32

/**
 * \brief Put the TTY into raw mode, so that the most keypresses can be
 * registered
 * 
 * This function has been adapted from code contained in an article on
 * [eTutorials.org](http://etutorials.org), called *11.20 Gathering Entropy
 * from the Keyboard*
 * 
 * \param fd The file descriptor of the tty device
 * 
 * \param saved_opts A pointer to a termios structure that saves the
 * original terminal parameters for restoration afterwards
 */
void make_tty_raw(int fd, struct termios *saved_opts)
{
    struct termios new_opts;
    
    // Retrieve the old terminal parameters prior to setting new ones, so
    // that we can 
    if (tcgetattr(fd, saved_opts) < 0)
        throw std::runtime_error("could not retrieve terminal parameters "
            "prior to gathering entropy");
    
    // Make a copy of saved_opts, not an alias.
    new_opts = *saved_opts;
    
    // Set up the various flags.
    new_opts.c_lflag    &= ~(ECHO | ICANON | IEXTEN | ISIG);
    new_opts.c_iflag    &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    new_opts.c_cflag    &= ~(CSIZE | PARENB);
    new_opts.c_cflag    |= CS8;
    new_opts.c_oflag    &= ~OPOST;
    new_opts.c_cc[VMIN]  = 1;
    new_opts.c_cc[VTIME] = 0;
    
    // Set the terminal parameters.
    if (tcsetattr(fd, TCSAFLUSH, &new_opts) < 0)
        throw std::runtime_error("could not set terminal parameters to raw "
            "mode prior to gathering entropy");
}   // end make_tty_raw

#endif

void gather_entropy(
        std::size_t entropy_bits,
        SCRYFE::ByteArray& entropy,
        std::ostream& out)
{
    // How many entries do we need?
    std::size_t number_of_entries = entropy_bits * 2;
    
    // How many bytes total will we generate?
    std::size_t total_bytes = number_of_entries * sizeof(EntropyEntry);
    
    // Allocate this length in the buffer.
    entropy.set_to_zero(total_bytes);
    std::size_t current_offset = 0;
    
    // Print out progress.
    print_progress(current_offset, total_bytes, out);
    
    // Actually gathering the data is done in a completely different way,
    // depending on whether we are on a posix system, or Windows.
#ifdef _WIN32
    // Loop through, until the array has been filled.
    while (current_offset < total_bytes)
    {
        EntropyEntry e;
            
        // Get the keypress
        e.m_c = _getch();
            
        // Get the time
        SYSTEMTIME st;
        GetSystemTime(&st);
        e.m_milliseconds = st.wMilliseconds;
            
        // Add the new entry to the array.
        memcpy((char*)entropy.bytes() + current_offset, &e, sizeof(e));
        current_offset += sizeof(e);
            
        // Report progress on the command-line.
        print_progress(current_offset, total_bytes, out);
    }   // end while the buffer is not full yet
    
// --- END WINDOWS
#else
// --- BEGIN POSIX
    
    // Open the tty terminal - this is where we will read our keypresses
    // from.
    int fd = open("/dev/tty", O_RDWR);
    
    // Make sure that the terminal device is closed if an exception is
    // thrown.
    try
    {
    
        // Set the terminal to 'raw' mode, keeping the existing terminal
        // parameters to restore for later.
        struct termios opts;
        make_tty_raw(fd, &opts);
        
        // Make sure that the terminal is set back normal after we're done.
        try
        {
            // Go into a loop, gathering keypresses.
            while (current_offset < total_bytes)
            {
                // Check for a typed character - this blocks until a
                // character is received.
                char c;
                if (read(fd, &c, 1) < 1)
                {
                    if (errno != EAGAIN)
                        throw std::runtime_error("error reading character "
                            "from terminal device");
                }
                
                // Grab the time stamp.
                struct timeval tv;
                struct timezone tz;
                if (gettimeofday(&tv, &tz) < 0)
                    throw std::runtime_error("error retrieving system time");
                
                EntropyEntry e;
                e.m_c = c;
                e.m_microseconds = (unsigned long int)tv.tv_usec;
                
                // Add the new entry to our buffer.
                memcpy((char*)entropy.bytes() + current_offset, &e,
                    sizeof(e));
                current_offset += sizeof(e);
                
                // Report progress on the command-line.
                print_progress(current_offset, total_bytes, out);
            }   // end while buffer is not full yet
            
            // Set the terminal back to normal.
            tcsetattr(fd, TCSAFLUSH, &opts);
        }
        catch (...)
        {
            // Set the terminal back to normal.
            tcsetattr(fd, TCSAFLUSH, &opts);
            throw;
        }
        
        // Close the tty FD
        close(fd);
    }
    catch (...)
    {
        // Close the tty FD
        close(fd);
        throw;
    }

#endif
}   // end gather_entropy
