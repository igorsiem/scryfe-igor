/**
 * \file Keyphrase.h Function for getting an encryption key phrase securely
 * from the console
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <scryfe/scryfe.h>
 
#ifndef _scryfe_cli_keyphrase_h_included
#define _scryfe_cli_keyphrase_h_included

/**
 * \brief Retrieve a key phrase securely from the console
 *
 * Note that the cursor is left on a new line after the password is entered.
 * All buffering is done using the SCRYFE::ByteArray, for security. This
 * function is adapted from the code at
 * http://www.cplusplus.com/forum/unices/27735/.
 *
 * \param keyphrase The buffer holding the keyphrase
 *
 * \param prompt The text prompt to display for getting the password
 *
 * \param replacement The character to use for a replacement while typing
 *
 * \throw std::runtime_error A console-related issue occured
 *
 * \throw SCRYFE::Error An error occurred with secure buffer handling
 */
extern void get_keyphrase(
    SCRYFE::ByteArray& keyphrase,
    const std::string& prompt = "Password: ",
    char replacement = '*');

#endif
