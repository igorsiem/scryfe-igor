/**
 * \file main.cpp The entry point for the cli executable
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdexcept>
#include <iostream>
#include <iomanip>

#include <scryfe/scryfe.h>

#include "Configuration.h"
#include "GatherEntropyFromCLI.h"
#include "Keyphrase.h"

/**
 * \brief A progress listener for encryption and decryption operation
 *
 * This class implements the progress monitoring callback interface of the
 * `SCRYFE::FileEncrypter` class. It is passed to the `encrypt` and `decrypt`
 * method calls, and simply prints the progress of the operation on the CLI.
 *
 * The callback function is invoked when each block is encrypted or
 * decrypted, and updating the console can be time-consuming. Instead, the
 * console is only written to every 100 calls.
 */
class ProgressReporter : public SCRYFE::FileEncrypter::ProgressMonitor
{
    public:
    
    /**
     * \brief Constructor, initialising the internal counter
     */
    ProgressReporter(void) :
        SCRYFE::FileEncrypter::ProgressMonitor(), m_update_count(0) {}
    
    /**
     * \brief Virtual destructor (trivial) for consistency
     */
    virtual ~ProgressReporter(void) {}

    /**
     * \brief Report on the progress of the operation
     * 
     * This method is called repeatedly during an encryption or decryption
     * operation (invoked by the SCryFE framework). Every 100 calls, the
     * progress is reported to the console.
     * 
     * \param number_of_bytes_processed The number of bytes that have
     * been processed (encrypted or decrypted) so far
     * 
     * \param total_number_of_bytes The total number of bytes to
     * process
     */
    virtual void signal_progress(
            int64 number_of_bytes_processed,
            int64 total_number_of_bytes)
    {
        if (++m_update_count >= 100)
            std::cout << "\rProgress " << std::setw(5) <<
                std::setprecision(1) << std::fixed << 100.0 *
                static_cast<double>(number_of_bytes_processed) /
                static_cast<double>(total_number_of_bytes) << "%          ";
    }
    
    private:
    
    /**
     * \brief The count of updates from the encryption / decryption operation
     */
    int m_update_count;
};  // end ProgressReporter

/**
 * \brief Entry point for the CLI executable
 *
 * This method parses the command-line, and executes the encryption /
 * decryption operation.
 *
 * \param argc The number of command-line arguments
 *
 * \param argv The vector of command-line arguments; the first of these is
 * the executable path
 *
 * \return Non-zero on error
 */
int main(int argc, char* argv[])
{
    int result = 0;
    try
    {
        std::cout << "SCryFE " << SCRYFE::version << std::endl <<
            "Copyright (c) 2014 Igor Siemienowicz" << std::endl <<
            std::endl <<
            "This program is free software: you can redistribute it and/or "
                "modify it under" << std::endl <<
            "the terms of the GNU General Public License as published by "
                "the Free Software" << std::endl <<
            "Foundation, either version 3 of the License, or (at your "
                "option) any later" << std::endl <<
            "version." << std::endl <<
            std::endl <<
            "This program is distributed in the hope that it will be "
                "useful, but WITHOUT" << std::endl <<
            "ANY WARRANTY; without even the implied warranty of "
                "MERCHANTABILITY or FITNESS" << std::endl <<
            "FOR A PARTICULAR PURPOSE. See the GNU General Public License "
                "for more" << std::endl <<
            "details." << std::endl <<
            std::endl <<
            "You should have received a copy of the GNU General Public "
                "License along with" << std::endl <<
            "this program. If not, see <http://www.gnu.org/licenses/>." <<
                std::endl <<
            std::endl;
                
        // Parse the command-line.
        Configuration& config = Configuration::get_configuration();
        config.parse(argc, argv);
        
        // If we are just printing help, then do it and exit, otherwise,
        // go ahead with the operation.
        if (config.help()) std::cout << config.help_message() << std::endl;
        else
        {
            // Get a keyphrase for the operation. This is done twice, the
            // second time for confirmation.
            //
            SCRYFE::ByteArray keyphrase, confirmation_keyphrase;
            do
            {
                get_keyphrase(keyphrase, "Keyphrase: ");
                get_keyphrase(confirmation_keyphrase, "Confirm keyphrase: ");
                
                if (keyphrase != confirmation_keyphrase)
                    std::cout << "The keyphrases do not match. Please try "
                        "again." << std::endl;
            } while (keyphrase != confirmation_keyphrase);
            
            // The key is the keyphrase hashed to the key length of the
            // cipher. At the moment, that is 256 bits. Destroy the
            // keyphrase, just to be on the safe side.
            SCRYFE::SHA256Hash hash256;
            SCRYFE::ByteArray key;
            hash256.process(keyphrase, key);
            keyphrase.clear();
            
            // Create a progress monitor.
            ProgressReporter progress_reporter;
            
            // Which operation are we doing?
            if (config.operation() == SCRYFE::encrypt)
            {
                // We are encrypting, so we need entropy for an IV.
                if (config.verbose())
                    std::cout << "Encryption requires a certain amount of "
                        "randomised data. Please type " << std::endl <<
                        "randomly on the keyboard until the random data "
                            "buffer is full:" << std::endl;
                else std::cout << "Please type randomly to fill the "
                    "entropy buffer" << std::endl;
                
                SCRYFE::ByteArray raw_entropy;
                gather_entropy(128, raw_entropy, std::cout);
                
                // We now have 128 bits of entropy (scattered over a much
                // wider array of bytes). We use the SHA-256 hash to
                // condition and condense this down to 256 bits (32 bytes)
                //
                // Note: we are actually trying to construct a 128-bit random
                // Initialisation Vector (IV), but we don't have a 128-bit
                // version of SHA (there isn't one). Instead, we hash 128
                // bits of entropy down to a 256-bit hash, and then XOR one
                // half of this with the other.
                //
                // I (Igor Siemienowicz) don't believe that this will result
                // in any loss of entropy, but this probably needs to be
                // verified by a cryptography expert.
                //
                //        - Igor Siemienowicz, 2 July 2014
                //
                // TODO Check that entropy-gathering algorithm for IV is
                // sufficient
                SCRYFE::ByteArray hashed_entropy;
                hash256.process(raw_entropy, hashed_entropy);
                
                // The IV is then the two halves of the hash, XORd together.
                SCRYFE::ByteArray iv =
                    SCRYFE::ByteArray(hashed_entropy.bytes(), 16) ^
                    SCRYFE::ByteArray((char*)hashed_entropy.bytes()+16, 16);
                    
                // Now we can go ahead and encrypt. If there is no exception,
                // the operation worked.
                SCRYFE::FileEncrypter::encrypt(
                    config.filename(),
                    config.cipher(),
                    config.block_mode(),
                    iv,
                    key,
                    &progress_reporter);
                    
                std::cout << std::endl;
                
                if (config.verbose())
                    std::cout << "encryption complete" << std::endl;
            }   // end if we are encrypting
            else
            {
                // We don't need the IV for decrypting (it comes from the
                // encrypted file). We can go ahead and decrypt. If there is
                // no exception, the operation was successful.
                SCRYFE::FileEncrypter::decrypt(
                    config.filename(),
                    config.cipher(),
                    config.block_mode(),
                    key,
                    &progress_reporter);
                
                std::cout << std::endl;

                if (config.verbose())
                    std::cout << "decryption complete" << std::endl;
            }   // end if we are decrypting
        }   // end if we are not just doing help.
    }
    catch (const SCRYFE::Error& scryfe_error)
    {
        std::cerr << "[SCRYFE ERROR] " << scryfe_error.what() << std::endl;
        result = -1;
    }
    catch (const std::exception& error)
    {
        std::cerr << "[ERROR] " << error.what() << std::endl;
        result = -2;
    }
    catch (...)
    {
        std::cerr << "[ERROR] *** unrecognised error condition" << std::endl;
        result = -3;
    }
    
    return result;
}   // end main
