`cli` Directory
===============
This directory contains the code files for the *SCryFE* CLI executable. This
program is intended as a consumer-level software interface for *SCryFE*
functionality. Unlike the *SCryFE* core library (which is licensed under the
LGPL), the CLI executable is licensed under the full GNU General Public
License (GPL) version 3.

Invocation
----------
The executable is invoked as follows:

    scryfe-cli [<filename> --operation [encrypt|decrypt]
        [-h|--help][--verbose]
    
Future iterations will allow selection of cipher and block mode, but in the
current implementation, the only options for these are AES-256 and OFB,
respectively.

The program will interactively request random keypresses for a source of
entropy (only for the encryption step), and a non-echoed pass-phrase. The
file operation will then commence.

It should be noted that the *file operation is* ***destructive,*** - i.e.
the selected file is overwritten *in situ*. This is deliberate, so that the
plaintext version of the file is not left lying around on the disk (ordinary
deletion is not secure).

Source File Index
-----------------
*   `CMakeLists` - The CMake file for the executable project

*   `Configuration.*` - The Configuration class

*   `GatherEntropyFromCLI.h.*` - Code for gathering randomness from random
    keypresses on the command-line
    
*   `Keyphrase.*` - Code for getting an encryption key phrase securely from
    the console

*   `main.cpp` - The entry point for the CLI executable

*   `readme.txt` - This file