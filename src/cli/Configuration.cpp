/**
 * \file Configuration.cpp Implements the Configuration class
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdexcept>
#include <sstream>
#include <iostream>

#include "Configuration.h"

Configuration& Configuration::get_configuration(void)
{
    static Configuration c;
    return c;
}   // end get_configuration

Configuration::Configuration(void) :
    m_cipher(SCRYFE::FileEncrypter::aes256),
    m_block_mode(SCRYFE::FileEncrypter::ofb),
    m_operation(SCRYFE::none),
    m_filename(),
    m_help(false),
    m_verbose(false)
{
}   // end constructor

void Configuration::parse(int argc, char* argv[])
{
    // Grab the executable name first.
    if (argc < 1)
        throw std::runtime_error("excutable invoked with zero "
            "arguments");
        
    std::string exename(argv[0]);
    
    // Loop through the rest of the arguments.
    for (int i = 1; i < argc; i++)
    {
        std::string argument(argv[i]);
        if ((argument == "-h") || (argument == "--help"))
            m_help = true;
        else if ((argument == "-o") || (argument == "--operation"))
        {
            // We need a second argument.
            if (i+1 >= argc)
                throw std::runtime_error("\"" + argument + "\" used without "
                    "a following argument; operation must be \"encrypt\" "
                    "or \"decrypt\"");
                    
            std::string operation_name(argv[++i]);
            if (operation_name == "encrypt") m_operation = SCRYFE::encrypt;
            else if (operation_name == "decrypt")
                m_operation = SCRYFE::decrypt;
            else throw std::runtime_error("operation \"" + operation_name +
                "\" is not recognised; operation must be \"encrypt\" "
                "or \"decrypt\"");
        }
        else if (argument == "--verbose") m_verbose = true;
        else m_filename = argument;
    }   // end argument loop
        
    // Check that we have everything we need.
    if (m_help == false)
    {
        if (m_filename.empty())
            throw std::runtime_error("a filename is required");
        if ((m_operation != SCRYFE::encrypt) &&
                (m_operation != SCRYFE::decrypt))
            throw std::runtime_error("an operation (\"encrypt\" or "
                "\"decrypt\") must be specified");
    }   // end if help was not requested.
            
    // If we are in verbose mode, print out our options.
    if (m_verbose)
    {
        std::cout << std::endl <<
            "filename: " << m_filename << std::endl <<
            "operation: " <<
                ((m_operation == SCRYFE::encrypt)?"encrypt":"decrypt") <<
                std::endl <<
            std::endl;
    }
}   // end parse
    
std::string Configuration::help_message(const std::string& exename)
{
    std::stringstream msg;
    msg << "The SCryFE CLI is invoked as follows:" << std::endl <<
        std::endl <<
        "    " << exename << " <filename> -o [encrypt|decrypt] "
            "[--verbose]" << std::endl <<
        std::endl << "Arguments:" << std::endl <<
        std::endl <<
        "-o, --operation [encrypt|decrypt]" << std::endl <<
        "    Perform the named operation (encryption or decryption) on "
            "<filename>" << std::endl <<
        std::endl <<
        "--verbose" << std::endl <<
        "    Provide verbose output (can be useful in "
            "debugging)" << std::endl <<
        std::endl <<
        "Note that the existing file is OVERWRITTEN. It is wise (but often "
            "less " << std::endl <<
        "secure to back up the file first";

    return msg.str();
}   // end help_message
