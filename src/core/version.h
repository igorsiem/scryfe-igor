/**
 * \file version.h Version information for *SCryFE*
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#ifndef _scryfe_version_h_included
#define _scryfe_version_h_included

/**
 * \brief The namespace for all SCryFE-related declarations
 */
namespace SCRYFE
{

/**
 * \brief A single four-element version number for all of SCryFE
 */
extern const char* version;
    
}   // end namespace SCRYFE

/**
 * \mainpage Welcome to the SCryFE Development Kit
 * 
 * This is the low-level documentation for the *SCryFE* Development Kit
 * (SDK), a free code library of strong cryptographic functionality that you
 * can add to your programs under the terms of the Gnu Lesser General Public
 * License (LGPL, version 3), and other licenses that are compatible with it.
 * These licenses are distributed as part of this package, and may be found
 * under the `licenses` subdirectory.
 * 
 * A higher-level introduction to *SCryFE* is distributed with this package
 * as a PDF document, which may be found in the `doc` subdirectory. It is the
 * best place to start for those who are unfamiliar with *SCryFE* and the
 * SDK.
 * 
 * The main home of the *SCryFE* project is [scrye.com](http://scryfe.com),
 * and the source code for this library and other *SCryFE* products is hosted
 * at [bitbucket.org/scryfe/scryfe](http://bitbucket.org/scryfe/scryfe).
 */

#endif
