/**
 * \file Entropy.cpp Implements the Entropy class
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include "Entropy.h"

#include <cstring>
#include <cmath>
#include <map>

namespace SCRYFE
{

Entropy::Entropy(
        Hash& hash,
        std::size_t max_cooked_size) :
    m_hash(hash),
    m_cooked(),
    m_max_cooked_size(max_cooked_size)
{
}   // end constructor

Entropy::~Entropy(void)
{
    // The m_cooked queue will clear itself securely, because the byte
    // array destructors clear themselves.
}   // end destructor

void Entropy::clear(void)
{
    // Byte array entries need to be deleted, but they will clear themselves
    // securely.
    while (m_cooked.size()) m_cooked.pop();
}   // end clear

void Entropy::cook(const void* bytes, std::size_t size)
{
    if (bytes == NULL)
        SCRYFE_RAISE_ERROR("attempt to cook random bytes with a NULL "
            "pointer");
            
    if (size <= 0)
        SCRYFE_RAISE_ERROR("attempt to cook a random byte array of size "
            "zero");
            
    ByteArray raw(bytes, size), cooked;
    m_hash.process(raw, cooked);
    m_cooked.push(cooked);
}   // end cook

void Entropy::pop_cooked(ByteArray& cooked_entry)
{
    if (m_cooked.size() == 0)
        SCRYFE_RAISE_ERROR("attempt to retrieve random data from empty "
            "entropy pool");
            
    cooked_entry = m_cooked.front();
    m_cooked.pop();
}   // end pop_cooked

/**
 * \brief Calculate log-base-2 of a number
 *
 * \param n The number to calculate the base-2 log for
 *
 * \return The base-2 log for n
 */
double log2(double n)
{
    return log(n) / log(2.0);
}   // end log2

double Entropy::shannon_entropy(const void* bytes, std::size_t size)
{
    // Find the frequencies of each byte value.
    std::map<byte, int> frequencies;
    for (std::size_t i = 0; i < size; i++)
    {
        if (frequencies.find(((byte*)bytes)[i]) == frequencies.end())
            frequencies[((byte*)bytes)[i]] = 0;
            
        frequencies[((byte*)bytes)[i]]++;
    }

    // Now move through the frequencies, totalling up the information
    // content.
    double infocontent = 0;
    for (std::map<byte, int>::const_iterator itr = frequencies.begin();
            itr != frequencies.end(); itr++)
    {
        double f = static_cast<double>(itr->second) / size;
        infocontent += f * log2(f);
    }
    
    infocontent *= -1;
    
    return infocontent;
}   // end shannon_entropy

double Entropy::shannon_entropy(const ByteArray& bytes)
{
    return shannon_entropy(bytes.bytes(), bytes.size());
}   // end shannon_entropy

}   // end namespace SCRYFE
