/**
 * \file Hash.h Declares the Hash class
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include "Types.h"
 
#ifndef _scryfe_hash_h_included
#define _scryfe_hash_h_included

namespace SCRYFE
{

/**
 * \brief An abstract base class for all hashing algorithms
 */
class Hash
{
    // --- Public interface ---
    
    public:
    
    /**
     * \brief Trivial constructor
     */
    Hash(void) {}
    
    /**
     * \brief Trivial destructor
     */
    virtual ~Hash(void) {}
    
    /**
     * \brief Hash the input, producing the hashed digest
     *
     * \param input The input bytes
     *
     * \param output The message digest (anything that was previously in
     * the byte array is cleared)
     *
     * \throws Error if a problem occurs with hashing
     */
    virtual void process(const ByteArray& input, ByteArray& output) = 0;
    
    /**
     * \brief The output size of the hashing algorithm, in bytes
     *
     * \return The output size of the algorithm, in bytes
     */
    virtual std::size_t output_size(void) const = 0;
};

}   // end namespace SCRYFE

#endif
