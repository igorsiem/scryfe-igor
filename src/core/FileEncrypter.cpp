/**
 * \file FileEncrypter.cpp Implements the FileEncrypter class
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include <cstdio>

#ifdef _WIN32
    #include <io.h>
#else
    #include <unistd.h>
#endif

#include "FileEncrypter.h"
#include "Error.h"
#include "AES256Cipher.h"
#include "OFBBlockMode.h"

namespace SCRYFE
{

void FileEncrypter::encrypt(
        const std::string& filename,
        cipher c,
        block_mode bm,
        const ByteArray& iv,
        const ByteArray& key,
        ProgressMonitor* progress_monitor)
{
    // Open the file using standard C-style fopen operations. This will fail
    // if the file does not exist, or cannot be accessed for reading and
    // writing.
    //
    // Windows implementations prefer the secure-er version of fopen.
#ifdef _WIN32
    FILE* file_ptr = NULL;
    errno_t fopen_result = fopen_s(&file_ptr, filename.c_str(), "r+b");
    if (fopen_result)
        SCRYFE_RAISE_ERROR("could not open file \"" << filename << "\" for "
            "reading and writing");
#else
    FILE* file_ptr = fopen(filename.c_str(), "r+b");
#endif

    if (file_ptr == NULL)
        SCRYFE_RAISE_ERROR("could not open file \"" << filename << "\" for "
            "reading and writing");
            
    // Pointers for our cipher and block mode objects. These need to be
    // deleted at the end as well.
    BlockCipher* cipher_ptr = NULL;
    BlockMode* block_mode_ptr = NULL;
    
    // Make sure our objects are deleted, and file is closed at the end, no
    // matter what.
    try
    {
        // Set up our cipher object.
        switch (c)
        {
            case aes256:
                cipher_ptr = new AES256Cipher();
                break;
                
            default:
                SCRYFE_RAISE_ERROR("unrecognised cipher enumerator when "
                    "encrypting file");
        }
        
        // Create and initialise the block mode object.
        switch (bm)
        {
            case ofb:
            {
                OFBBlockMode* ofb_block_mode_ptr =
                    new OFBBlockMode(*cipher_ptr);
                ofb_block_mode_ptr->initialise(iv, key,
                    SCRYFE::encrypt);
                block_mode_ptr = ofb_block_mode_ptr;
            }
            break;
                
            default:
                SCRYFE_RAISE_ERROR("unrecognised block mode enumerator when "
                    "encrypting file");
        }
        
        // Find out what the file size is. We're using fseek / ftell, which
        // a lot of people deprecate, but should work OK the way we're using
        // it, because we've opened the file in binary, AND we're using the
        // 'large file' variants of the system calls.
#ifdef _WIN32
        int result = _fseeki64(file_ptr, 0, SEEK_END);
#else
         int result = fseeko(file_ptr, 0, SEEK_END);
#endif  
        if (result)
            SCRYFE_RAISE_ERROR("could not set the file pointer to the end "
                "of file \"" << filename << "\"");

#ifdef _WIN32
        int64 file_size = _ftelli64(file_ptr);
#else
        int64 file_size = ftello(file_ptr);
#endif
        
        // Make sure we put the file pointer back at the beginning.
#ifdef _WIN32
        result = _fseeki64(file_ptr, 0, SEEK_SET);
#else
        result = fseeko(file_ptr, 0, SEEK_SET);
#endif  
        if (result)
            SCRYFE_RAISE_ERROR("could not set the file pointer to the "
                "beginning of file \"" << filename << "\"");
            
        // Now we can begin the encryption loop. Start by allocating an
        // encryption buffer.
        ByteArray buffer;
        buffer.set_to_zero(cipher_ptr->block_size());
        bool finished = false;
        std::size_t byte_count = 0;
        while (!finished)
        {
            // Report our progress.
            if (progress_monitor)
                progress_monitor->signal_progress(byte_count, file_size);
            
            // Remember the current position in the file.
            fpos_t pos;
            int result = fgetpos(file_ptr, &pos);
            if (result)
                SCRYFE_RAISE_ERROR("error retrieving position in file \"" <<
                    filename << "\" during encryption operation");
                    
            // Read the next block from the file.
            std::size_t bytes_read =
                fread(buffer.bytes(), 1, buffer.size(), file_ptr);
            byte_count += bytes_read;
                
            // We have a 'short count' - this should mean that we've reached
            // the end of the file.
            if (bytes_read < buffer.size()) finished = true;
                
            // If the byte count is short (which should mean that we are at
            // the end of the file), then use a temporary buffer of the
            // correct size.
            if (bytes_read > 0)
            {
                if (bytes_read < buffer.size())
                {
                    ByteArray temp;
                    temp.copy_from(buffer.bytes(), bytes_read);
                    buffer = temp;
                }   // end if we have read less than a full block
                
                // Encrypt the buffer, making sure we got the same amount
                // of ciphertext back.
                ByteArray ciphertext;
                block_mode_ptr->process(buffer, ciphertext);
                if (ciphertext.size() != bytes_read)
                    SCRYFE_RAISE_ERROR("size of ciphertext block does not "
                        "match size of plaintext block; ciphers that change "
                        "block size are not currently supported");
                
                // Set the file pointer to where we read the bytes from, and
                // write the ciphertext over the top.
                result = fsetpos(file_ptr, &pos);
                if (result)
                    SCRYFE_RAISE_ERROR("error setting position in file \"" <<
                        filename << "\" during encryption operation");
                        
                std::size_t bytes_written =
                    fwrite(ciphertext.bytes(), 1, ciphertext.size(),
                        file_ptr);
                    
                if (bytes_written != ciphertext.size())
                    SCRYFE_RAISE_ERROR("an attempt to write " <<
                        ciphertext.size() << " bytes to file \"" <<
                        filename << "\" failed; " << bytes_written <<
                        " byte(s) were written");
                        
                // To get around some kind of inconsistency in the Windows
                // version of fwrite, we move to the beginning of the block
                // again, and read. This is purely to get the file pointer
                // in the right place, which for some reason doesn't work
                // properly under windows unless we do this.
                fsetpos(file_ptr, &pos);
                fread(ciphertext.bytes(), 1, ciphertext.size(), file_ptr);
            }   // end if some bytes were read
        }   // end while we have not reached the end of the file.
        
        // We should be at the end of the file now. Write the IV.
#ifdef _WIN32
    result = _fseeki64(file_ptr, 0, SEEK_END);
#else
    result = fseeko(file_ptr, 0, SEEK_END);    
#endif
        if (result)
            SCRYFE_RAISE_ERROR("could not move to the end of the file "
                "to write the initialisation vector after encrypting");
                
        std::size_t iv_bytes_written = fwrite(iv.bytes(), 1, iv.size(),
            file_ptr);
        if (iv_bytes_written != iv.size())
            SCRYFE_RAISE_ERROR("an attempt to write the initialisation "
                "vector bytes of the block mode at the end of the file "
                "failed; the IV is " << iv.size() << " bytes long; " <<
                iv_bytes_written << " byte(s) were written");
        
        // Clean everything up.
        delete block_mode_ptr;
        block_mode_ptr = NULL;
        
        delete cipher_ptr;
        cipher_ptr = NULL;
    
        // Close the file handle.
        fclose(file_ptr);
    }
    catch (...)
    {
        if (block_mode_ptr) delete block_mode_ptr;
        block_mode_ptr = NULL;
        
        if (cipher_ptr) delete cipher_ptr;
        cipher_ptr = NULL;
        
        // Close the file handle.
        fclose(file_ptr);
        throw;
    }
}   // end encrypt

void FileEncrypter::decrypt(
        const std::string& filename,
        cipher c,
        block_mode bm,
        const ByteArray& key,
        ProgressMonitor* progress_monitor)
{
    // Open the file using standard C-style fopen operations. This will fail
    // if the file does not exist, or cannot be accessed for reading and
    // writing.
    //
    // Windows implementations prefer the secure-er version of fopen.
#ifdef _WIN32
    FILE* file_ptr = NULL;
    errno_t fopen_result = fopen_s(&file_ptr, filename.c_str(), "rb+");
    if (fopen_result)
        SCRYFE_RAISE_ERROR("could not open file \"" << filename << "\" for "
            "reading and writing");
#else
    FILE* file_ptr = fopen(filename.c_str(), "rb+");
#endif

    if (file_ptr == NULL)
        SCRYFE_RAISE_ERROR("could not open file \"" << filename << "\" for "
            "reading and writing");
        
    // Pointers for our cipher and block mode objects. These need to be
    // deleted at the end as well.
    BlockCipher* cipher_ptr = NULL;
    BlockMode* block_mode_ptr = NULL;
    
    // Make sure our objects are deleted, and file is closed at the end, no
    // matter what.
    try
    {
        // Set up our cipher object.
        switch (c)
        {
            case aes256:
                cipher_ptr = new AES256Cipher();
                break;
                
            default:
                SCRYFE_RAISE_ERROR("unrecognised cipher enumerator when "
                    "decrypting file");
        }
                
        // Retrieve the IV from the end of the file. This is the same size
        // as the block size of the cipher.
        ByteArray iv;
        iv.set_to_zero(cipher_ptr->block_size());
        
        int offset = iv.size();
        
#ifdef _WIN32
        int result = _fseeki64(file_ptr, -offset, SEEK_END);
#else
        int result = fseeko(file_ptr, -offset, SEEK_END);
#endif
        
        if (result)
            SCRYFE_RAISE_ERROR("could not set the file pointer to read "
                "the initialisation vector when attempting to decrypt \"" <<
                filename << "\"");
            
        // At this point, the file pointer is at the REAL end of the file
        // (since the IV was tacked on to the end. We can use ftell to
        // tell us the real number of bytes in the file.
#ifdef _WIN32
        int64 real_file_size = _ftelli64(file_ptr);
#else
        int64 real_file_size = ftello(file_ptr);
#endif
                
        std::size_t bytes_read = fread(iv.bytes(), 1, iv.size(), file_ptr);
        if (bytes_read != iv.size())
            SCRYFE_RAISE_ERROR("could not read the initialisation vector "
                "from the end of \"" << filename << "\" for decryption; " <<
                iv.size() << " bytes were requested; " << bytes_read <<
                " byte(s) were read");
            
        // Create and initialise the block mode object.
        switch (bm)
        {
            case ofb:
            {
                OFBBlockMode* ofb_block_mode_ptr =
                    new OFBBlockMode(*cipher_ptr);
                ofb_block_mode_ptr->initialise(iv, key,
                    SCRYFE::decrypt);
                block_mode_ptr = ofb_block_mode_ptr;
            }
            break;
                
            default:
                SCRYFE_RAISE_ERROR("unrecognised block mode enumerator when "
                    "decrypting file");
        }
        
        // Now we can begin the decryption loop. Start by allocating a
        // decryption buffer.
        ByteArray buffer;
        buffer.set_to_zero(cipher_ptr->block_size());
        
        long int number_of_bytes_read = 0;
        while (number_of_bytes_read < real_file_size)
        {
            // If we're doing progress monitoring, call back the progress
            // monitor here.
            if (progress_monitor)
                progress_monitor->signal_progress(
                    number_of_bytes_read, real_file_size);
            
            // Seek to where we need to be in the file - we need to do this
            // explicitly, because Windows / VS seems to have a dodgy
            // implementation of fread / fwrite and file pointer handling.
            // This basically addresses issue #3.
#ifdef _WIN32
            result = _fseeki64(file_ptr, number_of_bytes_read, SEEK_SET);
#else
            result = fseeko(file_ptr, number_of_bytes_read, SEEK_SET);
#endif

            if (result)
                SCRYFE_RAISE_ERROR("could not move file pointer to the "
                    "correct position in file \"" << filename << "\" while "
                    "decrypting");
        
            // If we are getting towards the end of the file, we may have
            // fewer bytes left to read than the block size.
            int64 bytes_to_read = real_file_size - number_of_bytes_read;
            if (bytes_to_read < (long int)cipher_ptr->block_size())
                buffer.set_to_zero(static_cast<std::size_t>(bytes_to_read));
            
            // Remember the current position in the file.
            fpos_t pos;
            int result = fgetpos(file_ptr, &pos);
            if (result)
                SCRYFE_RAISE_ERROR("error retrieving position in file \"" <<
                    filename << "\" during decryption operation");
            
            // Read the next block from the file.
            std::size_t bytes_read =
                fread(buffer.bytes(), 1, buffer.size(), file_ptr);

            if (bytes_read < buffer.size())
                SCRYFE_RAISE_ERROR("could not read " << buffer.size() <<
                    " bytes from file \"" << filename << "\" while "
                    "decrypting");
                
            number_of_bytes_read += bytes_read;
            
            // Decrypt the buffer, making sure we got the same number of
            // bytes back.
            ByteArray plaintext;
            block_mode_ptr->process(buffer, plaintext);
            if (plaintext.size() != buffer.size())
                SCRYFE_RAISE_ERROR("size of plaintext block does not "
                    "match size of ciphertext block; ciphers that change "
                    "block size are not currently supported");
            
            // Set the file pointer to where we read the bytes from, and
            // write the plaintext over the top.
            result = fsetpos(file_ptr, &pos);
            if (result)
                SCRYFE_RAISE_ERROR("error setting position in file \"" <<
                    filename << "\" during decryption operation");
                
            std::size_t bytes_written =
                fwrite(plaintext.bytes(), 1, plaintext.size(), file_ptr);
                    
            if (bytes_written != plaintext.size())
                SCRYFE_RAISE_ERROR("an attempt to write " <<
                    plaintext.size() << " bytes to file \"" << filename <<
                    "\" failed; " << bytes_written << " byte(s) were "
                    "written");
        }   // end while we have not reached the end of the (real) file.
        
        // We need to truncate the IV that was written at the end of the
        // file. Truncate is platform-specific.
#ifdef _WIN32
        result = _chsize_s(_fileno(file_ptr), real_file_size);
#else
        result = ftruncate(fileno(file_ptr), real_file_size);
#endif
        if (result)
            SCRYFE_RAISE_ERROR("attempt to resize file \"" << filename <<
                "\" to its original size after decryption failed");
        
        // Clean everything up.
        delete block_mode_ptr;
        block_mode_ptr = NULL;
        
        delete cipher_ptr;
        cipher_ptr = NULL;
    
        // Close the file handle.
        fclose(file_ptr);
    }
    catch (...)
    {
        if (block_mode_ptr) delete block_mode_ptr;
        block_mode_ptr = NULL;
        
        if (cipher_ptr) delete cipher_ptr;
        cipher_ptr = NULL;
        
        // Close the file handle.
        fclose(file_ptr);
        throw;
    }
}   // end decrypt

}   // end namespace SCRYFE
