/**
 * \file Cipher.h Declaration of a number of cipher-related base classes
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include "Types.h"

#ifndef _scryfe_cipher_h_included
#define _scryfe_cipher_h_included

namespace SCRYFE
{

/**
 * \brief Base-class for all raw ciphers in *SCryFE*
 *
 * All classes encapsulating algorithms that encrypt raw bytes are derived
 * from this class.
 *
 * Note that copying is disabled for this entire class hierarchy. This is
 * because a number of cipher objects have state that should be kept secure,
 * and we don't want multiple copies floating around in memory.
 */
class Cipher
{
    // --- External interface ---
    
    public:
    
    /**
     * \brief Trivial constructor
     */
    Cipher(void) {}
    
    /**
     * \brief Trivial destructor
     */
    virtual ~Cipher(void) {}
    
    /**
     * \brief Securely clear any internal state of the cipher
     *
     * The cipher may need to be re-initialised before it can be used to
     * process any further data.
     */
    virtual void clear(void) = 0;
    
    /**
     * \brief Process input to produce output
     * 
     * This method is for performing the encryption or decryption operation.
     * It is assumed that the cipher has been initialised with a key, and is
     * read to go.
     * 
     * The implementation may throw an exception for any reason.
     * 
     * \param input The input block to process
     * 
     * \param output The output byte array
     * 
     * \throws Error if anything goes wrong
     */
    virtual void process(const ByteArray& input, ByteArray& output) = 0;
    
    // Disabled copy semantics
    private:
    Cipher(const Cipher&);
    Cipher& operator=(const Cipher&);
};  // end class Cipher

/**
 * \brief A base class for all Symmetric ciphers
 *
 * Copy is disabled for instances of this class.
 */
class SymmetricCipher : public Cipher
{
    // --- Public interface ---
    
    public:
    
    /**
     * \brief Trivial constructor
     */
    SymmetricCipher(void) : Cipher() {}
    
    /**
     * \brief Trivial destructor
     */
    virtual ~SymmetricCipher(void) {}
    
    // Disabled copy semantics
    private:
    SymmetricCipher(const SymmetricCipher&);
    SymmetricCipher& operator=(const SymmetricCipher&);
};  // end class SymmetricCipher

/**
 * \brief A base class for all block ciphers
 *
 * Copying is disabled for this class
 */
class BlockCipher : public SymmetricCipher
{
    // --- External interface ---
    
    public:
    
    /**
     * \brief Trivial constructor
     */
    BlockCipher(void) : SymmetricCipher() {}
    
    /**
     * \brief Trivial destructor
     */
    virtual ~BlockCipher(void) {}
    
    /**
     * \brief Retrieve the block size of the cipher
     *
     * \return The block size of the cipher (in bytes)
     */
    virtual std::size_t block_size(void) const = 0;
    
    /**
     * \brief Initialise the cipher for processing with a given key and
     * mode
     *
     * \param key The key to use; this must be in the correct form and length
     * for the cipher, or an exception is thrown
     *
     * \param m The mode to use for the cipher (encrypt or decrypt)
     *
     * \throws Error if there was a problem with the arguments or
     * initialisation
     */
    virtual void initialise(const ByteArray& key, cipher_mode m) = 0;
    
    // Disable copy semantics
    private:
    BlockCipher(const BlockCipher&);
    BlockCipher& operator=(const BlockCipher&);
};  // end BlockCipher

}   // end namespace SCRYFE

#endif
