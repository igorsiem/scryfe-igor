`core` Directory
================
Core declarations and functionality for use throughout the *SCryFE* project

Index
-----
*   `AES256Cipher.*` - Cipher class for AES-256 encryption
*   `BlockMode.h` - Framework for block cipher modes
*   `Cipher.h` - A number of Cipher-related abstract base classes
*   `Entropy.h` - A simple class for management of true random numbers
*   `Error.*` - Basic Error exception class
*   `Hash.h` - Base class for hashing classes
*   `OFBBlockModel.*` - Output FeedBack block cipher mode implementation
*   `SHAHash.*` - The SHA*Hash series of classes
*   `Types.*` - Core data types
*   `version.*` - Version information

Note that the `Mutex.*` and `Guard.*` files contain declarations for multi-
threading. This is not currently used in *SCryFE* (although it will be in
the future, and they cause a problem with Visual Studio Express (see
issue #25), so that have been renamed `*.*.disabled`.