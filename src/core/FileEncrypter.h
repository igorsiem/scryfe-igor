/**
 * \file FileEncrypter.h Declares the FileEncrypter class
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include <string>
#include "Types.h"

// Define 64-bit integers reliably
#ifdef _WIN32
    #define WIN32_LEAN_AND_MEAN
    
    /**
     * \brief A definition of a 64-bit integer, since there is no
     * standardised one across all platforms
     */
    #define int64 __int64
    
    /**
     * \brief A definition of a 64-bit unsigned integer, since there is no
     * standardised one across all platforms
     */    
    #define uint64 unsigned __int64
#else
    #include <inttypes.h>
    
    /**
     * \brief A definition of a 64-bit integer, since there is no
     * standardised one across all platforms
     */
    #define int64 int64_t
    
    /**
     * \brief A definition of a 64-bit unsigned integer, since there is no
     * standardised one across all platforms
     */    
    #define uint64 uint64_t
#endif

 
#ifndef _scryfe_fileencrypter_h_included
#define _scryfe_fileencrypter_h_included

namespace SCRYFE
{

/**
 * \brief A simple file encryption implementation
 *
 * This class implements atomic file encryption / decryption operations from
 * a selected block cipher, mode, initialisation vector and key. The
 * operations are static and stateless, and this class need not be
 * instantiated.
 *
 * When encrypting, an initialiation vector (IV) must be supplied. This
 * should come from a truly random source (like keyboard presses processed
 * using the Entropy class). This is appended to the end of the encrypted
 * file (since it need not be kept secret - that's what the key is for) and
 * reused for decryption. This means that encryption will change the file
 * size slightly, even when streaming block modes like OFB are used.
 *
 * It is important to note that encryption and decryption operations are
 * **destructive** - the existing file is **overwritten**. This is
 * intentional, so that the plaintext bytes are actively overwritten on this
 * disk. Among other things, this implies that *if an attempt is made to
 * decrypt a file with an incorrect selection of cipher, block mode or key,
 * the file will be irretrievably corrupted.*
 * 
 * Finally, note that there is no unit test for this class, because it will
 * need a lot of messy fixtures and such. Instead, an integration test
 * harness executable will be run by a ruby test script. This covers the same
 * functionality as a unit test would.
 */
class FileEncrypter
{
    // --- Public interface ---
    
    public:
    
    /**
     * \brief Available ciphers
     */
    enum cipher
    {
        aes256  ///< AES-256 (Rijndael)
        
        // More to come...
    };  // end cipher
    
    /**
     * \brief Available block modes
     */
    enum block_mode
    {
        ofb     ///< Output Feed Back
        
        // More to come...
    };  // end block_mode
    
    /**
     * \brief A progress monitor interface
     * 
     * This class is used to implement the listener pattern for monitoring
     * the progress of an encryption / decryption operation. External code
     * implements a class derived from this class, and passes a pointer to
     * an object of the derived class when the `encrypt` or `decrypt` methods
     * are called.
     * 
     * The `encrypt` and `decrypt` methods call the `signal_progress` method
     * of this class to report how far the operation has progressed.
     * 
     * Note that there is no safe way to abort the operation prematurely.
     * This is deliberate, because the result would be a corrupted file.
     */
    class ProgressMonitor
    {
        public:
        
        /**
         * \brief Virtual (trivial) destructor for consistency
         */
        virtual ~ProgressMonitor(void) {}
            
        /**
         * \brief Signal that progress has been made in the operation
         * 
         * Derived classes can implement this method to display a percentage,
         * or update a progress bar.
         * 
         * This method is called from the thread in which the `encrypt` or
         * `decrypt` method is running. Callers need to take appropriate
         * measures in multi-threading situations.
         * 
         * \param number_of_bytes_processed The number of bytes that have
         * been processed (encrypted or decrypted) so far
         * 
         * \param total_number_of_bytes The total number of bytes to
         * process
         */
        virtual void signal_progress(
            int64 number_of_bytes_processed,
            int64 total_number_of_bytes) = 0;
    };  // end class ProgressMonitor
    
    /**
     * \brief Encrypt the given file
     *
     * \param filename The path to the file
     *
     * \param c The cipher to use
     *
     * \param bm The block mode to use
     *
     * \param iv The initialisation vector for the block mode; this must be
     * the same size as the block size of the cipher
     *
     * \param key The key to use; this must be the correct length for the
     * cipher
     * 
     * \param progress_monitor An optional pointer to a progress monitor
     * object so that external code can be notified of the progress of the
     * operation
     *
     * \throws Error if there is a problem with access to the file, any of
     * the arguments is incorrect, or there is some issue during the
     * encryption operation
     */
    static void encrypt(
        const std::string& filename,
        cipher c,
        block_mode bm,
        const ByteArray& iv,
        const ByteArray& key,
        ProgressMonitor* progress_monitor = NULL);
        
    /**
     * \brief Decrypt the given file
     *
     * \param filename The path to the file
     *
     * \param c The cipher to use
     *
     * \param bm The block mode to use
     *
     * \param key The key to use; this must be the correct length for the
     * cipher
     *
     * \param progress_monitor An optional pointer to a progress monitor
     * object so that external code can be notified of the progress of the
     * operation
     * 
     * \throws Error if there is a problem with access to the file, any of
     * the arguments is incorrect, or there is some issue during the
     * decryption operation
     */
    static void decrypt(
        const std::string& filename,
        cipher c,
        block_mode bm,
        const ByteArray& key,
        ProgressMonitor* progress_monitor = NULL);
};  // end FileEncrypter

}   // end namespace SCRYFE

#endif
