/**
 * \file Types.cpp Core type implementations
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include <cstring>
#include <sstream>
#include <iomanip>

#ifdef _WIN32
    #include <windows.h>
#else
    #include <sys/mman.h>
#endif

#include "Error.h"
#include "Types.h"

namespace SCRYFE
{
    
ByteArray::ByteArray(void) :
    m_bytes(NULL),
    m_size(0)
{
}   // end constructor

ByteArray::ByteArray(const void* bytes, std::size_t size) :
    m_bytes(NULL),
    m_size(0)
{
    copy_from(bytes, size);
}   // end constructor

ByteArray::~ByteArray(void)
{
    clear();
}   // end destructor

byte ByteArray::at(std::size_t i) const
{
    if ((i < 0) || (i >= m_size))
        SCRYFE_RAISE_ERROR("index out of range when accessing byte array");
    
    return m_bytes[i];
}   // end at

byte& ByteArray::at(std::size_t i)
{
    if ((i < 0) || (i >= m_size))
        SCRYFE_RAISE_ERROR("index out of range when accessing byte array");
    
    return m_bytes[i];
}   // end at

void ByteArray::copy_from(const void* bytes, std::size_t size)
{
    // Clear the array first
    clear();
    m_bytes = new byte[size];
    
    // Prevent the memory from being cached to disk. This is platform-
    // specific
#ifdef _WIN32
    VirtualLock(m_bytes, size);
#else
    mlock(m_bytes, size);
#endif
    
    memcpy(m_bytes, bytes, size);
    m_size = size;
}   // end copy_from

void ByteArray::copy_from_hex(const std::string& s)
{
    std::size_t size = s.size() / 2;
    
    // Check if there is an odd number of hex chars
    if (s.size() % 2) size++;
        
    // Create the byte buffer, and make sure it is deleted if something
    // goes wrong. Note that the buffer is locked into physical RAM down
    // below.
    byte* bytes = new byte[size];
    try
    {
        size_t i = 0, j = 0;
        while (i < s.size())
        {
            // Get the value of the high character. This is slightly
            // inefficient code-wise, but is done this way for clarity.
            char high_char = s[i];
            byte high_byte = 0;
            if ((high_char >= '0') && (high_char <= '9'))
                high_byte = high_char - '0';
            else if ((high_char >= 'a') && (high_char <= 'f'))
                high_byte = high_char - 'a' + 10;
            else if ((high_char >= 'A') && (high_char <= 'F'))
                high_byte = high_char - 'A' + 10;
            else SCRYFE_RAISE_ERROR("illegal character '" << high_char <<
                "' in hex string");
                
            // Now do the next byte in the pair, if there is one.
            i++;
            if (i < s.size())
            {
                char low_char = s[i];
                byte low_byte = 0;
                
                if ((low_char >= '0') && (low_char <= '9'))
                    low_byte = low_char - '0';
                else if ((low_char >= 'a') && (low_char <= 'f'))
                    low_byte = low_char - 'a' + 10;
                else if ((low_char >= 'A') && (low_char <= 'F'))
                    low_byte = low_char - 'A' + 10;
                else SCRYFE_RAISE_ERROR("illegal character '" << low_char <<
                    "' in hex string");
                   
                bytes[j++] = (high_byte << 4) + low_byte;
                i++;
            }
            else
            {
                // We are at the last half-byte (because there is an odd
                // number of characters, so use the high byte value
                // unshifted.
                bytes[j++] = high_byte;
                break;
            }
        }   // end loop through characters
        
        // Now we can clear our original contents, and replace our buffer
        // with the new one.
        clear();
        m_bytes = bytes;
        
    // Prevent the memory from being cached to disk. This is platform-
    // specific
#ifdef _WIN32
    VirtualLock(m_bytes, size);
#else
    mlock(m_bytes, size);
#endif
        
        m_size = size;
    }
    catch (...)
    {
        delete[] bytes;
        throw;
    }
}   // end copy_from_hex

std::string ByteArray::to_hex_string(void) const
{
    std::stringstream s;
    for (std::size_t i = 0; i < m_size; i++)
        s << std::setfill('0') << std::setw(2) << std::hex  << (int)m_bytes[i];
        
    return s.str();
}   // end to_hex_string

void ByteArray::clear(void)
{
    if (m_bytes)
    {
        // Simple security precaution - set the bytes to zero before clearing
        // them.
        set_to_zero();
        delete[] m_bytes;
        
        m_bytes = 0;
        m_size = 0;
    }
}   // end clear

#ifndef _WIN32
/**
 * \brief Declare a pointer to the standard memset function, but make it
 * `volatile`
 *
 * This ensure that when it is called, the compiler won't optimise it out.
 * See http://www.daemonology.net/blog/2014-09-04-how-to-zero-a-buffer.html
 */
static void * (* const volatile memset_ptr)(void *, int, size_t) = memset;

/**
 * \brief Zeros out the given memory, using a version of `memset` that
 * hopefully won't be optimised out by the compiler
 *
 * See http://www.daemonology.net/blog/2014-09-04-how-to-zero-a-buffer.html.
 * Some compilers *may* still optimise this out, but there's not much we can
 * do about it. Hopefully, this makes it less likely... See issue #34 for
 * more details.
 *
 * \param p A pointer to the memory buffer that is to be zeroed out
 *
 * \param len The length of buffer pointed to by `p`
 */
static void secure_memzero(void * p, size_t len)
{
    (memset_ptr)(p, 0, len);
}
#endif

void ByteArray::set_to_zero(void)
{
    if (m_bytes)
    {
        // Do this differently in Windows. See issue #34 for further details.
#ifdef _WIN32
        SecureZeroMemory(m_bytes, m_size);
#else    
        secure_memzero(m_bytes, m_size);
#endif
    }
}   // end set_to_zero
    
void ByteArray::set_to_zero(size_t size)
{
    clear();
    m_bytes = new byte[size];
    m_size = size;
    set_to_zero();
    
    // Prevent the memory from being cached to disk. This is platform-
    // specific
#ifdef _WIN32
    VirtualLock(m_bytes, size);
#else
    mlock(m_bytes, size);
#endif
}   // end set_to_zero

ByteArray ByteArray::xor_with(const ByteArray& rhs) const
{
    // The size of the result is the minimum of self and rhs.
    std::size_t newsize = size();
    if (newsize > rhs.size()) newsize = rhs.size();
    
    // Create a new array to return
    ByteArray result;
    result.set_to_zero(newsize);

    for (size_t i = 0; i < newsize; i++)
        result[i] = at(i) ^ rhs[i];
        
    return result;
}   // end xor_with

ByteArray& ByteArray::concatenate(
        const ByteArray& first, 
        const ByteArray& second,
        ByteArray& result)
{
    // Make sure the result buffer has enough space.
    std::size_t new_size = first.size() + second.size();
    if (result.size() != new_size)
        result.set_to_zero(new_size);
       
    // Copy the first and second arrays into the result array side-by-side
    memcpy(result.bytes(), first.bytes(), first.size());
    memcpy(
        (byte*)result.bytes() + first.size(),
        second.bytes(),
        second.size());
        
    return result;
}   // end concatenate

ByteArray ByteArray::operator+(const ByteArray& second)
{
    ByteArray result;
    return concatenate(*this, second, result);
}   // end operator+

ByteArray& ByteArray::operator+=(const ByteArray& second)
{
    ByteArray result;
    concatenate(*this, second, result);
    operator=(result);
    return *this;
}   //end operator+=

void ByteArray::reverse(void)
{
    for (std::size_t i = 0; i < m_size/2; i++)
    {
        char c = m_bytes[i];
        m_bytes[i] = m_bytes[m_size - i - 1];
        m_bytes[m_size - i - 1] = c;
    }
}   // end reverse

bool ByteArray::operator==(const ByteArray& rhs) const
{
    bool result = true;
    
    // If sizes are different, then it is different
    if (size() == rhs.size())
    {
        // Look for the first non-equal byte.
        for (size_t i = 0; i < size(); i++)
        {
            if (at(i) != rhs[i])
            {
                result = false;
                break;
            }
        }
    }
    else result = false;
    
    return result;
}   // end operator==

ByteArray::ByteArray(const ByteArray& rhs) :
    m_bytes(NULL),
    m_size(0)
{
    copy_from(rhs.m_bytes, rhs.m_size);
}   // end copy constructor

ByteArray& ByteArray::operator=(const ByteArray& rhs)
{
    copy_from(rhs.m_bytes, rhs.m_size);
    return *this;
}   // end operator=

}   // end namespace SCRYFE
