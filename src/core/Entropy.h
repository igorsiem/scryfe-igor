/**
 * \file Entropy.h Declares the Entropy class
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include <queue>

#include "Error.h"
#include "Types.h"
#include "Hash.h"
 
#ifndef _scryfe_entropy_h_included
#define _scryfe_entropy_h_included

namespace SCRYFE
{

/**
 * \brief Entropy-related functionality, including an entropy pool
 *
 * This class maintains a random number pool. 'Raw' randomness can be added
 * from any source with sufficient entropy, and it is 'cooked' using a
 * supplied hashing algorithm. Cooked chunks of randomness are the same size
 * as the output of the hash.
 *
 * Static methods for Shannon's measure of entropy are also included, but are
 * not currently used anywhere else.
 *
 * \todo Update the documentation for this class - it's a bit vague
 */
class Entropy
{
    // --- External interface ---
    
    public:
    
    /**
     * \brief Constructor; initialises the entropy pool so that it uses the
     * given hashing algorithm
     *
     * Note: cooked entropy chunks will be the same size as the output of the
     * hashing algorithm.
     *
     * \param hash The hashing algorithm object to use; this object must
     * exist for the life of the entropy object
     *
     * \param max_cooked_size The maximum number of 'cooked' entropy entries,
     * so that the pool doesn't get too large
     */
    Entropy(
        Hash& hash,
        std::size_t max_cooked_size);
     
    /**
     * \brief Destructor - cleans up the internal entropy pool
     */
    ~Entropy(void);

    /**
     * \brief Securely clear the entropy pool
     */
    void clear(void);
    
    /**
     * \brief The size of each entry in the cooked pool
     *
     * This is the same as the output size of the hash algorithm.
     *
     * \return The number of bytes in a cooked chunk
     */
    std::size_t cooked_entry_size(void) const { return m_hash.output_size(); }
    
    /**
     * \brief The maximum number of cooked entries
     * 
     * This is set so that the cooked entropy pool does not grow infinitely.
     * 
     * \return The maximum number of entries in the cooked pool
     */
    std::size_t max_cooked_size(void) const { return m_max_cooked_size; }
    
    /**
     * \brief The number of entries in the cooked pool
     *
     * \return The number of entries
     */
    std::size_t cooked_size(void) const { return m_cooked.size(); }
    
    /**
     * \brief Cook a chunk of entropy
     *
     * The given bytes are used as the input to the hash function, and the
     * result added a chunk to the pool of 'cooked' randomness. The random
     * bytes submitted to this method should be obtained from some random
     * source such as keyboard timings or mouse movements.
     *
     * \param bytes A pointer to the buffer of random bytes
     *
     * \param size The size of the random buffer in bytes
     *
     * \throws Error if the cooked pool is full
     */
    void cook(const void* bytes, std::size_t size);
    
    /**
     * \brief Cook a chunk of entropy
     *
     * The given bytes are used as the input to the hash function, and the
     * result added a chunk to the pool of 'cooked' randomness. The random
     * bytes submitted to this method should be obtained from some random
     * source such as keyboard timings or mouse movements.
     *
     * \param bytes The array of random bytes
     *
     * \throws Error if the cooked pool is full
     */
    void cook(const ByteArray& bytes) { cook(bytes.bytes(), bytes.size()); }
    
    /**
     * \brief Pop a chunk of cooked randomness from queue
     *
     * \param cooked_entry The byte array to receive the random data
     *
     * \throws Error if there are no cooked entries in the queue (i.e.,
     * cooked_size() returns zero)
     */
    void pop_cooked(ByteArray& cooked_entry);
    
    /**
     * \brief Calculate the Shannon entropy for the given bytes
     *
     * \param bytes A pointer to the bytes
     *
     * \param size The number of bytes in the array
     *
     * \return double The number of bits of Shannon entropy in the
     * sample
     */
    static double shannon_entropy(const void* bytes, std::size_t size);
     
    /**
     * \brief Calculates the Shannon entropy for the given byte array
     *
     * \param bytes The array of bytes
     *
     * \return The number of bits of Shannon entropy in the sample
     */
    static double shannon_entropy(const ByteArray& bytes);
    
    // --- Internal declarations ---
    
    private:
    
    // Disable copy semantics
    Entropy(const Entropy&);
    Entropy& operator=(const Entropy&);
    
    /**
     * \brief The hashing object to use to 'cook' the raw entropy
     */
    Hash& m_hash;
    
    /**
     * \brief The queue of cooked entropy entries; each of these is a byte
     * array that is the same size as the output of the hashing algorithm
     */
    std::queue<ByteArray> m_cooked;
    
    /**
     * \brief The maximum number of cooked entries that can be in the pool
     */
    std::size_t m_max_cooked_size;
};  // end Entropy

}   // end SCRYFE

#endif
