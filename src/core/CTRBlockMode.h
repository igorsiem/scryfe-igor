/**
 * \file CTRBlockMode.h Declares the CTRBlockMode class
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include "BlockMode.h"

#ifndef _scryfe_ctrblockmode_h_included
#define _scryfe_ctrblockmode_h_included

namespace SCRYFE {

/**
 * \brief Implements the Counter (CTR) block cipher mode
 *
 * CTR block mode works by constructing a *counter* by combining a random
 * quantity (equivalent to an IV in other block modes) with a *nonce*
 * (number, used once). The counter is encrypted and XOR'd with the
 * plaintext block (similarly to OFB mode). Each successive block
 * increments the nonce. The essential part of this algorithm is that the
 * counter is unique for each block. The clever part is that the counter
 * simply needs to be incremented by 1 each time, so the operation can be
 * parallelised.
 *
 * The NIST recommendation for implemeting this in the case of a 128-bit
 * block size is to have the top 64-bits as a random IV, and the bottom
 * 64-bits as the nonce. This makes it easy and quick to implement.
 *
 * See http://en.wikipedia.org/wiki/Block_cipher_mode_of_operation and
 * http://csrc.nist.gov/groups/ST/toolkit/BCM/documents/proposedmodes/ctr/ctr-spec.pdf
 *
 * Note that CTR mode shares some characteristics with OFB mode:
 *
 *  *   The *encrypt* operation of the cipher must be used in both
 *      *encrypting* and *decrypting*, because the cipher is used to encrypt
 *      the counter (not the plaintext), and the encrypted counter is then
 *      XOR'd with the plaintext (or the ciphertext to invert the operation).
 *
 *  *   There is no need to keep the randomised component of the counter a
 *      secret
 *
 *  *   CTR with a block cipher can be used as a stream cipher without
 *      padding
 *
 * Note that the initialisation operation applies to the cipher object as
 * well. A given cipher object should not be used by more than one block mode
 * object at the same time.
 *
 * Objects of this class are set up slightly differently to other block modes
 * (such as OFB). The constructor does very little except set up the object.
 * To use it, the `initialise` method must be called, passing in a reference
 * to the cipher, and the other parameters required for encryption.
 *
 * This implementation of CTR only supports ciphers with 128-bit block
 * sizes.
 */
class CTRBlockMode : public BlockMode
{
    // --- External interface ---
    
    public:
    
    /**
     * \brief Constructor - sets all attributes to defaults
     *
     * Note that the block mode cannot be used until it is initialised.
     *
     * \param block_cipher The block cipher to use; note that due to current
     * constraints, only ciphers with a block size of 128 bits can be used;
     * any other block size will cause an exception from the `initialise`
     * method
     */
    explicit CTRBlockMode(BlockCipher& block_cipher);
    
    /**
     * \brief Destructor - clears everything securely
     */
    virtual ~CTRBlockMode(void) { clear(); }
    
    /**
     * \brief Clear everything securely, including the cipher
     */
    virtual void clear(void);
    
    /**
     * \brief Initialise the block mode with a given Initialisation Vector
     * (IV), key and cipher mode
     *
     * \param block_cipher A reference to the block cipher to use; this must
     * have a block size of 128 bits; any other value will cause an
     * exception; note also that this object must exist until initialise is
     * called again with a different cipher object, or this object is
     * destroyed
     *
     * \param iv The initialisation vector; this must be 64-bits long
     *
     * \param key The key with which to initialise the cipher; note that
     * this must be of the correct form and length for the cipher, otherwise
     * an exception is thrown
     *
     * \param m The cipher mode (encryption or decryption); note that this
     * is actually ignored for CTR mode; this is because CTR mode uses the
     * cipher's encrypt operation in *both* directions
     *
     * \param nonce The value to which the nonce is to be initialised; 0 is
     * the default, but some other value may be desirable under some
     * circumstances
     *
     * \throws Error if there was a problem with the initialisation
     * arguments or the initialisation process
     */
    void initialise(
        const ByteArray& iv,
        const ByteArray& key,
        cipher_mode m,
        uint64_t nonce = 0);
        
    /**
     * \brief Process a block (either encrypt or decrypt, depending on
     * initialisation)
     *
     * \todo Parallelise this operation
     *
     * \param input The input block (must be the same size as the block_size)
     *
     * \param output The output block (will be the same size as the
     * block_size)
     *
     * \throws Error if the is a problem with the input or processing
     */
    virtual void process(const ByteArray& input, ByteArray& output);        
    
    // --- Internal declarations ---
    
    private:

    /**
     * \brief The nonce object - incremented with each block
     */
    uint64_t m_nonce;
    
    /**
     * \brief The initialisation vector - the top 64 bits of the counter
     */
    ByteArray m_iv;
    
    // Disable copy semantics
    CTRBlockMode(const CTRBlockMode&);
    CTRBlockMode& operator=(const CTRBlockMode&);
};  // end CTRBlockMode

}   // end namespace SCRYFE
#endif
