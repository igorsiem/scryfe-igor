/**
 * \file SHAHash.cpp Implements the SHA*Hash series of classes
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include "SHAHash.h"
#include <string>
#include <cstring>
#include "../third-party/algorithms/sha/sha.h"
#include "Error.h"

namespace SCRYFE
{

/**
 * \brief A quick method of returning error messages for SHA operations
 */
std::string sha_error_message(int result_code)
{
    std::string message = "unknown SHA hashing error";
    switch (result_code)
    {
        case (int)(shaSuccess):
            message = "no SHA error";
            break;
            
        case (int)(shaNull):
            message = "SHA NULL pointer error";
            break;
            
        case (int)(shaInputTooLong):
            message = "SHA input is too long";
            break;
            
        case (int)(shaStateError):
            message = "SHA state error";
            break;
            
        case (int)(shaBadParam):
            message = "SHA bad parameter error";
            break;
            
        default: break;
    }
    
    return message;
}   // end sha_error_message

void SHA256Hash::process(const ByteArray& input, ByteArray& output)
{
    // Set the output bytes to the right size, filled with zeros.
    output.set_to_zero(SHA256HashSize);

    // Initialise the SHA256 hashing context.
    SHA256Context context;
    int result = SHA256Reset(&context);
    if (result) SCRYFE_RAISE_ERROR(sha_error_message(result));
    
    // Set the input and retrieve the output, checking for errors at each
    // stage.
    result = SHA256Input(&context, (const uint8_t*)input.bytes(),
        input.size());
    if (result) SCRYFE_RAISE_ERROR(sha_error_message(result));
    
    result = SHA256Result(&context, (uint8_t*)output.bytes());
    if (result) SCRYFE_RAISE_ERROR(sha_error_message(result));
    
    // Clear the hashing context, just in case.
    memset(&context, 0, sizeof(context));
}   // end process

}   // end namespace SCRYFE
