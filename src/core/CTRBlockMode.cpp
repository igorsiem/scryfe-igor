/**
 * \file CTRBlockMode.cpp Implements the CTRBlockMode class
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */
 
#include "CTRBlockMode.h"
#include "Error.h"

// TODO remove debugging code
#include <iostream>

namespace SCRYFE {

CTRBlockMode::CTRBlockMode(BlockCipher& block_cipher) :
    BlockMode(block_cipher),
    m_nonce(0),
    m_iv()
{
}   // end constructor

void CTRBlockMode::clear(void)
{
    BlockMode::clear();
    m_nonce = 0;
    m_iv.clear();
}   // end clear

void CTRBlockMode::initialise(
        const ByteArray& iv,
        const ByteArray& key,
        cipher_mode m,
        uint64_t nonce)
{
    // First, check the block size of the cipher.
    if (block_size() != 16)
        SCRYFE_RAISE_ERROR("An attempt has been made to use CTR block "
            "mode with a cipher that has a block size of " << block_size() <<
            " byte(s); this implementation of CTR block mode mode only"
            " supports ciphers with a block size of 16 bytes.");
            
    // Check the IV size.
    if (iv.size() != 8)
        SCRYFE_RAISE_ERROR("CTR block mode was initialised with an "
            "initialisation vector that is " << iv.size() << " byte(s) "
            "long; an IV that is 8 bytes long is required");
            
    m_nonce = nonce;
    m_iv = iv;
    
    m_block_cipher.initialise(key, encrypt);
}   // end initialise

void CTRBlockMode::process(const ByteArray& input, ByteArray& output)
{
    // Make sure the input is the right size. It can be SMALLER than the
    // block size, but not larger.
    if (input.size() > block_size())
        SCRYFE_RAISE_ERROR("An attempt was made to encrypt an input block "
            "of " << input.size() << " using an CTR Block Mode object with "
            "a block size of " << block_size() << " bytes");
            
    // Put together our input counter.
    ByteArray counter, e;
    
    // Increment the nonce (after it has been copied).
    ByteArray nonce_bytes(&m_nonce, sizeof(m_nonce));
    m_nonce++;
    
    counter = m_iv;
    nonce_bytes.reverse();
    counter += nonce_bytes;
    
    // Do the processing
    m_block_cipher.process(counter, e);
    output = e ^ input;
}   // end process     

}   // end namespace SCRYFE
