/**
 * \file SHAHash.h Declares the SHA*Hash series of classes
 *
 * \todo Classes for other variants of SHA hashing
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include "Hash.h"
 
#ifndef _scryfe_shahash_h_included
#define _scryfe_shahash_h_included

namespace SCRYFE
{

/**
 * \brief A SHA-256 bit hash
 *
 * Internal code uses the IETF SHA-256 implementation
 */
class SHA256Hash : public Hash
{
    // --- External interface ---
    
    public:
    
    /**
     * \brief Trivial constructor
     */
    SHA256Hash(void) : Hash() {}
    
    /**
     * \brief Trivial destructor
     */
    virtual ~SHA256Hash(void) {}
    
    /**
     * \brief Perform the hashing operation
     *
     * \param input The input bytes
     *
     * \param output The message digest (anything that was previously in
     * the byte array is cleared)
     *
     * \throws Error if a problem occurs with hashing
     */
    virtual void process(const ByteArray& input, ByteArray& output);
    
    /**
     * \brief The output size of the hashing algorithm, in bytes
     *
     * \return The output size of the algorithm, in bytes
     */
    virtual std::size_t output_size(void) const { return 32; }
    
};  // end SHA256Hash

}   // end namespace SCRYFE

#endif
