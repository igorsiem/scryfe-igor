/**
 * \file Error.h Declares the Error class
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include <stdexcept>
#include <string>
#include <sstream>

#ifndef _scryfe_Error_h_included
#define _scryfe_Error_h_included

namespace SCRYFE
{

/**
 * \brief A base class for all error exceptions in *SCryFE*
 *
 * This is a simple error-signalling class that includes a single message.
 * It can be used directly, or specialised in other parts of the project.
 * Objects of this class or its derivatives should be used for signalling
 * *all* errors in *SCryFE*, and these classes should *not* be used for any
 * other purpose than to signal errors.
 */
class Error : public std::exception
{
    // --- Public interface ---
    
    public:
        
    /**
     * \brief Constructor, initialising the error message
     * 
     * \param message The message explaining the error
     */
    explicit Error(const std::string& message);
    
    /**
     * \brief Trivial destructor
     */
    virtual ~Error(void) throw () {}
    
    /**
     * \brief Retrieve the message associated with the error
     * 
     * \return A pointer to a string containing the error message
     */
    virtual const char* what(void) const throw()
        { return m_message.c_str(); }
        
    /**
     * \brief Copy constructor
     * 
     * \param rhs The object from which to copy
     */
    Error(const Error& rhs);
    
    /**
     * \brief Assignment operator
     * 
     * \param rhs The object from which to copy
     * 
     * \return A reference to copied self
     */
    Error& operator=(const Error& rhs);
    
    // --- Internal declarations ---
    
    private:
        
    /**
     * \brief The error message
     */
    std::string m_message;
};  // end Error

/**
 * \brief 'Convenience' macro for throwing an Error exception with streamable
 * message arguments
 * 
 * \param message This can have multiple arguments of different types,
 * separated by the 'stream' operator (<<)
 */
#define SCRYFE_RAISE_ERROR( message ) \
{ \
    std::stringstream msg; \
    msg << message; \
    throw ::SCRYFE::Error(msg.str()); \
}

}   // end namespace SCRYFE

#endif
