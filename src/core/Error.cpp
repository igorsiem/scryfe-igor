/**
 * \file Error.h Implements the Error class
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include "Error.h"

namespace SCRYFE
{
    
Error::Error(const std::string& message) :
    std::exception(),
    m_message(message)
{
}   // end constructor
    
Error::Error(const Error& rhs) :
    std::exception(rhs),
    m_message(rhs.m_message)
{
}   // end copy constructor
    
Error& Error::operator=(const Error& rhs)
{
    std::exception::operator=(rhs);
    m_message = rhs.m_message;
    return *this;
}   // end operator=

}   // end namespace SCRYFE
