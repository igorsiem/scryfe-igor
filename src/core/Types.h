/**
 * \file Types.h Core type declarations
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include <stdint.h>
#include <cstddef>
#include <string>

#ifndef _scryfe_types_h_included
#define _scryfe_types_h_included

namespace SCRYFE
{
    
/**
 * \brief A single 8-bit quantity
 */
typedef uint8_t byte;

/**
 * \brief An array of bytes of arbitrary length
 * 
 * This class manages a byte array right down to the allocation and
 * deallocation of memory. It is intended for cryptographic use, for things
 * like keys, bitwise XOR operations and buffers.
 * 
 * Because objects of this class will manage secret data, it class takes the
 * following security precautions:
 *
 * *   Internal buffers are prevented from being cached to disk (system-
 *     specific API calls)
 *
 * *   Internal buffers are cleared to zero before being deallocated
 * 
 * \todo Implement more cryptographic security functionality - moving memory
 * around, memory clearing with random numbers, and so on.
 */
class ByteArray
{
    // --- External interface ---
    
    public:
        
    /**
     * \brief Default constructor = initialises to an empty array
     */
    ByteArray(void);
    
    /**
     * \brief Constructor initialising the array, copying the supplied bytes
     * to an internal buffer
     * 
     * \param bytes A pointer to the bytes
     * 
     * \param size The number of bytes to copy
     */
    ByteArray(const void* bytes, std::size_t size);
    
    /**
     * \brief Destructor - destroys the internal buffer
     */
    virtual ~ByteArray(void);
    
    // -- Accessors --
    
    /**
     * \brief Retrieve the size of the array, in bytes
     * 
     * \return The size of the array in bytes
     */
    std::size_t size(void) const { return m_size; }
    
    /**
     * \brief Const access to a byte in the array (using [] operator)
     * 
     * \param i The index of the byte to access; must be in the range
     * [0,size)
     * 
     * \throws Error If i is out of range
     */
    byte at(std::size_t i) const;
    
    /**
     * \brief Const access to a byte in the array (using [] operator)
     * 
     * \param i The index of the byte to access; must be in the range
     * [0,size)
     * 
     * \throws Error If i is out of range
     */
    byte operator[](std::size_t i) const { return at(i); }
    
    /**
     * \brief Non-const access to a byte in the array (can be used as an
     * l-value)
     * 
     * \param i The index of the byte to access; must be in the range
     * [0,size)
     * 
     * \throws Error If i is out of range
     */
    byte& at(std::size_t i);
    
    /**
     * \brief Non-const access to a byte in the array (using the [] operator;
     * can be used as an l-value)
     * 
     * \param i The index of the byte to access; must be in the range
     * [0,size)
     * 
     * \throws Error If i is out of range
     */
    byte& operator[](std::size_t i) { return at(i); }
    
    /**
     * \brief Copy the given bytes into the array, replacing the existing
     * contents
     * 
     * \param bytes A pointer to the bytes to be copied
     * 
     * \param size The size of the byte array
     */
    void copy_from(const void* bytes, std::size_t size);
    
    /**
     * \brief Get bytes from a hex string
     *
     * This method parses a hex string to fill the byte array. If there is
     * an odd number of hex characters, the last character is used to
     * populate the LOW bits of the last byte.
     *
     * Upper and lower-case hex letter are supported.
     *
     * \param s The hex string to parse
     *
     * \throws Error if any character is not a hexadecimal character
     */
    void copy_from_hex(const std::string& s);
    
    /**
     * \brief Express the byte array as a hex string
     *
     * \return A hex string representing the byte array
     */
    std::string to_hex_string(void) const;
    
    /**
     * \brief Clear the byte array
     *
     * Note that, as a simple security precaution, the bytes are set to zero
     * before the memory is freed.
     */
    void clear(void);
    
    /**
     * \brief Set all the bytes in the existing array to zero
     *
     * This method has no effect if the array is empty.
     *
     * Note that the main reason for this method is security. On windows,
     * this method uses the `SecureZeroMemory` method, which is guaranteed to
     * work. On posix system, we're using (a slightly hacked version of)
     * `memset`, which may be optimised out by some compilers (see 
     * http://www.daemonology.net/blog/2014-09-04-how-to-zero-a-buffer.html)
     *
     * See issue #34 for more details.
     */
    void set_to_zero(void);
    
    /**
     * \brief Clear the existing array, and create a new array of the given
     * length, with bytes set to zero
     *
     * \param size The size of the new array
     */
    void set_to_zero(size_t size);
    
    /**
     * \brief Const access to the raw byte array
     *
     * \return Pointer to the internal bytes buffer
     */
    const void* bytes(void) const { return m_bytes; }
    
    /**
     * \brief Non-const access to the raw byte array
     * 
     * Note: *this is dangerous - DON'T go off the edge of the buffer!*.
     */
    void* bytes(void) { return m_bytes; }
    
    // -- Bitwise operations --
    
    /**
     * \brief Bit-wise XOR across the bytes of the array
     * 
     * Corresponding bytes in the array are XOR'd together to give the new
     * result array. If one array is shorter than the other, the operation
     * stops at the end of the shorter array, and the result has the same
     * length as the shorter array.
     * 
     * \todo Do a version of this method that takes the result array as a
     * reference argument to avoid the extra copy
     * 
     * \param rhs The array with which to compare
     * 
     * \return An array that is the XOR product of self and rhs
     */
    ByteArray xor_with(const ByteArray& rhs) const;
    
    /**
     * \brief Bit-wise XOR across the bytes of the array (xor operator)
     * 
     * Corresponding bytes in the array are XOR'd together to give the new
     * result array. If one array is shorter than the other, the operation
     * stops at the end of the shorter array, and the result has the same
     * length as the shorter array.
     *
     * \param rhs The array with which to compare
     * 
     * \return An array that is the XOR product of self and rhs
     */
    ByteArray operator^(const ByteArray& rhs) const
        { return xor_with(rhs); }
        
    // -- Concatenation --
    
    /**
     * \brief Concatenate two byte array objects
     *
     * Note that the result vector will be resized if it is not the exact
     * size that it needs to be. This should be kept in mind when optimising.
     *
     * \param first The first byte array
     *
     * \param second The second byte array
     *
     * \param result A reference to the byte vector that will receive the
     * concatenated result
     *
     * \return A reference to the result object, for convenience.
     */
    static ByteArray& concatenate(
        const ByteArray& first, 
        const ByteArray& second,
        ByteArray& result);
        
    /**
     * \brief Addition operator (performs concatenation)
     *
     * Note: this is not particularly efficient as it always involves a copy.
     *
     * \param second The second array to concatenate to this one
     *
     * \return A new byte array that is the concatenation of self and second
     */
    ByteArray operator+(const ByteArray& second);
    
    /**
     * \brief Increment operator (performs concatenation)
     *
     * Note: this is not particularly efficient as it always involves two
     * copy operations.
     *
     * \param second The second array to concatenate to this one
     *
     * \return A reference to concatenated self
     */
    ByteArray& operator+=(const ByteArray& second);
    
    /**
     * \brief Reverse the order of the bytes in the array
     *
     * \todo The implementation currently uses a temporary copy; need to
     * do a more efficient version
     */
    void reverse(void);
    
    // -- Logical operations --
    
    /**
     * \brief Equality comparison
     * 
     * \param rhs The object with which to compare
     *
     * \return True if the arrays are equal
     */
    bool operator==(const ByteArray& rhs) const;
    
    /**
     * \brief Inequality comparison
     * 
     * \param rhs The object with which to compare
     * 
     * \return True if the arrays are not equal
     */
    bool operator!=(const ByteArray& rhs) const
        { return !operator==(rhs); }
        
    // -- Copy Semantics --
    
    /**
     * \brief Copy constructor
     * 
     * \param rhs The object from which to copy
     */
    ByteArray(const ByteArray& rhs);
    
    /**
     * \brief Assignment operator
     * 
     * \param rhs The object from which to copy
     * 
     * \return A reference to copied self
     */
    ByteArray& operator=(const ByteArray& rhs);
    
    // --- Internal declarations ---
    
    private:
        
    /**
     * \brief The size of the array
     */
    std::size_t m_size;
    
    /**
     * \brief The array of bytes
     */
    byte* m_bytes;
};  // end class ByteArray

/**
 * \brief The mode in which a cipher is to work (encryption or decryption)
 */
enum cipher_mode
{
    none,       ///< Mode has not been set
    encrypt,    ///< Cipher will encrypt
    decrypt     ///< Cipher will decrypt
};

}   // end namespace SCRYFE

#endif
