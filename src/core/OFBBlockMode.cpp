/**
 * \file OFBBlockMode.cpp Implements the OFBBlockMode class
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include "OFBBlockMode.h"
#include "Error.h"

namespace SCRYFE
{

OFBBlockMode::OFBBlockMode(BlockCipher& block_cipher) :
    BlockMode(block_cipher),
    m_input_state()
{
}   // end constructor

void OFBBlockMode::clear(void)
{
    BlockMode::clear();
    m_input_state.clear();
}   // end clear

void OFBBlockMode::initialise(
        const ByteArray& iv,
        const ByteArray& key,
        cipher_mode)
{
    if (iv.size() != block_size())
        SCRYFE_RAISE_ERROR("OFB block mode was initialised using an "
            "initialisation vector that is " << iv.size() << " bytes long; "
            "an IV that is " << block_size() << " bytes long is required");
            
    m_input_state = iv;
    m_block_cipher.initialise(key, encrypt);
}   // end initialise

void OFBBlockMode::process(const ByteArray& input, ByteArray& output)
{
    // Make sure the input is the right size. It can be SMALLER than the
    // block size, but not larger.
    if (input.size() > block_size())
        SCRYFE_RAISE_ERROR("An attempt was made to encrypt an input block "
            "of " << input.size() << " using an OFB Block Mode object with "
            "a block size of " << block_size() << " bytes");
        
    // Get our new input state - it will be used for the XOR with this
    // plaintext, then encrypted for the next block.
    ByteArray new_input_state;
    m_block_cipher.process(m_input_state, new_input_state);
    m_input_state = new_input_state;
    
    // Now we can XOR the output.
    output = m_input_state ^ input;
}   // end process 

}   // end namespace SCRYFE
