/**
 * \file version.h Version information for *Crypto*
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

namespace SCRYFE
{
    
const char* version = "0.1.0.0";

};  // end namespace SCRYFE
