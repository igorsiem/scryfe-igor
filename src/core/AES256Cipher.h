/**
 * \file AES256Cipher.h Declares the AES256Cipher class
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include "Cipher.h"
#include "../third-party/algorithms/rijndael/Rijndael.h"

#ifndef _scryfe_aes256_cipher_h_included
#define _scryfe_aes256_cipher_h_included

namespace SCRYFE
{

/**
 * \brief Interface for the AES-256 (Rijndael) encryption algorithm
 * 
 * This class wraps the standard Rijndael reference implementation. After
 * construction, the cipher object needs to be initialised with a key for
 * encryption or decryption, after which the `process` method may be called
 * for multiple 128 bit (16 byte) blocks.
 * 
 * Note that this cipher class should be used with an appropriate block mode,
 * rather than just encrypting data by itself. This would equivalent to using
 * Eletronic Code Book (ECB) mode, which is very weak.
 */
class AES256Cipher : public BlockCipher
{
    // --- External interface ---
    
    public:
        
    // -- External methods --
    
    /**
     * \brief Trivial constructor
     */
    AES256Cipher(void);
    
    /**
     * \brief Destructor; clears the internal state buffer
     */
    virtual ~AES256Cipher(void);
    
    /**
     * \brief Retrieve the block size of the cipher
     *
     * \return The block size of the cipher (in bytes)
     */
    virtual std::size_t block_size(void) const { return 16; }
    
    /**
     * \brief Clear the internal state buffer
     * 
     * This is mainly used for security purposes. Note that the cipher must
     * be re-initialised before it can be used after this method is called.
     */
    void clear(void);
    
    /**
     * \brief Initialise the cipher for processing with a given key and
     * mode
     *
     * \param key The key to use; this must be in the correct form and length
     * for the cipher, or an exception is thrown
     *
     * \param m The mode to use for the cipher (encrypt or decrypt)
     *
     * \throws Error if there was a problem with the arguments or
     * initialisation
     */
    virtual void initialise(const ByteArray& key, cipher_mode m);    
        
    /**
     * \brief Retrieve the mode in which the cipher is operating
     *
     * \return The cipher mode
     */
    cipher_mode mode(void) const { return m_mode; }
    
    /**
     * \brief Perform encryption or decryption
     * 
     * It is assumed that the cipher has been initialised, and is ready to
     * go (otherwise an exception is thrown).
     * 
     * The implementation may throw an exception for any reason.
     * 
     * \param input The input block to process
     * 
     * \param output The output byte array
     * 
     * \throws Error if anything goes wrong
     */
    virtual void process(const ByteArray& input, ByteArray& output);
    
    // --- Internal declarations ---
    
    private:
    
    // Disable copy semantics
    AES256Cipher(const AES256Cipher&);
    AES256Cipher& operator=(const AES256Cipher&);
    
    /**
     * \brief The mode in which the cipher has been initialised (`none` if
     * the cipher has not been initialised)
     */
    cipher_mode m_mode;
    
    /**
     * \brief The encryption working buffer
     */
    unsigned long m_rk[RKLENGTH(256)];
    
    /**
     * \brief The number of rounds for encryption
     */
    int m_rounds;
};  // end AES256Cipher

}   // end namespace SCRYFE

#endif
