/**
 * \file BlockMode.h Declares the BlockMode base class
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include "Cipher.h"
 
#ifndef _scryfe_blockmode_h_included
#define _scryfe_blockmode_h_included

namespace SCRYFE
{

/**
 * \brief Base class for block cipher mode implementations
 *
 * Block modes are used to link the results of one block encryption /
 * decryption operation with other blocks, to help obscure the overall
 * structure of the plaintext.
 *
 * A good reference for why this is necessary, and some of the different
 * ways of doing it can be found
 * [here](http://en.wikipedia.org/wiki/Block_cipher_mode).
 *
 * This is a base class for implementations of individual block modes. These
 * are set up with an instance of a particular block cipher, and then
 * initialised for encryption or decryption, usually with a randomised
 * Initialisation Vector (IV) that is the same size as the block size of the
 * cipher. Encryption and decryption can then proceed in sequence.
 *
 * Note that block modes have *state*, and the order of blocks (usually)
 * matters (the exception is CTR mode, for which block encryption and
 * decryption can be parallelised). This also means that a given cipher
 * object should not be used by more than one block mode object
 * simultaneously.
 */
class BlockMode
{
    // --- Public interface ---
    
    public:
    
    /**
     * \brief Constructor, setting up the reference to the block cipher being
     * used
     *
     * \param block_cipher A reference to the block cipher that will be used;
     * this object must exist for the life of the block cipher
     */
    explicit BlockMode(BlockCipher& block_cipher) :
        m_block_cipher(block_cipher) {}
    
    /**
     * \brief Destructor - clears the state of the cipher securely
     */
    virtual ~BlockMode(void) { clear(); }
    
    /**
     * \brief Securely clear the state of the cipher
     */
    virtual void clear(void) { m_block_cipher.clear(); }  
    
    /**
     * \brief Retrieve the block size being used (this is the same as the
     * block size of the cipher)
     */
    virtual std::size_t block_size(void) const
        { return m_block_cipher.block_size(); }
        
    /**
     * \brief Process a block (either encrypting or decrypting, depending
     * on how the block mode object has been set up)
     *
     * \param input The input block (must be the same size as the block_size)
     *
     * \param output The output block (will be the same size as the
     * block_size)
     *
     * \throws Error if the is a problem with the input or procesing
     */
    virtual void process(const ByteArray& input, ByteArray& output) = 0;
    
    // --- Internal declarations ---
    
    protected:

    /**
     * \brief A reference to the block cipher object being used
     */
    BlockCipher& m_block_cipher;
        
    private:
    
    // Disable copy semantics
    BlockMode(const BlockMode&);
    BlockMode& operator=(const BlockMode&);
};  // end BlockMode

}   // end namespace SCRYFE

#endif
