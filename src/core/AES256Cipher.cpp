/**
 * \file AES256Cipher.cpp Implements the AES256Cipher class
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include <cstring>
#include "AES256Cipher.h"
#include "Error.h"

namespace SCRYFE
{
    
AES256Cipher::AES256Cipher(void) :
    BlockCipher(),
    m_mode(none),
    m_rounds(0)
{
    // Clear the working buffer
    memset(m_rk, 0, RKLENGTH(256) * sizeof(unsigned long));
}   // end constructor

AES256Cipher::~AES256Cipher(void)
{
    // Clear the working buffer.
    clear();
}   // end destructor

void AES256Cipher::clear(void)
{
    memset(m_rk, 0, RKLENGTH(256) * sizeof(unsigned long));
    m_mode = none;
    m_rounds = 0;
}   // end clear

void AES256Cipher::initialise(const ByteArray& key, cipher_mode m)
{
    // Check the key size.
    if (key.size() != 32)
        SCRYFE_RAISE_ERROR("AES-256 cipher initialised with a key that "
            "is " << key.size() << " bytes long; AES-256 keys must be 32 "
            "bytes in length");
        
    // Check the operating mode.
    if (m == encrypt)
        m_rounds = rijndaelSetupEncrypt(m_rk,
            (const unsigned char*)key.bytes(), 256);
    else if (m == decrypt)
        m_rounds = rijndaelSetupDecrypt(m_rk,
            (const unsigned char*)key.bytes(), 256);
    else
        SCRYFE_RAISE_ERROR("AES-256 cipher is being initialised with an "
            "unknown operating mode");
        
    m_mode = m;
}   // end initialise

void AES256Cipher::process(const ByteArray& input, ByteArray& output)
{
    if (input.size() != 16)
        SCRYFE_RAISE_ERROR("AES-256 cipher being used to encrypt or decrypt "
            "input that is " << input.size() << " bytes long; input to this "
            "cipher must be 16 bytes");
        
    if (m_mode == encrypt)
    {
        output.set_to_zero(16);
        rijndaelEncrypt(m_rk, m_rounds, (const unsigned char*)input.bytes(),
            (unsigned char*)output.bytes());
    }
    else if (m_mode == decrypt)
    {
        output.set_to_zero(16);
        rijndaelDecrypt(m_rk, m_rounds, (const unsigned char*)input.bytes(),
            (unsigned char*)output.bytes());
    }
    else
        SCRYFE_RAISE_ERROR("AES-256 cipher being used to encrypt or decrypt "
            "when it has not been initialised");
}   // end process

}   // end namespace SCRYFE
