/**
 * \file OFBBlockMode.h Declares the OFBBlockMode class
 * 
 * \author Igor Siemienowicz
 * 
 * \copyright The content of this file is copyright (c) 2014 by the named
 * author(s), and licensed under terms of the Gnu Lesser General Public
 * License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses`
 * directory for the full text of this license.
 */

#include "BlockMode.h"

#ifndef _scryfe_ofbblockmode_h_included
#define _scryfe_ofbblockmode_h_included

namespace SCRYFE
{

/**
 * \brief Implements the Output FeedBack cipher block mode
 *
 * OFB mode words by using the selected cipher (and key) to encrypt a
 * random Initialisation Vector (IV), which is then XORd with the plaintext
 * to produce the ciphertext. The encrypted IV (prior to the XOR operation)
 * is used as the IV for encrypting the *next* block of plaintext, and so on.
 *
 * This is explained in more detail in the [Wikipedia article on block cipher
 * modes](http://en.wikipedia.org/wiki/Block_cipher_mode), but some important
 * things to note for this block mode are:
 *
 *  *   The *decrypt* operation must used the cipher in *encrypt* mode (again
 *      starting with the IV) and the result XORd with the ciphertext.
 *
 *  *   This technique essentially uses the cipher to generate a pseudo-
 *      random one-time pad, seeded by the IV. However, as with most block
 *      modes, there is no need to keep the IV secret as long as the key is
 *      kept secret.
 *
 *  *   Because of the XOR operation, this block mode effectively turns the
 *      block cipher into a stream cipher. The final block of ciphertext need
 *      only be as long as the corresponding plaintext.
 *
 * Note that the initialisation operation applies to the cipher object as
 * well. A given cipher object should not be used by more than one block mode
 * object at the same time.
 */
class OFBBlockMode : public BlockMode
{
    // --- Public interface ---
    
    public:
    
    /**
     * \brief Constructor, setting the cipher object reference
     *
     * \param block_cipher A reference to the block cipher to be used; this
     * object must last at least the life time of the OFBBlockMode object
     */
    explicit OFBBlockMode(BlockCipher& block_cipher);
    
    /**
     * \brief Destructor - clears everything for security, including the
     * cipher object
     */
    virtual ~OFBBlockMode(void) { clear(); }
    
    /**
     * \brief Clear everything securely, including the cipher
     */
    virtual void clear(void);
    
    /**
     * \brief Initialise the block mode with a given Initialisation Vector
     * (IV), key and cipher mode
     *
     * \param iv The initialisation vector; this must be the same length as
     * the block length of the cipher
     *
     * \param key The key with which to initialise the cipher; note that
     * this must be of the correct form and length for the cipher, otherwise
     * an exception is thrown
     *
     * \param m The cipher mode (encryption or decryption); note that this
     * is actually ignored for OFB mode; this is because OFB mode uses the
     * cipher's encrypt operation in *both* directions
     *
     * \throws Error if there was a problem with the initialisation
     * arguments or the initialisation process
     */
    void initialise(
        const ByteArray& iv,
        const ByteArray& key,
        cipher_mode m);
    
    /**
     * \brief Process a block (either encrypt or decrypt, depending on
     * initialisation)
     *
     * \param input The input block (must be the same size as the block_size)
     *
     * \param output The output block (will be the same size as the
     * block_size)
     *
     * \throws Error if the is a problem with the input or processing
     */
    virtual void process(const ByteArray& input, ByteArray& output);
    
    // --- Internal declarations ---
    
    private:
    
    /**
     * \brief The current input state of the OFB mode
     *
     * This is used as the input for the encryption operation. It is
     * initialised using the IV, and replaced by the output of the
     * encryption operation before being XORd with each block.
     */
    ByteArray m_input_state;
    
    // Disable copy semantics
    OFBBlockMode(const OFBBlockMode&);
    OFBBlockMode& operator=(const OFBBlockMode&);
};  // end OFBBlockMode
 
}   // end namespace SCRYFE

#endif
