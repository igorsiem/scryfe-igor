`include` Directory
===================
This directory structure is created for a single-point include, so that
library users can set compilers to use `src/include` as an include directory,
and then simply put the following in code:

    #include <scryfe/scryfe.h>
    
... and get all of *SCryFE* functionality in one hit.
