# File:         Rakefile
# Description:  Top-level rake file for the SCryFE project
# Author:       Igor Siemienowicz
# Copyright:    The content of this file is copyright (c) 2014 by the named
#               author(s), and licensed under terms of the Gnu Lesser General
#               Public License (LGPL). See the file `lgpl-3.0.txt` in the
#               `doc/licenses` directory for the full text of this license.

require 'digest'

# --- CONFIGURATION ---

# The top-level build directory - everything generated goes here.
BUILD_DIR = "build"

# Deliverable products go here
DELIVERABLES_DIR = BUILD_DIR + "/deliverables"

# --- TASKS ---

# Set up clean and clobber tasks
require 'rake/clean'
CLOBBER.include(BUILD_DIR)

# Include rake tasks from subdirectories
Dir.glob("src/*.rake").each { |r| import r }
Dir.glob("src/test/*.rake").each { |r| import r }
Dir.glob("doc/*.rake").each { |r| import r }

# -- Deliverables ---

directory DELIVERABLES_DIR

desc "build all deliverable artefacts (except website)"
task :deliverables => DELIVERABLES_DIR
    # Note: other sub-tasks will add items to the :deliverables task

# -- MD5 Digests of all deliverable files (excluding the website) --

# Produce digests of all the deliverables
desc "generate the list of md5 digests for all deliverables"
task :md5_digests => :deliverables do
    digests = {}
    Dir.glob("#{DELIVERABLES_DIR}/*.*") do |filename|
        digests[filename] = Digest::MD5.file(filename).hexdigest
    end
    
    file = File.open("#{DELIVERABLES_DIR}/digests.txt", "w");
    file.puts("SCryFE Deliverables\n" +
              "===================\n");
    digests.each { |filename, digest| file.puts "#{filename} = #{digest}" }
end

# -- Top-level stuff --

desc "build and test everything (except website)"
task :all => :md5_digests
    # Note: other sub-tasks will additems to the :all task

desc "default build action (== 'all')"
task :default => :all

# --- MISCELLANEOUS ---

# -- Determine SCryFE Version --

SCRYFE_VERSION_FILENAME = "src/core/version.cpp"

# Open the SCryFE version file, and look for the line containing the version
# string.
versioninfo = nil
File.open(SCRYFE_VERSION_FILENAME).each do |line|
    versioninfo = line.match(/version\s=\s"(\d)\.(\d)\.(\d)\.(\d)"/)
    
    # Did we get the version info in this line?
    if (versioninfo)
        # Check that we got four parts
        if (versioninfo.size != 5)
            raise "found version info, but it contained " +
                "#{versioninfo.size} parts; 5 parts are required"
        end
        
        $SCRYFE_MAJOR_VERSION = versioninfo[1].to_i
        $SCRYFE_MINOR_VERSION = versioninfo[2].to_i
        $SCRYFE_PATCH = versioninfo[3].to_i
        $SCRYFE_BUILD = versioninfo[4].to_i
        
        break
    end
end # end each line in file

# Check that we got version information
if $SCRYFE_MAJOR_VERSION == nil
    raise "could not locate version information in " +
            "#{SCRYFE_VERSION_FILENAME}"
end

# Put the version info into environment variables so that other things (like
# cmake) can access them.
$SCRYFE_VERSION =
        "#{$SCRYFE_MAJOR_VERSION}." +
        "#{$SCRYFE_MINOR_VERSION}." +
        "#{$SCRYFE_PATCH}." +
        "#{$SCRYFE_BUILD}"
        
ENV['SCRYFE_MAJOR_VERSION'] = $SCRYFE_MAJOR_VERSION.to_s
ENV['SCRYFE_MINOR_VERSION'] = $SCRYFE_MINOR_VERSION.to_s
ENV['SCRYFE_PATCH'] = $SCRYFE_PATCH.to_s
ENV['SCRYFE_BUILD'] = $SCRYFE_BUILD.to_s
ENV['SCRYFE_VERSION'] = $SCRYFE_VERSION

puts "SCryFE version: #{$SCRYFE_MAJOR_VERSION}.#{$SCRYFE_MINOR_VERSION}" +
    ".#{$SCRYFE_PATCH}.#{$SCRYFE_BUILD}"

# -- Determine what platform we're on --

# This is easier in later versions of ruby / rake, but we want to support as
# wide a range of options as possible.
$PLATFORM = :linux
$PLATFORM = :windows if (Rake::Win32::windows?)
puts "Platform is #{$PLATFORM}"

# Use the ruby 'pack' trick to determine platform bit-ness
$WORDSIZE = 32
$WORDSIZE = 64 if ['foo'].pack("p").size == 8
ENV['WORDSIZE'] = "#{$WORDSIZE}"
puts "System word size is #{$WORDSIZE}-bit"

if $PLATFORM == :windows
    puts "Only 32-bit windows supported for now (see issue #21)"
end

# TODO OSX
# TODO 32-bit and 64-bit
# TODO Variations of linux
