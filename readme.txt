The SCryFE Project
==================
*"Strong Crypto For Everyone"*

Welcome to *SCryFE*, a project for creating a set of strong crypto products
that anyone can use, and use properly.

If you want to know what this project is all about, the best place to start
is the *SCryFE Manifesto* document. This is located in the `doc`
subdirectory, in a file called `SCryFE-Manifesto.md`. It is in markdown
format, so it can be read pretty-much anywhere.

Index
-----
*   `doc` - Documentation

*   `src` - Source code

*   `Rakefile` - Instructions for the `rake` utility for building all of
    *SCryFE*