# File:         doc.rake
#
# Description:  Rake file for building documentation from markdown and source
#               source, using pandoc and doxygen
#
# Author:       Igor Siemienowicz
#
# Copyright:    The content of this file is copyright (c) 2014 by the named
#               author(s), and licensed under terms of the Gnu Lesser General
#               Public License (LGPL). See the file `lgpl-3.0.txt` in the
#               `doc/licenses` directory for the full text of this license.

# --- CONFIGURATION ---

# The top-level documentation source directory. Most of the .md files are
# in here somewhere.
DOC_SRC_DIR = "doc"

# The top-level built documentation directory - all compiled docs go under
# here.
DOC_BUILD_DIR = BUILD_DIR + "/doc"

# --- TASKS ---

# Make sure the documentation build directory exists
directory DOC_BUILD_DIR

# -- The SCryFE Manifesto --

# Build the SCryFE manifesto
file "#{DOC_BUILD_DIR}/SCryFE-Manifesto.pdf" => [
        "#{DOC_SRC_DIR}/SCryFE-Manifesto.md",
        DOC_BUILD_DIR] do
    sh "pandoc #{DOC_SRC_DIR}/SCryFE-Manifesto.md " +
        "-o #{DOC_BUILD_DIR}/SCryFE-Manifesto.pdf"
end

# Put the manifesto in the deliverables area
file "#{DELIVERABLES_DIR}/SCryFE-Manifesto.pdf" =>
        [DELIVERABLES_DIR, "#{DOC_BUILD_DIR}/SCryFE-Manifesto.pdf"] do
    FileUtils.cp("#{DOC_BUILD_DIR}/SCryFE-Manifesto.pdf",
        "#{DELIVERABLES_DIR}/SCryFE-Manifesto.pdf")
end

task :deliverables => "#{DELIVERABLES_DIR}/SCryFE-Manifesto.pdf"

# -- Developer Documentation --

DEVELOPER_DOC_BUILD_DIR = DOC_BUILD_DIR + "/developer"
DEVELOPER_DOC_SRC_DIR = DOC_SRC_DIR + "/developer"

# - High-level SDK Manual -

# Just put the SDK manual in the developer doc area for the moment.
directory DEVELOPER_DOC_BUILD_DIR
file "#{DEVELOPER_DOC_BUILD_DIR}/SCryFE-Development-Kit-Manual.pdf" =>
        [DEVELOPER_DOC_BUILD_DIR,
        "#{DEVELOPER_DOC_SRC_DIR}/SCryFE-Development-Kit-Manual.md"] do
    sh "pandoc #{DEVELOPER_DOC_SRC_DIR}/" +
        "SCryFE-Development-Kit-Manual.md -o " +
        "#{DEVELOPER_DOC_BUILD_DIR}/SCryFE-Development-Kit-Manual.pdf"
end

# - Low-level (Doxygen) build -

LOW_LEVEL_DEVELOPER_DOC_BUILD_DIR = DEVELOPER_DOC_BUILD_DIR + "/low-level"
directory LOW_LEVEL_DEVELOPER_DOC_BUILD_DIR

task :doxygen_core => LOW_LEVEL_DEVELOPER_DOC_BUILD_DIR do
    sh "doxygen src/core/Doxyfile"
end

# - Algorithms -

# Algorithms documentation
#
# First, make sure there's a subdirectory for the algorithms docs.
ALGORITHMS_DOC_BUILD_DIR = DOC_BUILD_DIR + "/algorithms"
directory ALGORITHMS_DOC_BUILD_DIR

# Now create a rule for each algorithm documentation file.
algorithm_doc_targets = []
Dir.glob("doc/algorithms/*.md").each do |filename|
    basename = File.basename(filename, ".md")
    target = "#{ALGORITHMS_DOC_BUILD_DIR}/#{basename}.pdf"
    file target => [ALGORITHMS_DOC_BUILD_DIR, filename] do
        sh "pandoc #{filename} -o #{target}"
    end
    
    algorithm_doc_targets << target
end

# -- User documentation --

# - CLI -
USER_DOC_SRC_DIR = DOC_SRC_DIR + "/user"
USER_DOC_BUILD_DIR = DOC_BUILD_DIR + "/user"

directory USER_DOC_BUILD_DIR

# - CLI man page -
file "#{USER_DOC_BUILD_DIR}/scryfe-cli.1" =>
        [USER_DOC_BUILD_DIR, "#{USER_DOC_SRC_DIR}/scryfe-cli.md"] do
    sh "pandoc -s -t man #{USER_DOC_SRC_DIR}/scryfe-cli.md " +
        "-o #{USER_DOC_BUILD_DIR}/scryfe-cli.1"
end

# - CLI users manual (PDF) -
file "#{USER_DOC_BUILD_DIR}/scryfe-cli-Users-Manual.pdf" =>
        [USER_DOC_BUILD_DIR, "#{USER_DOC_SRC_DIR}/scryfe-cli.md"] do
    sh "pandoc #{USER_DOC_SRC_DIR}/scryfe-cli.md " +
        "-o #{USER_DOC_BUILD_DIR}/scryfe-cli-Users-Manual.pdf"
end

# - Main User Manual -
user_manual_source_filenames = 
    Dir.glob(USER_DOC_SRC_DIR + "/user-manual/*.png")
user_manual_source_filenames << 
    (USER_DOC_SRC_DIR + "/user-manual/SCryFE-User-Manual.md")

file "#{USER_DOC_BUILD_DIR}/SCryFE-User-Manual.pdf" => 
        user_manual_source_filenames do
    sh "pandoc #{USER_DOC_SRC_DIR}/user-manual/SCryFE-User-Manual.md " +
        "-o #{USER_DOC_BUILD_DIR}/SCryFE-User-Manual.pdf"
end

# - Release notes -
file "#{DOC_BUILD_DIR}/release-notes.txt" =>
        ["#{DOC_SRC_DIR}/release-notes.txt", DOC_BUILD_DIR] do
    # Copy the release notes line by line, and substitute the version
    # information.
    output = File.open("#{DOC_BUILD_DIR}/release-notes.txt", "w")
    File.open("#{DOC_SRC_DIR}/release-notes.txt", "r").each do |line|
        if line.include? "{version}"
            output.puts "**Version #{$SCRYFE_VERSION}**"
        else
            output.puts line
        end
    end
    
    output.close
    puts "release notes built"
end

# -- Namespace Management --

namespace :docs do
    desc "build the manifesto document"
    task :manifesto => "#{DOC_BUILD_DIR}/SCryFE-Manifesto.pdf"
    
    desc "build the algorithms documentation"
    task :algorithms => algorithm_doc_targets

    namespace :user do
        namespace :cli do
            desc "build the CLI man page"
            task :man_page => "#{USER_DOC_BUILD_DIR}/scryfe-cli.1"
            
            desc "build the CLI user manual"
            task :manual =>
                "#{USER_DOC_BUILD_DIR}/scryfe-cli-Users-Manual.pdf"
                
            task :all => [:man_page, :manual]
        end # namespace cli
        
        desc "build all CLI documentation"
        task :cli => "cli:all"
        
        desc "build the main user manual"
        task :manual => "#{USER_DOC_BUILD_DIR}/SCryFE-User-Manual.pdf"
        
        task :all => [:cli, :manual]
    end # namespace user
    
    desc "build all user documentation"
    task :user => "user:all"
    
    namespace :developer do
        desc "build the low-level developer documentation"
        task :low_level => :doxygen_core
        
        desc "build the high-level developer documentation"
        task :high_level => "#{DEVELOPER_DOC_BUILD_DIR}" +     
            "/SCryFE-Development-Kit-Manual.pdf"
        
        task :all => [:low_level, :high_level]
    end # namespace developer
    
    desc "build the developer documentation"
    task :developer => "developer:all"
    
    desc "assemble the release notes"
    task :release_notes => "#{DOC_BUILD_DIR}/release-notes.txt"
    
    task :all => [:manifesto, :algorithms, :user, :developer, :release_notes]
end

desc "build all documentation"
task :docs => [DOC_BUILD_DIR, "docs:all"]

task :deliverables => "docs:manifesto"

# -- Website --

WEBSIDE_SRC_DIR = USER_DOC_SRC_DIR + "/website"
WEBSITE_DELIVERABLE_DIR = DELIVERABLES_DIR + "/website"
directory WEBSITE_DELIVERABLE_DIR

desc "build the SCryFE website"
task :website => [
        WEBSITE_DELIVERABLE_DIR,
        "#{USER_DOC_BUILD_DIR}/scryfe-cli-Users-Manual.pdf",
        "#{USER_DOC_BUILD_DIR}/SCryFE-User-Manual.pdf"] do

    sh "jekyll build --source #{WEBSIDE_SRC_DIR} " +
        "--destination #{WEBSITE_DELIVERABLE_DIR}"

    FileUtils.cp(
        "#{USER_DOC_BUILD_DIR}/scryfe-cli-Users-Manual.pdf",
        "#{WEBSITE_DELIVERABLE_DIR}/assets")

    FileUtils.cp(
        "#{USER_DOC_BUILD_DIR}/SCryFE-User-Manual.pdf",
        "#{WEBSITE_DELIVERABLE_DIR}/assets")
end

# -- Top-level tasks --
# Note that we are leaving the website task OUT of the 'all' task for now.
task :all => :docs
