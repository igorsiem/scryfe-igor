`design` Directory
==================
All design documentation for *SCryFE* goes here. Diagrams are drawn using the
free [Dia](http://dia-installer.de/) tool.

*   `ByteArray.dia` - A detailed look at the `ByteArray` class

*   `Hash.dia` - A few details about the `Hash` family of classes

*   `scryfe_design.dia` - High-level design diagram of the *SCryFE*
    class architecture; not much detail, but a good way to get a structural
    overview
    