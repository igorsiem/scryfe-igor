*SCryFE* Development Kit Readme
===============================
This is the *SCryFE* Development Kit (SDK). The SDK allows programmers to
include the cryptographic functionality of *SCryFE* in other software.

For more details, consult the `release-notes.txt` file, and the SDK Manual
(located in the `doc` subdirectory) as well as:

*   [The *SCryFE* website](http://scryfe.com)

*   [The *SCryFE* blog](http://scryfe.tumblr.com)

*   [The *SCryFE* twitter account](http://twitter.com/scryfeproject)

The *SCryFE* team can be contacted via e-mail at scyfe@scryfe.com.
