---
title: SCryFE Development Kit (SDK) Manual
author: Igor Siemienowicz
date: August 2014
---

# Introduction
This is the Manual for the *SCryFE* Development Kit (SDK). The SDK allows programmers to include the cryptographic functionality of *SCryFE* in other software. This document is a high-level explanation of the SDK and its structure, along with code examples. Low-level class-by-class documentation is provided in browseable HTML.

The current version of the SDK makes the following functionality available in its Application Programming Interface (API):

*   SHA-256 and SHA-512 cryptographic hash functions
*   AES-256 symmetric block cipher
*   Output Feedback (OFB) block mode
*   In-place whole-file encryption
*   Secure handling of byte arrays

Enhancements planned for future releases include:

*   Additional symmetric and asymmetric ciphers
*   Additional cryptographic hash functions
*   Cryptographic-quality pseudorandom number generation
*   Key-management services
*   Interfaces and bindings for other languages

*SCryFE* development is primarily driven by its role as the provider of cryptographic functionality for the consumer toolset. It is also motivated by a desire to provide a code library that can be used with other software to include cryptographic functionality that is compatible with *SCryFE*.

# Licensing
This document is copyright (C)  2014 Igor Siemienowicz.

Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.3 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. A copy of the license is included in the section entitled "GNU Free Documentation License".

The *SCryFE* Development Kit (SDK) is copyright (C) 2014 Igor Siemienowicz, and licensed under the GNU Lesser General Public License (LGPL) 3.0. The full text of this license is contained the file named `lgpl-3.0.txt`, in the `license` subdirectory of this distribution.

# Getting Started

## Adding the SDK to Your Project
The first thing to do to use the SDK is to get it integrated into your build system. There are various distributions of the library available in precompiled form for toolsets like Microsoft Visual Studio, but you may prefer to include the source code directly into your project. This should work perfectly well for most compilers.

The *SCryFE* Development Kit has the following basic directory structure:

    scryfe
      |
      +-core
      |   |
      |   +-<scryfe core headers and source files>
      |
      +-doc
      |   |
      |   +-low-level
      |   |   |
      |   |   +-<detailed class documentation>
      |   |
      |   +-<user manual and release notes>
      |
      +-include
      |   |
      |   +-scryfe
      |       |
      |       +-scryfe.h
      |
      +-licenses
      |
      +-third-party
          |
          +-<third party code>

The single `scryfe.h` header contains all the inclusions needed to use the SDK. If this directory structure is retained, a project need only:

*   Add the `scryfe/include` directory to the include path
*   Ensure that the `.cpp` and `.c` files under the `core` and `third-party` directories are linked
*   Add the line

    `#include <scryfe/scryfe.h>`

    ... to the beginning of any code or header file that uses *SCryFE* functionality

## A Simple Example
The following code example shows how to perform a simple SHA-256 hash on an ordinary string. An brief overview of each of the classes used can be found in the [*Library Structure*](#library-structure) section.

    #include <iostream>
    #include <scryfe/scryfe.h>

    int main(void)
    {
        SCRYFE::ByteArray input("abc", 3), output;
        SCRYFE::SHA256Hash hash;

        hash.process(input, output);
        std::cout << hash.to_hex_string() << std::endl;
    }

Depending on what development platform is being used, and whether or not the *SCryFE* source code is being linked, a variety of issues may arise, but in general, if things are working as expected, some things to check are:

*   That the `scryfe/include` directory is part of your include path
*   That either
    -   The pre-built *SCryFE* library is being linked into the project / make file
    -   The *SCryFE* core library source code is included in the project  / make file

More examples are provided in the [*Example Usage*](#example-usage) section.

## A Note on Namespaces
All SCK declarations are made inside a single namespace called `SCRYFE`. This document sometimes omits this for brevity, where it is obvious that a *SCryFE* name is being used.

# Library Structure
This section discusses the basic structure and relationships of the various classes and functions in *SCryFE*. Fordetails of every class in the SDK, consult the low-level documentation, located in the `/doc/low-level` subdirectory.

## The `Cipher` Class Hierarchy
`SCRYFE::Cipher` is a base-class for a all classes that perform classic encryption functions in *SCryFE*. It is (or will be) the root of an inheritance hierarchy that includes symmetric, asymmetric, block and stream ciphers. In this version of the SDK, `Cipher` is the parent of a number of abstract classes, and exactly one concrete class - `SCRYFE::AES256Cipher`.

![The `Cipher` class hierarchy](doc/developer/Cipher-class-hierarchy.png)

The essential function of cipher objects is to process sequences of bytes (encapsulated by [the `SCRYFE::ByteArray` class](#the-bytearray-class), either encrypting them, or decrypting them. For this purpose, the `Cipher` base class declares the `process` method, which takes references to `ByteArray` objects for input and output.

Most cipher implementations require a certain amount of working memory for maintaining their internal state. In theory, being able to examine this memory can constitute a security vulnerability, so `Cipher` declares a `clear` method for securely destroying such internal buffers. The memory is overwritten before being released to the heap. Note that any session initialisation that has been performed on the cipher previously will be lost by calling this method. If the particular derived cipher class being used requires specific initialisation (e.g., the `SCRYFE::AES256Cipher` class), this will have to be executed again before the cipher object can be reused. Note also that `clear` is called by the `Cipher` class destructor.

The `SymmetricCipher` and `BlockCipher` classes are also abstract, and function mainly as a 'placeholders' in the hierarchy, in anticipation of further development. `SymmetricCipher` is the base class for all symmetric cipher algorithms, as opposed to asymmetric ciphers used in public key cryptography.

The `BlockCipher` class is a base class for ciphers that work on blocks of bytes, rather than the individual bytes in a stream (stream ciphers). Block ciphers are generally used with `BlockMode` objects (see the section entitled [*The `BlockMode` Class Hierarchy*](#the-blockmode-class-hierarchy) for more details) to securely transform amounts of data larger than a single block.[^asymmetric-block-ciphers] This class introduces the (pure virtual) `block_size` and `initialise` methods. The `block_size` method simply returns the size (in bytes) of the data blocks on which the cipher works, which is generally fixed for a given algorithm, while the `initialise` method is used to set up the cipher object with a given key for either encryption or decryption. This method needs to be called after the cipher object is instantiated --- and after `clear` has been called --- before it is used to process data.

The `AES256Cipher` is a concrete cipher class that can actually be instantiated to encrypt blocks of data using the [NIST Advanced Encryption Standard (AES-256)](http://en.wikipedia.org/wiki/Advanced_Encryption_Standard) algorithm (originally known as 'Rijndael'). This cipher uses a 256-bit key, and encrypts 128-bit blocks. In general, its usage pattern is as follows:

1.  Instantiate the cipher object, as in the following example:

        SCRYFE::AES256Cipher cipher;

2.  Initialise the cipher with a 256-bit (32-byte) key, and an enumerator indicating whether the processing will be encryption or decryption:

        // 'key' is a ByteArray object containing 32 bytes
        cipher.initialise(key, SCRYFE::encrypt);
        // use SCRYFE::decrypt for decryption

    Note that the key should generally be a hashed version of a long keyphrase. The `Hash` class hierarchy can be used to produce key hashes. It is generally a good idea to call the `ByteArray::clear` method for the key object after initialisation, to minimise the amount of time that the key spends in memory.

3.  Invoke the `process` method repeatedly to perform encryption or decryption on 128-bit blocks of data.

        // 'input' and 'output' are ByteArray objects
        // 'input' contains 16 bytes of data
        cipher.process(input, output);

4.  Call `clear` for the cipher object, to clear its internal state (or delete the object, or allow it to go out of scope).

        cipher.clear();

These steps are given simply to illustrate how `Cipher` objects work. It is worth repeating that, when encrypting large amounts of data (i.e., larger than a single block), a block cipher should be used with a *block cipher mode*, rather than in the 'standalone' manner outlined in the example code above. Simply encrypting successive blocks of data in sequence, using a block cipher, is *not* secure. This is explained in more detail in the section entitled [*The `BlockMode` Class Hierarchy*](#the-blockmode-class-hierarchy).

Future iterations of the SDK will include more cipher options.

[^asymmetric-block-ciphers]: Obviously, asymmetric ciphers are also - generally - block ciphers as well, which means that deriving the `BlockCipher` class from `SymmetricCipher` may be inappropriate. This is somewhat academic in the current implementation, as the only instantiable class in the hierarchy is `AES256Cipher`. Future iterations of the SDK will resolve this in a consistent manner.

## The `Hash` Class Hierarchy
The *SCryFE* `Hash` class hierarchy encapsulates crypotgraphic hash functions.[^wikipedia-on-hashes] The current iteration of the SDK includes only the `SCRYFE::Hash` abstract base class, and the concrete `SHA256Hash` class derived from it.

`Hash` declares a simple hashing interface, with two important methods:

*   `output_size`, which returns the output size of the hashing function in bytes
*   `process`, which performs the actual hashing algorithm, taking `input` and `output` parameters of type `ByteArray` (see [*The `ByteArray` Class*](#the-bytearray-class) below)

The `SHA256Hash` class implements the `Hash` interface with the NIST Secure Hash Algorithm (SHA-2, 256 bits). In this version of the SDK, hashing is mainly used to process encryption keyphrases, and also to 'condition' sources of external entropy (such as keyboard press timings). Using it is very simple:

    // 'input' is a byte array of arbitrary length
    SCRYFE::ByteArray output;
    SCRYFE::SHA256Hash hash;
    hash.process(input, output)

    // 'output' now contains a 32-byte hash of the input.

For other examples using the `SHA256Hash` class, consult the [*Example Usage*](#example-usage) section. Future iterations of the SDK will include other hashing options.

[^wikipedia-on-hashes]: For a a reasonable introduction to cryptographic hashing, consult [the Wikipedia article on the subject](http://en.wikipedia.org/wiki/Cryptographic_hash_function)

## The `BlockMode` Class Hierarchy
From the perspective of security, when using a block cipher, it is not sufficient to simply use the cipher to encrypt successive blocks of plaintext. Even after encrypting in this fashion, essential elements of data structure across multiple blocks can be preserved. [The Wikipedia article on block cipher modes of operation](http://en.m.wikipedia.org/wiki/Block_cipher_mode) gives a good explanation of why this is a problem, and how block cipher modes work to correct this.

`SCRYFE::BlockMode` objects implement block cipher modes that can be used with any symmetric block cipher object. This version of the SDK only implements a single block cipher mode: Output Feedback (OFB).[^ofb-stream-mode] Future iterations will implement other modes.

All block mode objects are constructed with a reference to a `SCRYFE::BlockCipher` object (such as an instance of `AES256Cipher`). This is the cipher object that will do the actual encryption of each block. The cipher object may be in an initialised or uninitialised state --- the block mode object is responsible for (re)initialising it as necessary. Similarly, block mode objects have `process` and `clear` methods  that perform similar functions to their counterparts in the `Cipher` class hierarchy, and call these methods on the associated `BlockCipher` object. For example, calling the block mode `clear` method, also securely clears the cipher object that was passed to the block mode object's constructor.

Block mode objects also include a `block_size` method. This is simply a convenience, returning the block size of the underlying cipher object.

The `OFBBlockMode` class implements output feedback (OFB) block cipher mode with whatever block cipher is passed to it. As well as the `process` and `clear` methods discussed above, `OFBBlockMode` has an `initialise` method that takes the following arguments:

*   `iv` --- a `ByteArray` initialisation vector (IV), discussed below
*   `key` --- a `ByteArray` encryption key (passed to the cipher object)
*   `m` --- the `cipher_mode`, `SCRYFE::encrypt` or `SCRYFE::decrypt`

The `initialise` method performs all necessary initialisation functions for the block cipher mode, including calling the corresponding `initialise` method of the cipher object.

The *initialisation vector* is a special requirement of most block cipher modes. It provides initial randomisation for the encryption operation. Initialisation vectors:

*   Should be truly random (i.e., generated from some true source of entropy)
*   Need not be kept secret --- they are not part of the encryption key, and may be freely passed with the ciphertext
*   Should not be reused; each encryption session should use a fresh IV
*   Need to be the same length as the cipher block size

The best way to generate good initialisation vectors is by implementing some form of entropy generation mechanism. There is some debate about the best way to do this for cryptographic purposes, but a detailed tutorial on using keystrokes and timings as a source of entropy can be found at [eTutorials.org](http://etutorials.org) in the section in the section entitled [*11.20 Gathering Entropy from the Keyboard*](http://etutorials.org/Programming/secure+programming/Chapter+11.+Random+Numbers/11.20+Gathering+Entropy+from+the+Keyboard/). The *SCryFE* consumer software uses a modified form of this method.

A simple encryption example using the `OFBBlockMode` class is as follows:

    // 'plaintext' is the array of char to encrypt
    // 'ciphertext' is an array of char for the encrypted output
    // 'size' is the size of the plaintext and ciphertext arrays in bytes
    // 'key' is the 32-byte encryption key
    // 'iv' is the 16-byte initialisation vector

    // Instantiate and initialise the cipher and block mode objects
    SCRYFE::AES256Cipher cipher;
    SCRYFE::OFBBlockMode block_mode(cipher);
    block_mode.initialise(iv, key, SCRYFE::encrypt);

    // Loop through the plaintext in blocks
    // (AES-256 block size is 16 bytes)
    std::size_t offset = 0;
    while (offset < size)
    {
        SCRYFE::ByteArray input;

        // Check to see if the number of remaining bytes is less than the block size
        std::size_t n = block_mode.block_size();
        if (size-offset < block_mode.block_size()) n = size-offset);

        // Process the plaintext into ciphertext
        // note that with OFB, the input size may be *smaller* than the block size
        input.copy_from(plaintext+offset, n);
        block_mode.process(input, output);
        memcpy(ciphertext+offset, output.bytes(), n);

        offset += block_mode.block_size();
    }

The decryption process is almost identical, substituting the `SCRYFE::decrypt` enumerator in the call to `SCRYFE::BlockMode::initialise`. The same initialisation vector as that given for encryption must be used. Remember that the IV need not be kept secret,[^secret-iv-does-not-add-to-security] and may be passed publically with the ciphertext. See the section entitled [*Example Usage*](#example-usage) for more examples.

[^ofb-stream-mode]: Note that the OFB block mode has the added advantage of effectively turning a block cipher into a stream cipher, since it does not require padding for incomplete blocks.

[^secret-iv-does-not-add-to-security]: Keeping the initialisation vector secret does not add to the security of the algorithm.

## The `FileEncrypter` Class
`SCRYFE::FileEncrypter` is a utility class for encrypting files using the `Cipher`, `BlockMode` and associated classes. It provides functionality for encrypting and decrypting single files with a single method call, taking care of the details of instantiating and initialising the `Cipher` and `BlockMode` objects as part of the process.

The class has two static methods: `encrypt` and `decrypt`. These may be used without instatiating the class itself. The `encrypt` method takes the following arguments:

*   `filename` - the path and filename of the file to be encrypted
*   `c` - an enumerator denoting the type of cipher to use; this version of the SDK only supports a single cipher: AES-256, denoted by `SCRYFE::FileEncrypter::aes256`
*   `bm` - an enumerator denoting the type of block cipher mode to use; this version of the SDK only include output feedback mode, denoted by `SCRYFE::FileEncrypter::ofb`
*   `iv` - the initialisation vector for encrypting; this is a `ByteArray` object that must contain 16 bytes
*   `key` - the encryption key; this is a `ByteArray` object containing 32 bytes
*   `progress` - an optional pointer to a `ProgressMonitor` object (see below)

The `decrypt` static method has the same arguments, except for `iv`. The initialisation vector for the block mode is actually appended to the encrypted file (changing the file size slightly). The decryption operation retrieves this, and removes it at the end of its operation.

***Important:*** *Both encryption and decryption operate on the actual file, not a temporary copy, and the file is* ***not*** *backed up beforehand. This deliberate, to avoid leaving an unencrypted copy of the plaintext, but also carries the associated danger of irretrievable file corruption. Files should be manually backed up to a secure location before being encrypted.*

Encryption and decryption can be lengthy operations on large files. `ProgressMonitor` is an abstract nested subclass of `SCRYFE::FileEncrypter` that declares an interface for monitoring the progress of these operations, and reporting it to the user. External code that calls the `encrypt` or `decrypt` methods can implement a class derived from `ProgressMonitor`, and pass a pointer of an instance of this derived class to the method call.

`ProgressMonitor` has only one significant method: `signal_progress` (declared pure virtual). This is a callback method that is invoked periodically during the progress of the encryption or decryption operation to report how far the task has progressed. It passes two argument: `number_of_bytes_processed` and `total_number_of_bytes`, being how many bytes of the file have been encrypted, and the total file length in bytes, respectively.

Using `ProgressMonitor can be as simple as the following:

    #include <iostream>
    #include <scryfe/scryfe.h>

    // Progress monitoring class
    class MyProgressMonitor : public SCRYFE::FileEncrypter::ProgressMonitor
    {
        virtual void signal_progress(
                int64 number_of_bytes_processed,
                int64 total_number_of_bytes)
        {
            std::cout << "processed " << number_of_bytes_processed <<
                " bytes of " << total_number_of_bytes_processed <<
                " bytes total" << std::endl;
        }

        // Virtual destructor for consistency
        virtual ~MyProgressMonitor(void) {}
    };

Executing a file encryption operation can thus be as simple as:

    // 'filename' is the name of a file
    // 'iv' is the initialisation vector (16 bytes)
    // 'key' is the encryption key (32 bytes)
    MyProgressMonitor pm;
    SCRYFE::FileEncrypter::encrypt(
        filename,
        SCRYFE::FileEncrypter::aes256,
        SCRYFE::FileEncrypter::ofb,
        iv,
        key,
        &pm);
    
Decryption works similarly:

    // Same variables as previous examples (except 'iv')
    SCRYFE::FileEncrypter::decrypt(
        filename,
        SCRYFE::FileEncrypter::aes256,
        SCRYFE::FileEncrypter::ofb,
        key,
        &pm);

Some additional things to note about file operations:

*   Using a `ProgressMonitor` obhject is optional. If the pointer parameter is set to `NULL`, the operation will proceed normally.
*   Progress monitoring using a callback is often used to update a progress bar or something similar in a GUI setting. This is what `ProgressMonitor` is mainly intended for, but developers should be aware that the callbacks are made from the encrypting / decrypting thread (i.e., the thread from which `encrypt` or `decrypt` was called). If the `signal_progress` method is being used to modify resources that are used by other threads, then external thread-safety / synchronisation mechanisms (e.g. mutexes or critical sections) should be used.
*   There is (currently) no way to abort a file encryption or decryption operation, other than to throw an exception from the `ProgressMonitor::signal_progress` method. The reason for this is that the doing so would almost certainly leave the file in an unknown and corrupted state, and so a catastrophic error condition is really the only circumstance under which the operation should be stopped prematurely.

[^remember-iv-not-secret]: Recall that block mode IVs are not kept secret. See the section on [*The `BlockMode` Class Hierarchy*](#the-blockmode-class-hieararchy) for more details.

## Other Classes and Types
The SDK includes a number of basic enumerations, classes and other declarations that are used across the *SCryFE* library.

### The `cipher_mode` Enumeration
This is a simple enumerator for the mode of operation in which a cipher can operate. Valid values are `encrypt`, `decrypt` and `none`.

### The `ByteArray` Class
`SCRYFE::ByteArray` is a utility class for managing an array of bytes with a fixed size. It is intended for general cryptographic use.

Specifically, it is used for handling secret data, such as keys and plaintext. In support of this, it takes the following precautions:

*   Internal memory buffers are prevented from being cached to disk (using system-specific API calls)
*   Internal memory buffers are explicitly overwritten with zeros prior to being deallocated

The class includes the following public methods:

*   `size` - returns the size of the array buffer in bytes
*   Individual byte retrieval using `operator[]` and the `at` method
*   `copy_from` - copying from a raw memory buffer
*   Translating to and from hexadecimal strings
*   `clear` - securely clears and deallocates the buffer
*   `set_to_zero` - sets all bytes in the array to zero; this method is overloaded, so that if it isused with a `size` argument, it explicitly clears the existing array, and resizes it if necessary
*   Const and non-const access to the raw byte array; obviously this access should be used judiciously
*   Bitwise XOR operations (which are commonly used in crypto applications)
*   Standard copy and equality comparison semantics

Consult the low-level documentation for this class for more details.

### Signalling Errors
All error conditions in*SCryFE* are signalled using exceptions. In this version of the SDK, there is a single *SCryFE*-specific exception class called `SCRYFE::Error`, which is derived from `std::exception`. `Error`is a very simple class that implements the `what` method of `std::exception` to return a human-readable error message.

Consult the low-level document for the `SCRYFE::Error` class for more details.

# A Note on Initialisation Vectors and Entropy
The example code given in the following sections does not include functionality for gathering random data for use as an initialisation vector. There are a variety of methods for doing this, with varying opinions on their suitability and security.

The *SCryFE* consumer software asks the user to type at random at the keyboard for a short time, and uses the values of the keys typed *and* the timing of the keypresses. This is a modified form of the method outlined in [eTutorials.org](http://eTutorials.org), in the section entitled [*11.20 Gathering Entropy from the Keyboard*](http://etutorials.org/Programming/secure+programming/Chapter+11.+Random+Numbers/11.20+Gathering+Entropy+from+the+Keyboard/). The source code for the *SCryFE* CLI and GUI tools may be examined to see how this is done from a terminal, and from a window.[^scryfe-consumer-software-source-code-checkout]

In any case, the sample code given below uses a simple 16-byte IV containing arbitrary bytes. This would *not* be suitable for a production-level application.

[^scryfe-consumer-software-source-code-checkout]: The source code for the *SCryFE* consumer software may be cloned using `git` from `https://bitbucket.org/scryfe/scryfe.git`.

# Example Code: Encrypting a Text String or Stream of Bytes
This example shows how a text string of arbitrary length can be encrypted and decrypted using the AES-256 cipher and output feedback mode. This could potentially be used used in message encryption of various kinds, although such applications generally use a Public Key Infrastructure (PKI) as well as a block cipher.[^future-pki]

This sample assume that the SDK has been set up and works as described in the [*Getting Started*](#getting-started) section.


````
// Simple program to encrypt, and then decrypt a string.
// This can easily be adapted to an arbitrary array of bytes.

#include <stdexcept>
#include <iostream>

#include <scryfe/scryfe.h>

int main(void)
{
    int retcode = 0
    try
    {
        // This is the string to be encrypted.
        std::string plaintext = "this is the plaintext";

        // This is the encryption keyphrase.
        // Note that it will be hashed before being used as a key.
        std::string keyphrase = "hello123";

        // Create the key by hashing the keyphrase.
        // Note the conversion of the string to a byte array for use in hashing.
        SCRYFE::ByteArray keyphrase_b(keyphrase.c_str(), keyphrase.size()), key;
        SCRYFE::SHA256Hash hash;
        hash.process(keyphrase_b, key);

        // The key contains a 32-byte hash of the keyphrase.

        // Create an arbitrary IV; as discussed, this is NOT suitable for a
        // 'real world' situation.
        // IVs need to be gathered from real random data.
        SCRYFE::ByteArray iv;
        iv.copy_from_hex("0102030405060708090a0b0c0d0e0f");

        // Put the plaintext into a byte array for encryption too.
        SCRYFE::ByteArray plaintext_b(plaintext.c_str(), plaintext.size()),
            ciphertext_b;

        // Create the cipher and block mode objects.
        SCRYFE::AES256Cipher cipher;
        SCRYFE::OFBBlockMode ofb(cipher);

        // Initialise the block mode (this initialises the cipher as well).
        ofb.initialise(iv, key, SCRYFE::encrypt);

        // Encrypt.
        ofb.process(plaintext_b, ciphertext_b);

        // At this point, 'ciphertext_b' contains the encrypted version of
        // the plaintext.

        // Reinitialise the block mode and cipher objects for decryption.
        ofb.initialise(iv, key, SCRYFE::decrypt);

        // We can now decrypt.
        SCRYFE::ByteArray decrypted_b;
        ofb.process(ciphertext_b, decrypted_b);

        // At this point, decrypted_b == plaintext_b
        // Both contain the plaintext string bytes, without a string-style
        // zero-terminator
        for (std::size_t i = 0; i < decrypted_b.size(); i++)
            std::cout << decrypted_b[i];

        std::cout << std::endl;
    }
    catch (const SCRYFE::Error& scryfe_error)
    {
        std::cerr << "SCRYFE ERROR: " << scryfe_error.what() << std::endl;
        retcode = -1;
    }
    catch (const std::exception& error)
    {
        std::cerr << "ERROR: " << error.what() << std::endl;
        retcode = -1;
    }
    catch (...)
    {
        std::cerr << "UNRECOGNISED ERROR - contact support" << std::endl;
        retcode = -1;
    }

    return retcode;
}
````

Running this code should result in the line:

    this is the plaintext

[^future-pki]: Support for Public Key Infrastructures is planned for future versions of *SCryFE*.

# GNU Free Documentation License
Version 1.3, 3 November 2008

Copyright © 2000, 2001, 2002, 2007, 2008 Free Software Foundation, Inc. [http://fsf.org/](http://fsf.org/)

Everyone is permitted to copy and distribute verbatim copies of this license document, but changing it is not allowed.

## 0. PREAMBLE
The purpose of this License is to make a manual, textbook, or other functional and useful document "free" in the sense of freedom: to assure everyone the effective freedom to copy and redistribute it, with or without modifying it, either commercially or noncommercially. Secondarily, this License preserves for the author and publisher a way to get credit for their work, while not being considered responsible for modifications made by others.

This License is a kind of "copyleft", which means that derivative works of the document must themselves be free in the same sense. It complements the GNU General Public License, which is a copyleft license designed for free software.

We have designed this License in order to use it for manuals for free software, because free software needs free documentation: a free program should come with manuals providing the same freedoms that the software does. But this License is not limited to software manuals; it can be used for any textual work, regardless of subject matter or whether it is published as a printed book. We recommend this License principally for works whose purpose is instruction or reference.

## 1. APPLICABILITY AND DEFINITIONS
This License applies to any manual or other work, in any medium, that contains a notice placed by the copyright holder saying it can be distributed under the terms of this License. Such a notice grants a world-wide, royalty-free license, unlimited in duration, to use that work under the conditions stated herein. The "Document", below, refers to any such manual or work. Any member of the public is a licensee, and is addressed as "you". You accept the license if you copy, modify or distribute the work in a way requiring permission under copyright law.

A "Modified Version" of the Document means any work containing the Document or a portion of it, either copied verbatim, or with modifications and/or translated into another language.

A "Secondary Section" is a named appendix or a front-matter section of the Document that deals exclusively with the relationship of the publishers or authors of the Document to the Document's overall subject (or to related matters) and contains nothing that could fall directly within that overall subject. (Thus, if the Document is in part a textbook of mathematics, a Secondary Section may not explain any mathematics.) The relationship could be a matter of historical connection with the subject or with related matters, or of legal, commercial, philosophical, ethical or political position regarding them.

The "Invariant Sections" are certain Secondary Sections whose titles are designated, as being those of Invariant Sections, in the notice that says that the Document is released under this License. If a section does not fit the above definition of Secondary then it is not allowed to be designated as Invariant. The Document may contain zero Invariant Sections. If the Document does not identify any Invariant Sections then there are none.

The "Cover Texts" are certain short passages of text that are listed, as Front-Cover Texts or Back-Cover Texts, in the notice that says that the Document is released under this License. A Front-Cover Text may be at most 5 words, and a Back-Cover Text may be at most 25 words.

A "Transparent" copy of the Document means a machine-readable copy, represented in a format whose specification is available to the general public, that is suitable for revising the document straightforwardly with generic text editors or (for images composed of pixels) generic paint programs or (for drawings) some widely available drawing editor, and that is suitable for input to text formatters or for automatic translation to a variety of formats suitable for input to text formatters. A copy made in an otherwise Transparent file format whose markup, or absence of markup, has been arranged to thwart or discourage subsequent modification by readers is not Transparent. An image format is not Transparent if used for any substantial amount of text. A copy that is not "Transparent" is called "Opaque".

Examples of suitable formats for Transparent copies include plain ASCII without markup, Texinfo input format, LaTeX input format, SGML or XML using a publicly available DTD, and standard-conforming simple HTML, PostScript or PDF designed for human modification. Examples of transparent image formats include PNG, XCF and JPG. Opaque formats include proprietary formats that can be read and edited only by proprietary word processors, SGML or XML for which the DTD and/or processing tools are not generally available, and the machine-generated HTML, PostScript or PDF produced by some word processors for output purposes only.

The "Title Page" means, for a printed book, the title page itself, plus such following pages as are needed to hold, legibly, the material this License requires to appear in the title page. For works in formats which do not have any title page as such, "Title Page" means the text near the most prominent appearance of the work's title, preceding the beginning of the body of the text.

The "publisher" means any person or entity that distributes copies of the Document to the public.

A section "Entitled XYZ" means a named subunit of the Document whose title either is precisely XYZ or contains XYZ in parentheses following text that translates XYZ in another language. (Here XYZ stands for a specific section name mentioned below, such as "Acknowledgements", "Dedications", "Endorsements", or "History".) To "Preserve the Title" of such a section when you modify the Document means that it remains a section "Entitled XYZ" according to this definition.

The Document may include Warranty Disclaimers next to the notice which states that this License applies to the Document. These Warranty Disclaimers are considered to be included by reference in this License, but only as regards disclaiming warranties: any other implication that these Warranty Disclaimers may have is void and has no effect on the meaning of this License.

## 2. VERBATIM COPYING
You may copy and distribute the Document in any medium, either commercially or noncommercially, provided that this License, the copyright notices, and the license notice saying this License applies to the Document are reproduced in all copies, and that you add no other conditions whatsoever to those of this License. You may not use technical measures to obstruct or control the reading or further copying of the copies you make or distribute. However, you may accept compensation in exchange for copies. If you distribute a large enough number of copies you must also follow the conditions in section 3.

You may also lend copies, under the same conditions stated above, and you may publicly display copies.

## 3. COPYING IN QUANTITY
If you publish printed copies (or copies in media that commonly have printed covers) of the Document, numbering more than 100, and the Document's license notice requires Cover Texts, you must enclose the copies in covers that carry, clearly and legibly, all these Cover Texts: Front-Cover Texts on the front cover, and Back-Cover Texts on the back cover. Both covers must also clearly and legibly identify you as the publisher of these copies. The front cover must present the full title with all words of the title equally prominent and visible. You may add other material on the covers in addition. Copying with changes limited to the covers, as long as they preserve the title of the Document and satisfy these conditions, can be treated as verbatim copying in other respects.

If the required texts for either cover are too voluminous to fit legibly, you should put the first ones listed (as many as fit reasonably) on the actual cover, and continue the rest onto adjacent pages.

If you publish or distribute Opaque copies of the Document numbering more than 100, you must either include a machine-readable Transparent copy along with each Opaque copy, or state in or with each Opaque copy a computer-network location from which the general network-using public has access to download using public-standard network protocols a complete Transparent copy of the Document, free of added material. If you use the latter option, you must take reasonably prudent steps, when you begin distribution of Opaque copies in quantity, to ensure that this Transparent copy will remain thus accessible at the stated location until at least one year after the last time you distribute an Opaque copy (directly or through your agents or retailers) of that edition to the public.

It is requested, but not required, that you contact the authors of the Document well before redistributing any large number of copies, to give them a chance to provide you with an updated version of the Document.

## 4. MODIFICATIONS
You may copy and distribute a Modified Version of the Document under the conditions of sections 2 and 3 above, provided that you release the Modified Version under precisely this License, with the Modified Version filling the role of the Document, thus licensing distribution and modification of the Modified Version to whoever possesses a copy of it. In addition, you must do these things in the Modified Version:

A.  Use in the Title Page (and on the covers, if any) a title distinct from that of the Document, and from those of previous versions (which should, if there were any, be listed in the History section of the Document). You may use the same title as a previous version if the original publisher of that version gives permission.

B.  List on the Title Page, as authors, one or more persons or entities responsible for authorship of the modifications in the Modified Version, together with at least five of the principal authors of the Document (all of its principal authors, if it has fewer than five), unless they release you from this requirement.

C.  State on the Title page the name of the publisher of the Modified Version, as the publisher.

D.  Preserve all the copyright notices of the Document.

E.  Add an appropriate copyright notice for your modifications adjacent to the other copyright notices.

F.  Include, immediately after the copyright notices, a license notice giving the public permission to use the Modified Version under the terms of this License, in the form shown in the Addendum below.

G.  Preserve in that license notice the full lists of Invariant Sections and required Cover Texts given in the Document's license notice.

H.  Include an unaltered copy of this License.

I.  Preserve the section Entitled "History", Preserve its Title, and add to it an item stating at least the title, year, new authors, and publisher of the Modified Version as given on the Title Page. If there is no section Entitled "History" in the Document, create one stating the title, year, authors, and publisher of the Document as given on its Title Page, then add an item describing the Modified Version as stated in the previous sentence.

J.  Preserve the network location, if any, given in the Document for public access to a Transparent copy of the Document, and likewise the network locations given in the Document for previous versions it was based on. These may be placed in the "History" section. You may omit a network location for a work that was published at least four years before the Document itself, or if the original publisher of the version it refers to gives permission.

K.  For any section Entitled "Acknowledgements" or "Dedications", Preserve the Title of the section, and preserve in the section all the substance and tone of each of the contributor acknowledgements and/or dedications given therein.

L.  Preserve all the Invariant Sections of the Document, unaltered in their text and in their titles. Section numbers or the equivalent are not considered part of the section titles.

M.  Delete any section Entitled "Endorsements". Such a section may not be included in the Modified Version.

N.  Do not retitle any existing section to be Entitled "Endorsements" or to conflict in title with any Invariant Section.

O.  Preserve any Warranty Disclaimers.

If the Modified Version includes new front-matter sections or appendices that qualify as Secondary Sections and contain no material copied from the Document, you may at your option designate some or all of these sections as invariant. To do this, add their titles to the list of Invariant Sections in the Modified Version's license notice. These titles must be distinct from any other section titles.

You may add a section Entitled "Endorsements", provided it contains nothing but endorsements of your Modified Version by various parties—for example, statements of peer review or that the text has been approved by an organization as the authoritative definition of a standard.

You may add a passage of up to five words as a Front-Cover Text, and a passage of up to 25 words as a Back-Cover Text, to the end of the list of Cover Texts in the Modified Version. Only one passage of Front-Cover Text and one of Back-Cover Text may be added by (or through arrangements made by) any one entity. If the Document already includes a cover text for the same cover, previously added by you or by arrangement made by the same entity you are acting on behalf of, you may not add another; but you may replace the old one, on explicit permission from the previous publisher that added the old one.

The author(s) and publisher(s) of the Document do not by this License give permission to use their names for publicity for or to assert or imply endorsement of any Modified Version.

## 5. COMBINING DOCUMENTS
You may combine the Document with other documents released under this License, under the terms defined in section 4 above for modified versions, provided that you include in the combination all of the Invariant Sections of all of the original documents, unmodified, and list them all as Invariant Sections of your combined work in its license notice, and that you preserve all their Warranty Disclaimers.

The combined work need only contain one copy of this License, and multiple identical Invariant Sections may be replaced with a single copy. If there are multiple Invariant Sections with the same name but different contents, make the title of each such section unique by adding at the end of it, in parentheses, the name of the original author or publisher of that section if known, or else a unique number. Make the same adjustment to the section titles in the list of Invariant Sections in the license notice of the combined work.

In the combination, you must combine any sections Entitled "History" in the various original documents, forming one section Entitled "History"; likewise combine any sections Entitled "Acknowledgements", and any sections Entitled "Dedications". You must delete all sections Entitled "Endorsements".

## 6. COLLECTIONS OF DOCUMENTS
You may make a collection consisting of the Document and other documents released under this License, and replace the individual copies of this License in the various documents with a single copy that is included in the collection, provided that you follow the rules of this License for verbatim copying of each of the documents in all other respects.

You may extract a single document from such a collection, and distribute it individually under this License, provided you insert a copy of this License into the extracted document, and follow this License in all other respects regarding verbatim copying of that document.

## 7. AGGREGATION WITH INDEPENDENT WORKS
A compilation of the Document or its derivatives with other separate and independent documents or works, in or on a volume of a storage or distribution medium, is called an "aggregate" if the copyright resulting from the compilation is not used to limit the legal rights of the compilation's users beyond what the individual works permit. When the Document is included in an aggregate, this License does not apply to the other works in the aggregate which are not themselves derivative works of the Document.

If the Cover Text requirement of section 3 is applicable to these copies of the Document, then if the Document is less than one half of the entire aggregate, the Document's Cover Texts may be placed on covers that bracket the Document within the aggregate, or the electronic equivalent of covers if the Document is in electronic form. Otherwise they must appear on printed covers that bracket the whole aggregate.

## 8. TRANSLATION
Translation is considered a kind of modification, so you may distribute translations of the Document under the terms of section 4. Replacing Invariant Sections with translations requires special permission from their copyright holders, but you may include translations of some or all Invariant Sections in addition to the original versions of these Invariant Sections. You may include a translation of this License, and all the license notices in the Document, and any Warranty Disclaimers, provided that you also include the original English version of this License and the original versions of those notices and disclaimers. In case of a disagreement between the translation and the original version of this License or a notice or disclaimer, the original version will prevail.

If a section in the Document is Entitled "Acknowledgements", "Dedications", or "History", the requirement (section 4) to Preserve its Title (section 1) will typically require changing the actual title.

## 9. TERMINATION
You may not copy, modify, sublicense, or distribute the Document except as expressly provided under this License. Any attempt otherwise to copy, modify, sublicense, or distribute it is void, and will automatically terminate your rights under this License.

However, if you cease all violation of this License, then your license from a particular copyright holder is reinstated (a) provisionally, unless and until the copyright holder explicitly and finally terminates your license, and (b) permanently, if the copyright holder fails to notify you of the violation by some reasonable means prior to 60 days after the cessation.

Moreover, your license from a particular copyright holder is reinstated permanently if the copyright holder notifies you of the violation by some reasonable means, this is the first time you have received notice of violation of this License (for any work) from that copyright holder, and you cure the violation prior to 30 days after your receipt of the notice.

Termination of your rights under this section does not terminate the licenses of parties who have received copies or rights from you under this License. If your rights have been terminated and not permanently reinstated, receipt of a copy of some or all of the same material does not give you any rights to use it.

## 10. FUTURE REVISIONS OF THIS LICENSE
The Free Software Foundation may publish new, revised versions of the GNU Free Documentation License from time to time. Such new versions will be similar in spirit to the present version, but may differ in detail to address new problems or concerns. See http://www.gnu.org/copyleft/.

Each version of the License is given a distinguishing version number. If the Document specifies that a particular numbered version of this License "or any later version" applies to it, you have the option of following the terms and conditions either of that specified version or of any later version that has been published (not as a draft) by the Free Software Foundation. If the Document does not specify a version number of this License, you may choose any version ever published (not as a draft) by the Free Software Foundation. If the Document specifies that a proxy can decide which future versions of this License can be used, that proxy's public statement of acceptance of a version permanently authorizes you to choose that version for the Document.

## 11. RELICENSING
"Massive Multiauthor Collaboration Site" (or "MMC Site") means any World Wide Web server that publishes copyrightable works and also provides prominent facilities for anybody to edit those works. A public wiki that anybody can edit is an example of such a server. A "Massive Multiauthor Collaboration" (or "MMC") contained in the site means any set of copyrightable works thus published on the MMC site.

"CC-BY-SA" means the Creative Commons Attribution-Share Alike 3.0 license published by Creative Commons Corporation, a not-for-profit corporation with a principal place of business in San Francisco, California, as well as future copyleft versions of that license published by that same organization.

"Incorporate" means to publish or republish a Document, in whole or in part, as part of another Document.

An MMC is "eligible for relicensing" if it is licensed under this License, and if all works that were first published under this License somewhere other than this MMC, and subsequently incorporated in whole or in part into the MMC, (1) had no cover texts or invariant sections, and (2) were thus incorporated prior to November 1, 2008.

The operator of an MMC Site may republish an MMC contained in the site under CC-BY-SA on the same site at any time before August 1, 2009, provided the MMC is eligible for relicensing.
