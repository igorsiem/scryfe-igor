`doc` Directory
===============
The `doc` directory is the top-level area for all *SCryFE* project
documentation. The default format for *SCryFE* documentation is Markdown.
Diagrams are drawn using the free [Dia](http://dia-installer.de/) tool.

Index
-----

### Directories
*   `algorithms` - Higher-level technical explanations on how certain things
    are done in *SCryFE*
    
*   `assets` - General graphics and logos for *SCryFE* documentation, and
    evangelism material

*   `design` - UML design diagrams to explain class relationships.

*   `developer` - Documentation intended for developers (including users
    of the SDK)

*   `licenses` - The text of all licenses used by *SCryFE* components need to
    be stored here and indexed. This is very important for ensuring that
    *SCryFE* remains free.
    
*   `user` - User documentation for *SCryFE* consumer products

### Files
*   `doc.rake` - The rakefile for building all documentation

*   `readme.txt` - This readme file

*   `release-notes.txt` - Notes distributed with each release

*   `SCryFE-Manifesto.md` - The overall, high-level, top-shelf archbishop and
    grand poobaa of *SCryFE* documentation; everything about everything is
    written down here
