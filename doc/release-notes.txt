SCryFE Release Notes
====================

*"Strong Crypto For Everyone"*

**Version {version}**

Welcome to *SCryFE*, the project for creating strong cryptographic
functionality that anyone can use. You can find out more about *SCryFE* at
the following locations:

*   [The *SCryFE* website](http://scryfe.com)

*   [The *SCryFE* blog](http://scryfe.tumblr.com)

*   [The *SCryFE* twitter account](http://twitter.com/scryfeproject)

This Release of *SCryFE*
------------------------
This is an initial 'put out something out there that actually does something'
release. There are two basic products:

*   Consumer software that allows you to encrypt and decrypt files; in CLI
    and GUI form

*   An SDK providing the the following cryptographic functionality;

    -   AES-256 encryption
    -   SHA-256 and SHA-512 hashing
    -   OFB cipher block mode
    -   Some other basic utility functionality

Currently supported platforms are:
    
*   Windows x32

*   Ubuntu Linux 14 (x32 and x64); other versions are untested, but may work

Future Releases
---------------
The roadmap for *SCryFE* is still being formulated, but the initial goal is
to create a replacement for [TrueCrypt](http://truecrypt.sourceforge.net).

*   Following this release, the existing limited *SCryFE* functionality will
    get some more rigourous testing. If we can interest anyone with a
    background in implementing strong crypto, we would hope to have them
    review the existing implementation for potential weaknesses.
    
    The results of testing and review will be released as *version 0.1.1.0*
    
*   The next areas of functionality targeted for implementation are:

    -   Mountable encrypted file systems
    
    -   Additional cryptographic algorithms, including ciphers, hashes,
        random-number generation
        
    This will be released as *version 0.2*.
    
*   Whole-disk encryption, released as *version 1.0*

*   Communications encryption, PKE, etc. - *versions 2, 3, etc.*

Contact
-------
The *SCryFE* team can be contacted via e-mail at scyfe@scryfe.com.