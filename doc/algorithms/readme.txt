`algorithms` Directory
======================
This directory contains technical explanations of how things are done in the
*SCryFE* library. It does *not* include explanations of third-party
algorithms (e.g., ciphers). It is intended for a technical audience,
including developers using the *SCryFE* library.

Index
-----
*   `file-encryption.md` - *(incomplete)* An overview of how the *SCryFE*
    library does file encryption using symmetric block-based ciphers.
