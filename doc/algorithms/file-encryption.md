---
title: File Encryption in *SCryFE*
date: 4 July 2007
---

-------------------------------------
          Igor Siemienowicz
  *igor.thecryptoproject@gmail.com*
-------------------------------------

The content of this file is copyright (c) 2014 by the named author(s), and licensed under terms of the Gnu Lesser General Public License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses` directory for the full text of this license.

# Introduction
This document explains how files are encrypted and decrypted using the
*SCryFE* library. The intended audience is software developers wishing to
use *SCryFE* for file encryption, or to understand how this is done. It
assumes a reasonable level of competence in software development, but not in
mathematics. The underlying ciphers and mathematical principles are not
discussed in detail, other than to explain how they are used at a code level.

# Interface
The primary interface for encrypting and decrypting files in *SCryFE* is the
`FileEncrypter` class (within the `SCRYFE` namespace). This class includes
two static (class) methods called `encrypt` and `decrypt`, which take all
the information they require as arguments to perform their respective
operations. This information includes:

*   The path and name of the file to be encrypted or decrypted

*   The cipher (encryption algorithm) to use

*   The block mode[^block-mode] to use

*   A randomly generated Initialisation Vector (IV) for the operation of the
    block mode (this is supplied for encryption, and retrieved from the file
    at decryption, and needs to be the same length as the block size of the
    cipher; e.g., for AES-256, which has a 128-bit block size, the IV needs
    to be 16 bytes long; more on this below)

*   The encryption key---this needs to be correct length for the cipher
    chosen; e.g., for AES-256, the key needs to be 32 bytes long

Thus, encrypting a file is as simple as:

    #include <scryfe/scryfe.h>

    // Encrypt a file.
    SCRYFE::FileEncrypter::encrypt(
        "myfile.txt",                   // the file to encrypt
        SCRYFE::FileEncrypter::aes256,  // the cipher
        SCRYFE::FileEncrypter::ofb,     // the block cipher mode
        iv,                             // the initialisation vector
        key);                           // the encryption key

The values `aes256` and `ofb` are enumerators that are subtypes of the
`FileEncrypter` class. Internally, the `encrypt` and `decrypt` methods take
care of instantiating the various objects that correspond to these
selections. At the time of writing (28 June 2014), *SCryFE* only includes a
single cipher (AES-256) and block mode (Output Feed Back, or OFB). The intent
is to add additional choices as *SCryFE* is developed.

The `iv` and `key` arguments are `SCRYFE::ByteArray` objects. `ByteArray` is
a class that wraps a raw array of bytes in C++.[^byte-array] Thus, encryption
is performed as a single method call.

It should be stressed that *the named file is overwritten by the encryption
and decryption operations.* The file is *not* copied. The main reason for
this is security. Simply creating an encrypted copy of the file and deleting
the original is generally *not* secure (unless a secure file wipe is
performed), because deleting a file in most file systems simply removes the
file from the directory index---the data remains on the disk, and is
retrievable. Overwriting the file with the encrypted version means that the
actual plaintext bytes are overwritten.

... *TODO decryption* ...

# The Cipher and Block Mode
... *TODO basic explanation of cipher and block mode classes* ...

# The Initialisation Vector (IV)
... *TODO explain random IVs* ...

# The Key
... *TODO explain keys and their use* ...

[^block-mode]: A cryptographic *block mode* is the method by which successive
    encryption blocks are 'chained' together to further obscure the overall
    structure of the data. The [Wikipedia article on block cipher
    modes](http://en.wikipedia.org/wiki/Block_cipher_mode_of_operation) gives
    an excellent explanation of how this works.

[^byte-array]: *SCryFE* uses this `ByteArray` class in preference to raw
    arrays of `char` adding functionality for memory management, and secure
    handling of secret data such as keys.
