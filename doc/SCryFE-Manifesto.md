---
title: '*SCryFE* Manifesto'
date: August 2014
---

-------------------------------------
      *"Strong Crypto for Everyone"*

          Igor Siemienowicz
  *igor.thecryptoproject@gmail.com*
-------------------------------------

The content of this file is copyright (c) 2014 by the named author(s), and licensed under terms of the Gnu Lesser General Public License (LGPL). See the file `lgpl-3.0.txt` in the `doc/licenses` directory for the full text of this license.

# Overview

## What Is It?
*SCryFE* is a project for creating a set of strong crypto products that anyone can use, and use properly. The first objective is to create a replacement for the recently defunct [TrueCrypt](http://truecrypt.sourceforge.net) disk and file encryption system.

Subsequent goals include:

*   Encrypted e-mail (integrated with popular email packages such as Thunderbird and Outlook)
*   Encrypted messaging / chat, either standalone, or as an add-on to existing packages
*   Encrypted voice; exact objectives unclear at this point, but either as a standalone VOIP system, or as an add-on to existing VOIP systems

Additional project objectives are to provide all functionality as high-quality re-usable FOSS code that can be easily both peer reviewed for vulnerabilities, and used by others in their applications.

## Guiding Principles

*   *"Strong crypto should be available to everyone for anything"*

    *Why?* One of the essential motivations for this project is that strong crypto promotes privacy. This is important for two basic reasons. First and foremost and that privacy is fundamentally important to protecting individual freedom in a society. To quote the [Privacilla web site](http://www.privacilla.org/fundamentals/whyprivacy.html):

    >Privacy helps individuals maintain their autonomy and individuality. People define themselves by exercising power over information about themselves and a free country does not ask people to answer for the choices they make about what information is shared and what is held close.

    Secondly, keeping one's information properly private has the functional benefit of protecting one from *identity fraud*, an increasingly common mechanism for theft and other forms of criminal and social harm.

    The *SCryFE* project seeks to enhance individual privacy by proving strong cryptographic capability to everyone:

    -   No matter who they are
    -   No matter where they are
    -   No matter what their economic circumstances are
    -   No matter what technical platform they are using
    -   No matter what they want to encrypt
 
*   *Transparency*---All work on *SCryFE* needs to be completely public and open to scrutiny, avoiding even the perception of secrecy. This includes:
    -   Source code and executable products
    -   Processes (development, management and financial), and governance
    -   Project-related dialog, notes and journals

*   *Freedom*--- *SCryFE* needs to be free (in the sense of both 'free speech' and 'free beer'):

    -   All material and components of  *SCryFE* must be free and unencumbered by restrictions on access, visibility, distribution and use. Such restrictions might include patents, or other legal means that would prevent particular people or groups from accessing *Crypto,* studying how it works, or using it in any particular way.

    -   Other than minimal fees to cover incidental costs such as dellivery or network access,  *SCryFE* products should be available to everyone at no charge.

*   *Credibility*
    -   All algorithms and components drawn from good and credible sources, with clear and documented provenance
    -   If possible,  *SCryFE* products should be reviewed and endorsed by recognised security experts

*   *Assumption of Vulnerability*
    -   It should never be assumed that any  *SCryFE* product is totally secure.
    -   All known theoretical and practical vulnerabilities to be documented, freely published and discussed

*   *Accessibility*--- *SCryFE* must be easy to obtain and easy to use. It should work in as wide a range of technical contexts as possible, and its functionality should be available as a library that other people can use in their own products.

## Products
 *SCryFE* will focus on providing two basic types of product:

*   A suite of easy-to-use, consumer-grade cryptography tools, including:
    -   (Initially) a full replacement for the functionality of TrueCrypt
    -   (In the future) PKI-based messaging and communication components, such as e-mail plugins, chat and encrypted VOIP software
*   An encryption code library, with interfaces in multiple languages

## Capabilities
The following is a summary of the intended capabilities of  *SCryFE* products:

*   Multiple choices of cryptographic algorithm, including ciphers, hashing and pseudo-random number generator
*   Public Key Encryption (future)
*   Key management
*   File and disk encryption
*   Encrypted communications (future)

## Benefits

*   Strong online and offline privacy
*   Freedom from overt and covert surveillance and influence
*   Easy to use consumer software for inexperienced users, with powerful and flexible libraries for programmers
*   Single solution for all privacy needs (eventually)

# Evangelism / Marketing

## Attracting People to the Project
*   Participants ...
*   Security Experts (for review / criticism and endorsement) ...
*   Users
*   Press / reviewers

## The  *SCryFE* Manifesto
This document outlines the direction of the  *SCryFE* project. It should not be considered static, but should be updated as the project and its goals are developed.

## Motivation
It is worth understanding why people would get involved  with the  *SCryFE* project in any role or capacity.

*   For **participating,** the main motivations would be a strong motivation for protecting privacy, and also interest in solving the technical problems involved.

*   For security experts, the motivation for **endorsing**  *SCryFE* would be confidence in its capabilities after reviewing them, a belief in the essential goals of the project, and also a belief that the project can succeed in achieving those goals.

*   The motivation for **using**  *SCryFE* would be a desire for private storage and/or communication of information, the belief that  *SCryFE* can deliver the required level of privacy, and also that its capabilities are easy and convenient to set up and use.

*   For software reviewers and bloggers, the main reason for reviewing, publicising and endorsing  *SCryFE* would be alignment with the goals of the project, and the desire to make their audience aware of the project, because they believe it to be capable of delivering on those goals.

The essential task of the evangelism and marketing aspect of this project is to reach these groups of people use these motivations.

## Evangelism Tools / Ideas
*   E-mail
*   Web site
*   Twitter
*   Blog
*   Participation in Crypto forums and discussions (also a good way to gather knowledge)

# Management

## Governance

### The  *SCryFE* Team {#the-crypto-team}
The  *SCryFE* Team is the notional 'owner' of the project. Collectively, team members make all significant decisions about the project, its direction, and the components contributed to it.

Initially, the project will be managed 'organically', with common sense prevailing, and a minimum of overhead. As required, the team will set policies and procedures for running the project in an effective fashion, taking into account:

*   The project goals
*   Fair and ethical behaviour
*   General 'best practices' for running a free software project

The  *SCryFE* team will have full access to the main  *SCryFE* source repository. Any team member has the capability and right to merge material into the repository, including the `master` branch, *subject to the policies and procedures of the project,* as set out in the version of this document that is on the `master` branch of the repository.

### Rules
*   Team decisions ...

### Project roles
It is anticipated that people participating to the project will contribute in different ways, and this worth reviewing these with a view to understanding how each type of contribution can be encouraged and best utilised. Naturally, a single person's efforts and talents may fall into more than one category.

*   *Champion*---A 'believer' in the project objectives, a  *SCryFE* 'champion' is strongly and personally motivated by the overall goals of the project, and is genuinely enthusiastic for its successful outcomes. Project champions are good sources of enthusiasm, inspiration and guidance, and can be good for helping motivate other participants.. Sometimes they have to be 'reined in' when their vision becomes overly grand or impractical.

*   *Fearless leader*---For all that free software projects are very egalitarian in structure, they still need leadership and direction. A 'fearless leader' of the  *SCryFE* project is someone who facilitates or coordinates planning, tasking and resource management. As a project grows in scope, and becomes more complex, it can be difficult to know what to work on next. The 'fearless leader' has a clear picture of the current status of the project, and the way forward, and is able to advise people about where to focus their efforts.

*   *Tech Wizard*---Technical input at any level

*   *Crypto Wizard*---Provider of security expertise

*   *Preacher*---Anyone who helps promote, evangelise or market the project to other people

## Expert Review
...

## The Grand Plan
1.  TrueCrypt replacement
    1.1 Basic end-to-end implementation of simple file encryption
    1.2 Mountable encrypted file systems
    1.3 Whole-disk encryption
2.  E-mail integration
3.  Messaging (chat)
4.  Audio
5.  ...

## Legal Considerations
*   Licensing ...
*   Indemnity for participants ...
*   Protection for participants and users where strong encryption is restricted

# Technical

## Development Principles
*  *Free / open tools wherever possible*---This is not always possible, and non-free tools will be used when necessary to allow  *SCryFE* to be used in as many environments as possible. One examples would be the XCode development environment for OSX and iOS.

*   *Clear, readable and well-commented code*---This is essential so that cryptographic implementations can be reviewed as easily as possible.

*   *Platform independence*---Wherever possible, so that the potential user pool is as large as possible

*   *Self-containment*--- *SCryFE* products should be deployable as standalone software, not relying other other components to work

*   *Pragmatism*---While good coding and development practices are the ideal, this must be tempered with 'real world' considerations and practicality

*   *Code reuse*---Wherever practical, existing (unencumbered) code should be reused, rather than writing stuff from scratch. This is not just motivated from laziness. Existed 'tried and true' code is preferable to new code that may be immature and have unknown bugs.

## Development Processes
*   *Agile*---Coding in short 'sprints' with frequent software releases

*   *Test-Driven Development*---Goals and requirements expressed as tests. When the system passes the tests, the requirements are deemed to be fulfilled.

## Procedures
*   *Code review*---...
*   *Version numbering*---...

## Software Development Life Cycle

### Requirements
*See the* Crypto Requirements *document.*

### Design
Design documentation for elements and products of the  *SCryFE* project are the minimum required to support the essentual purposes of:

*   Assisting the creative development process
*   Promoting understanding to people wishing to understand the project implementation.

All project design documentation is located in the `doc/design` subdirectory of the project repository.

### Implementation
Implementation is the process of turning requirements and design work into tangible products. For  *SCryFE*, these tangible products are:

*   A consumer software suite of cryptography products for a variety of supported platforms, including:
    -   A GUI-based executable for encrypting and decrypting files and *(future)* managing encrypted filesystems and disks
    -   A CLI-based executable that performs the same functions as the GUI
    -   Documentation
    -   An automated installer
*   A Software Development Kit (SDK) of cryptographic functionality, including:
    -   A C++ API, and *(future)* APIs in other languages
    -   Linkable libraries for various supported platforms
    -   Documentation for the API with examples, including explanations of how the consumer product code is structured
    -   Code examples

The tools for producing these products are discussed in the [*Tools and Platforms*](#tools-and-platforms) section.

It is worth recognising that most implementation choices (e.g., coding style and practices, structure and architecture, etc.) are explicitly deferred at this early stage of development. General common sense and best practices should prevail. As necessary, the  *SCryFE* team will put additional rules and procedures in place as the project develops (see [*The Crypto Team*](#the-crypto-team)).

### Testing
...

### Deployment
...

## Tools and Platforms {#tools-and-platforms}
*   Documentation
    -   Markdown
    -   [Dia (for technical diagrams)](http://dia-installer.de/)
    -   [Doxygen](http://www.stack.nl/~dimitri/doxygen/)
    -   [Pandoc](http://johnmacfarlane.net/pandoc/)
*   Source control: [Bitbucket](https://bitbucket.org/)
*   Languages
    -   C++ (core functionality)
    -   Functionality interfaces
        -   Java
        -   Python
        -   Ruby
        -   Swift (for iOS)
*   Build system: [CMake](http://cmake.org/)
*   General automation: rake
*   Compilers: multiple, as appropriate for platform
*   Installation packaging: [NSIS](http://nsis.sourceforge.net/Main_Page)
*   Issue management and development planning: [Bitbucket](https://bitbucket.org/)
*   Virtualisation: [VirtualBox](https://www.virtualbox.org/)