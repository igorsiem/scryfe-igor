---
title: scryfe-cli(1) *SCryFE* CLI User Manual
date: 5 July 2014
author: Igor Siemienowicz
---

# Name
`scryfe-cli` - Strong encryption utility
    
# Synopsis
`scryfe-cli [options] [file]`
    
# Description
**Disclaimer: Please note: This version of** *SCryFE* **is incomplete, and
has not been fully tested. It may destroy data, even when used correctly, and
has not been reviewed by security experts, and so may be insecure. It is not
recommended for use in an production context.**

*SCryFE* is a collection of strong encryption functionality for a variety of
applications. In this current (very early) implementation, this utility
encrypts and decrypts file securely, with a password.

To encrypt a file, specify `encrypt` operation, and the file path / name, as
in the following example:

    scryfe-cli -o encrypt myfile.txt
    
**It is important to note that encryption overwrites the file. It does not
create a copy.**

The utility will ask for a typed key phrase (which will be converted to
asterisks), and will then request that the user type randomly on the
keyboard. The purpose of this is to allow *SCryFE* to accumulate random bytes
of data (entropy) to initialise the encryption algorithms. The user should
type as randomly as possible, until the percentage indicator displayed
reaches 100%.

The file will then be encrypted.

To decrypt an encrypted file, specify the `decrypt` operation, as in the
following example:

    scryfe-cli -o decrypt myfile.txt
    
Once again, the utility will ask for the keyphrase. Also again, **note that
the file is overwritten, not copied. If an incorrect key phrase is entered,
the file will be irretrievably corrupted.**

In its current implementation, *SCryFE* uses the AES-256 (Rijndael) symmetric
block cipher in output feedback (OFB) mode. Future iterations will include:

*   more choices of cipher and block mode
*   Expanded functionality for mounted encrypted file systems and whole disk
    encryption

# Options
-h, --help
:   A brief help message

-o *OPERATION*, --operation *OPERATION*
:   Specify the operation to perform. *OPERATON* must be one of "encrypt" or
    "decrypt"

--verbose
:   Verbose output

# Copyright
*SCryFE* and the `scryfe-cli` utility are copyright (C) 2014 Igor
Siemienowicz

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see
[<http://www.gnu.org/licenses/>](http://www.gnu.org/licenses/)

# See Also
*SCryFE* source code and all documentation can be downloaded from
[BitBucket](https://bitbucket.org/scryfe/scryfe)