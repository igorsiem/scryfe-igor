`user` Directory
================
This directory is for user-level documentation of *SCryFE* consumer products.
The markdown source files are compiled into various usable formats for
inclusion in deliverable packages.

Index
-----
*   `scryfa-cli.md` - The user manual for the *SCryFE* command-line interface
    this is converted into a man page for posix-style systems, as well as a
    PDF

*   `user-manual` - The main user manual for *SCryFE*; this concentrates
    mainly on the GUI (and refers to the CLI manual)

*   `website` - The `jekyll` source for the [*SCryFE*
    website](http://scryfe.com)
