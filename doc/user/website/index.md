---
layout: page
title: Welcome to SCryFE
---

*"Strong Crypto for Everyone"*

Welcome to *SCryFE*, a project for creating free encryption software and code libraries. *SCryFE* is still in the early stages of development, but the initial goal of the project is to produce a functional replacement for [TrueCrypt](http://truecrypt.sourceforge.net/). You can read more about where the project is going on [the About *SCryFE* page](/about).

**The latest (August 2014):** You can now [download](download) an early test version of the *SCryFE* consumer software, and SDK. The only 'end user' functionality currently included is simple encryption of single files, using the AES-256 cipher in output feedback mode. *This version is purely intended for testing and review - it should not be used in any production context whatsoever. Please read all available documentation before installing and running this software.*

## Contact
This website is for general information about *SCryFE*, and major release announcements. Regular updates and announcements are made via [@scryfeproject on Twitter](http://twitter.com/scryfeproject) and [scryfe.tumblr.com](http://scryfe.tumblr.com). The *SCryFE* team may be contacted on [scryfe@scryfe.com](mailto:scryfe@scryfe.com).

If you're interested in the technical aspects of encryption software, you can check out [the *SCryFE* repo on Bitbucket.org](https://bitbucket.org/scryfe/scryfe).