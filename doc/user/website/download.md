---
layout: page
permalink: /download/
title: Downloads and Documentation
---

All versions of *SCryFE* software can be downloaded from the [*SCryFE* Bitbucket downloads page](https://bitbucket.org/scryfe/scryfe/downloads). The following sections describe the various versions of *SCryFE*, what they are intended for, and any specific issues or comments associated with them.

## Latest Stable Version (N/A)
There is currently no stable version of *SCryfe* available for download. The initial release version is in testing.

## Documentation
The following user manual documents can be downloaded directly. These are also included wit h the software distributions.

*   [The *SCryFE* User Manual](/assets/SCryFE-User-Manual.pdf)
*   [The *SCryFE* CLI User Manual](/assets/scryfe-cli-Users-Manual.pdf)

## Build `0.1.0.0`
This is an initial test version of the *SCryFE* software. It includes very limited functionality:

*   Encryption and decryption of single files, using the AES-256 cipher and output feedback block mode
*   (SDK only) Encryption and decryption of arbitrary sequences of data, using the AES-256 cipher and output feedback mode
*   (SDK only) SHA-512 and SHA-256 hashing

This version will be subjected to a formal testing and review process. Issues arising from this will be logged and addressed as appropriate for a beta release.

*Note: This build is intended for testing and review only. It has had little or no testing so far, and is* ***not*** *intended for production use. It may corrupt or unintentionally expose data and files on which it is used.*
