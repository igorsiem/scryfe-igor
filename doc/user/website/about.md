---
layout: page
permalink: /about/
title: About SCryFE
---

*"Strong Crypto For Everyone"*

*SCryFE* is a project for producing strong cryptographic software that anyone can access and use easily. It is motivated by the premise that privacy is an important thing to protect in today's society, and that strong encryption capabilities ought to be available to everyone to help protect that right. The initial goal of the project is to provide a functional replacement for [TrueCrypt](http://truecrypt.sourceforge.net) (in fact, the recent demise of that project was one of the inspirations for *SCryFE*).

## Why Are We Doing This?
There are two essential motivations for the *SCryFE* project. First and foremost is that privacy is fundamentally important to protecting individual freedom in a society. To quote the [Privacilla web site](http://www.privacilla.org/fundamentals/whyprivacy.html):

>Privacy helps individuals maintain their autonomy and individuality. People define themselves by exercising power over information about themselves and a free country does not ask people to answer for the choices they make about what information is shared and what is held close.

Secondly, keeping one's information properly private has the functional benefit of protecting one from *identity fraud*, an increasingly common mechanism for theft and other forms of criminal and social harm. As electronic forms of social and economic interaction become more prevalent, this threat becomes larger, and privacy becomes more and more important?

## So What is *SCryFE* Exactly?
*SCryFE* is a collection of computer software. Specifically, it consists of the following:

*   Consumer level software that helps you keep your information and communication secret, including:
    -   File and file system encryption (the current target of the project)
    -   Encrypted e-mail (future target)
    -   Encrypted messaging, and voice (longer-term targets)
*   A code library (the *SCryFE* Developer's Kit, or SDK), for writing software that performs cryptographic functions; the intent is that all functionality provided by the consumer software be made available to programmers at the SDK level as well.

## What Can *SCryFE* Do Right Now?
In all honesty, very little! At the time of writing (August 2014), *SCryFE* is still in the early stages of development. A initial test version with extremely limited functionality is [available for download](download), but *it should not be used for 'production' purposes* (such as actually encrypting data or files that you care about). It is *only* intended for review and testing, and a stable beta will be released when this is complete.

For those who want to fiddle, or are simply curioys, the source code for the project is [hosted on Bitbucket)](https://bitbucket.org/scryfe/scryfe) as a public project, and it can be freely forked, copied, examined and played with.

If all goes well, we hope to release production-grade executable software in the first quarter of 2015.

## Where is *SCryFE* Going?
The following is a rough roadmap for the development of *SCryFE* products and technology:

*    *Version 1* - Files and file system encryption
    -   *0.1* - Simple file encryption and decryption
    -   *0.2* - Mountable encrypted file system
    -   *1.0* - Whole disk encryption
*   *Version 2* - Encrypted e-mail
    -   Public-Key Infrastructures (OpenPGP implementation)
    -   E-mail plugins
    -   E-mail client
*   *Version 3* - Encrypted chat
*   *Version 4* - Encrypted voice

## *SCryFE* is Free
*SCryFE* is a free software project (both as in 'free speech' and as in 'free beer'), and its products are licensed under the Gnu General Public License (GPL) version 3. Software components used by the project, but not written by the *SCryFE* team are also licensed under the GPL, or another license that is compatible with it.

Apart from the obvious benefits of getting something for nothing, an important aspect of the security of *SCryFE* is that all its source code and algorithms can be freely examined by anyone. This means that potential weaknesses can be found and fixed by anyone. Relying on traditional 'closed source' software for security is often a bad idea, because one has to rely on (and trust) a single party or group to maintain that software and find and fix all flaws.

It is worth noting that this principle is particularly true of the mathematical algorithms that are the heart of modern crypto systems. *SCryFE* only uses publicly available encryption algorithms that have been thoroughly reviewed by cryptography experts and commonly believed to be strong. Secret or immature algorithms are best avoided, because significant flaws can be discovered in crypto systems, even after a repeated reviews period.

All *SCryFE* software components and source code are free, and may be accessed, used and changed under the terms of the GPL.

## Who Are We?
At the time of writing (August 2014), the *SCryFE* team is very small, comprised mainly of applications programmers. Some of us have a background in encryption and mathematics, and we are hoping to recruit people with active experience in this area. If you are interested in participating in *SCryFE*, please contact us on [scryfe@scryfe.com](mailto:scryfe@scryfe.com).