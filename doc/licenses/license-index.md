License Index
=============
This document lists the various licenses used by *SCryFE* and its components.
*SCryFE* itself is licensed under LGPL 3.0 (see `lgpl-3.0.txt`). Various
third-party components are licensed in different ways, but are chosen for
compatibility with the LGPL 3.0 license.

*Please note:* Licensing is important in this project. All components *must*
have a license that is compatible with GPL 3.0 or LGPL 3.0 (in the case of
code libraries. When adding third-party code to this project, please consider
this, and update this document accordingly.

*   *CppUnit* - Unit testing library; licensed under LGPL 3.0 (see
    `lgpl-3.0.txt`)
    
*   *Documentation* - All *SCryFE* documentation is licensed under the Gnu
    Free Documentation Licence 1.3 (see `fdl-1.3.txt`), except where
    explicitly noted otherwise.

*   *Rijndael* - The Rijndael reference implementation is public-domain (see
    [http://www.efgh.com/software/rijndael.htm](http://www.efgh.com/software/rijndael.htm)
    for more information

*   *SCryFE* - All code written under this project for incorporation in the
    *SCryFE* library is licensed under LGPL 3.0 (see `lgpl-3.0.txt`)
    
*   *`scryfe-cli`* - The consumer-level Command-Line Interface to *SCryFE*
    functionality is licensed under the GPL version 3.0 (see `gpl-3.0.txt`).

*   *SHA* - The IETF SHA hash reference implementation is taken directly from
    [RFC-6234](http://tools.ietf.org/html/rfc6234). It is free to use and
    distribute, provided that the IETF copyright and distribution notice is
    distributed with all source and binary products. See the file
    `IETF-distribution-notice.txt`.